<?php
$root = $_SERVER['DOCUMENT_ROOT'];

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
require($root.'/inc/classes/sessions.php');
include($root.'/inc/system/usession.php');

if(!$uban_type) {
 header('Location: /');
 exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Доступ ограничен.</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
  <div id="header_load"></div>
 <div id="page">
  <div id="black_bg"></div>
  <div id="error_head"></div>

  <div id="box_smilies_box"></div>

  <div id="loading"><div id="load"></div></div>

<? include($root.'/include/header.php') ?>

   <div id="content">
    <div id="left">
     <div id="account_is_deleted_left">
      Доступ ограничен.
     </div>
    </div>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main">
         <div class="info-header-block banned_img">
             <h4>Доступ ограничен.</h4>
             <p>К сожелению, доступ к сайту есть только у сотрудников Russian Express.</p>
             <p>Ожидайте когда первый освободившийся <b>Администратор</b> рассмотрит вашу заявку.</p>
             <p><b>Russian Express</b> одна из самых больших компаний.</p>
             <div class="info-bottom">
                 Доступ ограничен.
             </div>
         </div>
		  <? if(!$uban_text) { ?>

		  <? } else { ?>
        <div id="blocked_page">
            <div id="blocked_user_comment_moder_title">Комментарий администратора о <b>причинах блокировки:</b></div>
            <div id="blocked_user_comment_moder">
                <? echo nl2br($uban_text); ?>
            </div>
        </div>
		  <? } ?>
      </div>
     </div>
     <input type="hidden" value="<? echo $usession; ?>" id="ssid">
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
 </body>
</html>
