<?php

/** Инициализация примитивных хелперов для бутстраппинга системы */

require __DIR__ . '/helpers.php';

/** Инициализация автозагрузки зависимостей из Composer */

$composerAutoloadFile = __DIR__ . '/vendor/autoload.php';

if(file_exists($composerAutoloadFile)){
    require __DIR__ . '/vendor/autoload.php';
} else {
    throw new Exception('No such autoload file.');
}

/** Запуск системы */
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'main';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
include($root.'/inc/system/profile.php');

if($uban_type) {
 header('Location: /blocked');
 exit;
} elseif($udel) {
 header('Location: /deleted');
 exit;
} elseif($user_logged) {
 header('Location: /tasks');
 exit;
}
//test
?>
<!DOCTYPE html>
<html lang="ru">
 <head>
  <title>Russian Express | Virtual Company</title>
<? include($root.'/include/head.php') ?>
 </head>
<body>
<div id="header_load"></div>
 <div id="black_bg"></div>
<div id="box_smilies_box"></div>
 <div id="page">
<? include($root.'/include/header.php') ?>

 <div id="content">
 <? include($root.'/include/left.php') ?>

     <div id="right_wrap">
       <div id="right_wrap_b">
        <div id="right">
         <div class="main nopad">
          <div class="info-header-block home_img">
           <h4>Russian Express</h4>
           <p>Официальный сайт компании</p>
           <p>Это уникальная система управления виртуальными компаниями на просторах <b>Euro Truck Simulator 2</b>.</p>
           <p>Доступ к сайту есть только у сотрудников <b>Russian Express</b>.</p>
              <p>Наш сервер TeamSpeak &mdash; <b><a href="ts3server://ts.vtc-express.ru">ts.vtc-express.ru</a></b>.</p>
          </div>
          <div class="text_home">
            <span>Одна из самых крупных компаний на просторах Euro Truck Simulator 2.</span>
          </div>
         </div>
        </div>
       </div>
<? include($root.'/include/footer.php') ?>

 </div>
</div>

   <div id="live_counter"></div>
   <input type="hidden" value="<? echo $_GET['ref']; ?>" id="ureg_ref" />
   <input type="hidden" id="captcha_key" />
   <input type="hidden" id="captcha_code" />
   <input type="hidden" value="1" id="ureg_wnd_open" />
  </div>

  <script id="mainscripts" type="text/javascript">

   $('#ulogin').attr('iplaceholder', 'Введите логин');
   $('#upassword').attr('iplaceholder', 'Введите пароль');

   _placeholder('#ulogin');
   _placeholder('#upassword');

   // авторизация по нажатию на Enter
   $('#ulogin, #upassword').keydown(function(event) {
    var keyCode = event.which;

    if(keyCode == 13) {
     users._post_login();

     return false;
    }
   });
  </script>
 </body>
</html>
