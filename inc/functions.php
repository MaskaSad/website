<?php
// определение IP
function ip_address() {
	if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) $ip_result = $_SERVER['HTTP_X_FORWARDED_FOR'];
	elseif (isset($_SERVER['HTTP_CLIENT_IP'])) $ip_result = $_SERVER['HTTP_CLIENT_IP'];
	else $ip_result = $_SERVER['REMOTE_ADDR'];
	return $ip_result;
}
// определение браузера пользователя
function user_browser() {
	$str = getenv('HTTP_USER_AGENT');
	if (strpos($str, 'Avant Browser', 0) !== false) return 'Avant Browser';
	elseif (strpos($str, 'Acoo Browser', 0) !== false) return 'Acoo Browser';
	elseif (@eregi('Iron/([0-9a-z\.]*)', $str, $pocket)) return 'SRWare Iron ' . $pocket[1];
	elseif (@eregi('Chrome/([0-9a-z\.]*)', $str, $pocket)) return 'Google Chrome ' . $pocket[1];
	elseif (@eregi('(Maxthon|NetCaptor)( [0-9a-z\.]*)?', $str, $pocket)) return $pocket[1] . $pocket[2];
	elseif (@strpos($str, 'MyIE2', 0) !== false) return 'MyIE2';
	elseif (@eregi('(NetFront|K-Meleon|Netscape|Galeon|Epiphany|Konqueror|' . 'Safari|Opera Mini)/([0-9a-z\.]*)', $str, $pocket)) return $pocket[1] . ' ' . $pocket[2];
	elseif (@eregi('Opera[/ ]([0-9a-z\.]*)', $str, $pocket)) return 'Opera ' . $pocket[1];
	elseif (@eregi('Orca/([ 0-9a-z\.]*)', $str, $pocket)) return 'Orca Browser ' . $pocket[1];
	elseif (@eregi('(SeaMonkey|Firefox|GranParadiso|Minefield|' . 'Shiretoko)/([0-9a-z\.]*)', $str, $pocket)) return 'Mozilla ' . $pocket[1] . ' ' . $pocket[2];
	elseif (@eregi('rv:([0-9a-z\.]*)', $str, $pocket) && strpos($str, 'Mozilla/', 0) !== false) return 'Mozilla ' . $pocket[1];
	elseif (@eregi('Lynx/([0-9a-z\.]*)', $str, $pocket)) return 'Lynx ' . $pocket[1];
	elseif (@eregi('MSIE ([0-9a-z\.]*)', $str, $pocket)) return 'Internet Explorer ' . $pocket[1];
	else return 'Unknown';
}

function new_time($a) { // преобразовываем время в нормальный вид
	date_default_timezone_set('Europe/Moscow');
	$ndate = date('d.m.Y', $a);
	$ndate_time = date('H:i', $a);
	$ndate_exp = explode('.', $ndate);
	$nmonth = array(
		1 => 'янв',
		2 => 'фев',
		3 => 'мар',
		4 => 'апр',
		5 => 'мая',
		6 => 'июн',
		7 => 'июл',
		8 => 'авг',
		9 => 'сен',
		10 => 'окт',
		11 => 'ноя',
		12 => 'дек'
	);

	foreach ($nmonth as $key => $value) {
		if ($key == intval($ndate_exp[1])) $nmonth_name = $value;
	}

	if ($ndate == date('d.m.Y')) return 'сегодня в ' . $ndate_time;
	elseif ($ndate == date('d.m.Y', strtotime('-1 day'))) return 'вчера в ' . $ndate_time;
	else return $ndate_exp[0] . ' ' . $nmonth_name . ' ' . $ndate_exp[2] . ' в ' . $ndate_time;
}

function getAge($day, $mouth, $year) {
	// если в этом году уже был день рождения
	if(	$mouth > date('m') || 
      $mouth == date('m') && 
      $day > date('d')
     ) {
		return (date('Y') - $year - 1);
	} else {
	  // если еще не прошел день рождения
	  $result = date('Y') - $year;
	}
	return $result;
}

function convoys_left_time($a) {
	date_default_timezone_set('Europe/Moscow');
	$ndate = date('d.m.Y', $a);
	$ndate_time = date('H:i', $a);
	$ndate_exp = explode('.', $ndate);
	$nmonth = array(
		1 => 'янв',
		2 => 'фев',
		3 => 'мар',
		4 => 'апр',
		5 => 'мая',
		6 => 'июн',
		7 => 'июл',
		8 => 'авг',
		9 => 'сен',
		10 => 'окт',
		11 => 'ноя',
		12 => 'дек'
	);

	foreach ($nmonth as $key => $value) {
		if ($key == intval($ndate_exp[1])) $nmonth_name = $value;
	}

	if ($ndate == date('d.m.Y')) return 'на сегодня';
	elseif ($ndate == date('d.m.Y', strtotime('+1 day'))) return 'на завтра';
	elseif ($ndate == date('d.m.Y', strtotime('-1 day'))) return 'был вчера';
	elseif ($ndate == date('d.m.Y', strtotime('-2 day'))) return 'был позавчера';
	elseif ($ndate == date('d.m.Y', strtotime('-3 day'))) return 'был 3 дня назад';
	else return '';
}

// генерируем случайные символы
function rand_str($length, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890')
{
	$chars_length = (strlen($chars) - 1);
	$string = $chars {
		rand(0, $chars_length)
	};
	for ($i = 1; $i < $length; $i = strlen($string)) {
		$r = $chars {
			rand(0, $chars_length)
		};
		if ($r != $string {
			$i - 1
		}) $string .= $r;
	}
	return $string;
}

// отправка письма на email
function send_email($to, $title, $message)
{
	$message = '
 ' . $title . '

	 ' . $message . '
    ';

	$headers  = "Content-type: text/html; charset=utf-8 \r\n";
	$headers .= "From: <robot@vtc-express.ru>\r\n";

	mail($to, $title, $message, $headers);
}

// проверяем список id аттачей
function check_id_attaches($list = null)
{
	if (!preg_match('/^(?:\d\,?)+\d?$/', $list)) {
		return 0;
	} else {
		return 1;
	}
}

// нормальный вид id'ов у аттачей
function normal_id_attaches($list = null)
{
	return preg_replace('/\,$/', '', $list);
}

// создаем переключатели страниц
function pages($param) {
	$pages_go = '';
	$pages_back = '';
	$page_tmpl = ''; // объявляем переменную page_tmpl для использования return
	$full_url_replace = array('?page=' . $_GET['page'], '&page=' . $_GET['page'], 'page=' . $_GET['page']);
	$full_url = str_replace($full_url_replace, '', $_SERVER['REQUEST_URI']);
	$full_string = str_replace($full_url_replace, '', $_SERVER['QUERY_STRING']);
	$que_url = $full_string ? '&' : '?';
	$pages_cicle_start = 1;
	$pages_count = ceil($param['ents_count'] / $param['ents_print']);

	if ($pages_count > 3) {
		$pages_cicle = 3;
		$pages_go = '<a class="page" href="' . $full_url . '' . $que_url . 'page=' . $pages_count . '" onclick="nav.go(this); return false;">»</a>';
		if ($_GET['page'] >= 2) {
			if ($_GET['page'] + 1 >= $pages_count) {
				$pages_cicle = $pages_count;
				$pages_go = '';
			} else $pages_cicle = $_GET['page'] + 2;
			$pages_cicle = $_GET['page'] + 1 >= $pages_count ? $pages_count : $_GET['page'] + 2;
			$pages_cicle_start = ($_GET['page'] == 1 || $_GET['page'] == 2) ? $_GET['page'] - 1 : $_GET['page'] - 2;
			if ($_GET['page'] > 3) $pages_back = '<a class="page" href="' . $full_url . '' . $que_url . 'page=1" onclick="nav.go(this); return false;">«</a>';
		}
	} else {
		$pages_cicle = $pages_count;
		$pages_cicle_start = 1;
	}

	for ($i = $pages_cicle_start; $i <= $pages_cicle; $i++) {
		if ($i == $param['page'] || !$param['page'] && $i == 1) $page_tmpl .= '<a class="page active" href="' . $full_url . '' . $que_url . 'page=' . $i . '" onclick="nav.go(this); return false;">' . $i . '</a>';
		else $page_tmpl .= '<a class="page" href="' . $full_url . '' . $que_url . 'page=' . $i . '" onclick="nav.go(this); return false;">' . $i . '</a>';
		if ($i != $pages_count) $page_tmpl .= '';
	}
	return $pages_back . '' . $page_tmpl . '' . $pages_go;
}

// создаем переключатели страниц для боксов
function pages_ajax($param) {
	$pages_go = '';
	$pages_back = '';
	$page_tmpl = ''; // объявляем переменную page_tmpl для использования return
	$full_url_replace = array('?page=' . $_GET['page'], '&page=' . $_GET['page'], 'page=' . $_GET['page']);
	$full_url = str_replace($full_url_replace, '', $_SERVER['REQUEST_URI']);
	$full_string = str_replace($full_url_replace, '', $_SERVER['QUERY_STRING']);
	$que_url = $full_string ? '&' : '?';
	$pages_cicle_start = 1;
	$pages_count = ceil($param['ents_count'] / $param['ents_print']);

	if ($pages_count > 3) {
		$pages_cicle = 3;
		$pages_go = '<a class="page" href="' . $full_url . '' . $que_url . 'page=' . $pages_count . '" onclick="return false">»</a>';
		if ($_GET['page'] >= 2) {
			if ($_GET['page'] + 1 >= $pages_count) {
				$pages_cicle = $pages_count;
				$pages_go = '';
			} else $pages_cicle = $_GET['page'] + 2;
			$pages_cicle = $_GET['page'] + 1 >= $pages_count ? $pages_count : $_GET['page'] + 2;
			$pages_cicle_start = ($_GET['page'] == 1 || $_GET['page'] == 2) ? $_GET['page'] - 1 : $_GET['page'] - 2;
			if ($_GET['page'] > 3) $pages_back = '<a class="page" href="' . $full_url . '' . $que_url . 'page=1" onclick="return false">«</a>';
		}
	} else {
		$pages_cicle = $pages_count;
		$pages_cicle_start = 1;
	}

	for ($i = $pages_cicle_start; $i <= $pages_cicle; $i++) {
		if ($i == $param['page'] || !$param['page'] && $i == 1) $page_tmpl .= '<a class="page active" href="' . $full_url . '' . $que_url . 'page=' . $i . '" onclick="return false">' . $i . '</a>';
		else $page_tmpl .= '<a class="page" href="' . $full_url . '' . $que_url . 'page=' . $i . '" onclick="return false">' . $i . '</a>';
		if ($i != $pages_count) $page_tmpl .= '';
	}
	return $pages_back . '' . $page_tmpl . '' . $pages_go;
}

// склонение числительных
function declOfNum($number, $titles) {
	$cases = array(2, 0, 1, 1, 1, 2);
	return $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
}

// CURL
function curl($url, $post = false) {
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt(
		$ch,
		CURLOPT_USERAGENT,
		'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/23.0.1229.94 Safari/537.4 AlexaToolbar/alxg-3.1'
	);
	if ($post) {
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	}
	curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
	curl_setopt($ch, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
	$response = curl_exec($ch);
	curl_close($ch);
	return $response;
}

// xss
function fxss($text) {
	return htmlspecialchars($text);
}

// параметры к URL
function querys($url, $param) {
	global $query_string;

	if ($query_string) {
		return $url . '?' . preg_replace('/' . $param . '=(.*?)&/', '', $query_string . '&' . $param . '=');
	} else {
		return $param ? $url . '?' . $param . '=' : '';
	}
}

// русские символы в json
function jdecoder($json_str) {
	return $json_str;
}

// количество вопросов в поддержке(левое меню)
function my_support_new($admin = null) {
	global $db, $user_id, $ugroup;

	if ($admin) {
		$query = "SELECT `id` FROM `support_questions` WHERE `uid` != '$user_id' AND `status` = '0' AND `del` = '0'";
	} else {
		$query = "SELECT `id` FROM `support_questions` WHERE `uid` = '$user_id' AND `status` = '1' AND `del` = '0'";
	}

	$q = $db->query($query);
	$n = $db->num($q);

	return $n;
}


function left_convoys() {
	global $db, $user_id, $ugroup;

	$query = "SELECT `ustart`, `ustop`, `udate` FROM `convoys` WHERE del = 0 ORDER BY `id` DESC LIMIT 1";

	$q = $db->query($query);
	$d = $db->fetch($q);
	$start = $d['ustart'];
	$stop = $d['ustop'];
	$udate = $d['udate'];
	$convoys_result .= '<div id="info_convoys_left" style="display: block;" onclick="convoys._info_left()"><b>Конвой ' . convoys_left_time($udate) . ':</b><br>' . $start . ' → ' . $stop . '</div>';

	if ($udate == '') {
		return false;
	}

	return $convoys_result ? $convoys_result : '';
}

function check_award_done($award_id = null, $uid = null, $type = null) {
 global $db;

 $q = $db->query("SELECT `id` FROM `award_done` WHERE `uid` = '$uid' AND `award_id` = '$award_id' AND `type` = '$type'");
 $d = $db->fetch($q);

 return $d['id'] ? 1 : 0;
}

function award($uid = null, $count = null, $type = null) {
    global $db, $dbName, $user_id, $ugroup;

    if($type == 1) {
        $qtype = '=';
    } elseif($type == 2) {
        $qtype = '=';
    } elseif($type == 3 || $type == 6) {
        $qtype = '=';
    } elseif($type == 4 || $type == 5) {
        $qtype = '<=';
    }

    $q = $db->query("SELECT `id`, `award_id`, `award_points`, `count`, `lvl` FROM `award` WHERE `count` ".$qtype." $count AND `type` = '$type' ORDER BY `count` DESC LIMIT 1");
    $d = $db->assoc($q);

    $id = $d['id'];
    $award_id = $d['award_id'];
    $award_points = $d['award_points'];
    $count = $d['count'];
    $lvl = $d['lvl'];

    if($award_id) {
        $check_award_done = check_award_done($award_id, $uid, $type);
    } else {
        $check_award_done = 1;
    }

    if($check_award_done == 0) {
        $db->query("INSERT INTO `$dbName`.`award_done` (`id`, `award_id`, `uid`, `lvl`, `time`, `type`) VALUES (NULL, '$award_id', '$uid', '$lvl', '".time()."', '$type');");
        addBalancePoints($award_points, $uid, 3);

        $json = 1;
    } else {
        $json = 2;
    }
    return $json;
}

function no_name($name = null)
{
	return trim($name) ? $name : 'Безымянный';
}

function no_avatar($avatar = null) {
  global $noavatar;

  return $avatar ? $avatar : $noavatar;
}

/*
 Получаем информацию о категории
 Пример вызова: getCategoryTitle('id категории');
*/
function getCategoryTitle($catid = null) {
    global $db;

    if($catid > 0) {
        $q = $db->query("SELECT `title` FROM `category` WHERE `id` = '$catid'");
        $d = $db->fetch($q);
        $cat_title = $d['title'];
    } else {
        $cat_title = 'Испытательный срок';
    }
   return $cat_title;
}

/*
 Добавление баллов пользователю за конвои, срочные заказы и др.
 Пример вызова: addBalancePoints('Количество баллов', 'id сотрудника', 'Тип 1) за конвои 2) за срочные заказы');
*/
function addBalancePoints($count = null, $to = null, $type = null) {
    global $db, $dbName, $user_id, $redis;

    $root = $_SERVER['DOCUMENT_ROOT'];
    include($root.'/inc/system/redis.php');

    if($count < 0) {
        return json_encode(array('error_text' => 'min_point'));
    } else {
        $db->query("UPDATE `$dbName`.`users` SET  `upoints` =  upoints + '$count' WHERE  `users`.`uid` = '$to' LIMIT 1 ;");
        $db->query("INSERT INTO `$dbName`.`balance_history` (`id`, `from`, `to`, `points`, `time`, `type`) VALUES (NULL, '$user_id', '$to', '$count', '".time()."', '$type');");

        $q = $db->query("SELECT `upoints` FROM `users` WHERE `uid` = '$to'");
        $d = $db->fetch($q);
        $redis_balance = $d['upoints'];

        $redis->hdel('user_'.$to.'_balance', 'balance');
        $redis->hset('user_'.$to.'_balance', 'balance', $redis_balance);
        $redis->expire('user_'.$to.'_balance', 43200);
    }

}

/*
 Изменения баллов сотруднику (пополнение или списание).
 Пример вызова: subBalancePoints('Количество баллов', 'id сотрудника', 'Тип 1) пополнение 2) списание');
*/
function subBalancePoints($count = null, $to = null, $type = null) {
    global $db, $dbName, $user_id, $redis, $logs;

    $root = $_SERVER['DOCUMENT_ROOT'];
    include($root.'/inc/system/redis.php');

    if($type == 1) {
        $result_type = 1;
        $query = "UPDATE `$dbName`.`users` SET `upoints` = upoints + '$count' WHERE  `users`.`uid` = '$to' LIMIT 1 ;";
    } else {
        $result_type = 2;
        $query = "UPDATE `$dbName`.`users` SET `upoints` = upoints - '$count' WHERE  `users`.`uid` = '$to' LIMIT 1 ;";
    }

    if($db->query($query)) {
        $logs->edit_points($user_id, $to, $to, $result_type, $count);

        $q = $db->query("SELECT `upoints` FROM `users` WHERE `uid` = '$to'");
        $d = $db->fetch($q);
        $redis_balance = $d['upoints'];

        $redis->hdel('user_'.$to.'_balance', 'balance');
        $redis->hset('user_'.$to.'_balance', 'balance', $redis_balance);
        $redis->expire('user_'.$to.'_balance', 43200);
    } else {
        return false;
    }

}

/*
 Получение текущего баланса сотрудника
 Пример вызова: getBalancePoints('id сотрудника');
*/
function getBalancePoints($user_id = null) {
    global $db, $redis;

    $root = $_SERVER['DOCUMENT_ROOT'];
    include($root.'/inc/system/redis.php');

    $old_balance = $redis->hget('user_'.$user_id.'_balance', 'balance');
    if($old_balance) {
        return $old_balance;
    } else {
        $q = $db->query("SELECT `upoints` FROM `users` WHERE `uid` = '$user_id'");
        $d = $db->fetch($q);
        $redis_balance = $d['upoints'];

        $redis->hdel('user_'.$user_id.'_balance', 'balance');
        $redis->hset('user_'.$user_id.'_balance', 'balance', $redis_balance);
        $redis->expire('user_'.$user_id.'_balance', 43200);
    }
    return $old_balance;
}


/**
 * @TODO remove it, dup of helpers.php
 */
if(!function_exists('dd')){

    /**
     * @deprecated
     */
    function dd($variable){
        var_dump($variable);
        die();
    }

}