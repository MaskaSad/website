<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$ref = $_SERVER['HTTP_REFERER'];

require($root.'/inc/classes/db.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
?>
<div id="reg_form_whiteh">
 <div id="whiteb_info">
  <div class="system">
   Пожалуйста, <b>заполните все данные</b>. Не вводите пароль от своей страницы ВКонтакте, а придумайте свой, которые будут использоваться только на этом сайте.
  </div>
  <div class="other"></div>
 </div>
 <div id="reg_form_whiteb">
  <div class="label">Логин</div>
  <div class="field"><input id="ureg_login" iplaceholder="Введите логин" type="text"></div>
  <div class="label top">Имя</div>
  <div class="field"><input id="ureg_name" iplaceholder="Введите имя" type="text"></div>
  <div class="label top">Никнейм</div>
  <div class="field"><input id="ureg_nik" iplaceholder="Введите никнейм" type="text"></div>
  <div class="label top">E-mail</div>
  <div class="field"><input id="ureg_email" iplaceholder="Введите e-mail" type="text"></div>

  <div class="label_right">
  <div class="label top">Пароль</div>
  <div class="field"><input id="ureg_password" iplaceholder="Введите пароль" type="password"></div>
  <div class="label top">Фамилия</div>
  <div class="field"><input id="ureg_surname" iplaceholder="Введите фамилию" type="text"></div>
  <div class="label top">Возраст</div>
  <div class="field"><input id="ureg_age" iplaceholder="Сколько вам лет?" type="text"></div>
  <div class="label top">Кто вас пригласил ?</div>
  <div class="field"><input id="ureg_user_ref" iplaceholder="Укажите его никнейм" type="text"></div>
  </div>
  <div onclick="users._post_reg()" id="reg_form_white_breg" class="blue_button_wrap">
   <div class="blue_button">
    Отправить заявку
    <div id="reg_form_white_breg_right"></div>
   </div>
  </div>
 </div>
</div>