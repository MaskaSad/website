<?php
class complaints {
	public function notify_no() {
		global $db, $dbName, $user_id;

		$q = $db->query("UPDATE `$dbName`.`users` SET  `archive` = '0' WHERE  `users`.`uid` = '$user_id' LIMIT 1;");

		return $q;
	}

	public function notify_dle() {
		global $db, $dbName, $user_id;

    $id = $_GET['id'];

		$q = $db->query("UPDATE `$dbName`.`archive` SET `del` = '1' WHERE `archive`.`id` = '$id' LIMIT 1;");

		return $q;
	}

	public function list_table_user_num() {
		global $db, $user_id;

		$q = $db->query("SELECT `id` FROM `archive` WHERE `to` = '$user_id'");
		$n = $db->num($q);

		return $n;
	}

	public function list_table_user() {
		global $db, $user_id;

		$uid = (int)$_GET['uid'];
		$page = (int)$_GET['page'];

		$q = $db->query(
			"SELECT archive.id, archive.url, archive.type, archive.time, archive.upoints, archive.ucategory, archive.module, archive.points_plus, archive.urgent_id, archive.urgent_money
       FROM `archive`
       WHERE archive.to = '$user_id' AND archive.del = '0'
       ORDER BY `id` DESC LIMIT $page, 20"
		 );

		while ($d = $db->fetch($q)) {
			$id = $d['id'];
			$url = $d['url'];
			$type = $d['type'];
			$category = $d['ucategory']; //категория
			$module = $d['module'];
			$module_type = $d['type'];
			$upoints = $d['upoints']; //сколько баллов получил за конвой
			$points_plus = $d['points_plus']; //сколько баллов за срочный заказ получил
			$urgent_id = $d['urgent_id']; //номер срочного заказа
			$time = $d['time']; //время
			$urgent_money = $d['urgent_money'];

			if ($type == 'group') {
				$url_result = 'public' . $url;
			} elseif ($type == 'user') {
				$url_result = 'id' . $url;
			} else {
				$url_result = $type2 . '' . $url;
			}

			if ($category == 1) {
				$category_result = 'Стажёр';
			} elseif ($category == 2) {
				$category_result = 'Младший водитель';
			} elseif ($category == 3) {
				$category_result = 'Старший водитель';
			} elseif ($category == 4) {
				$category_result = 'Специалист';
			} elseif ($category == 5) {
				$category_result = 'Профессионал 1-й категории';
			} elseif ($category == 6) {
				$category_result = 'Профессионал 2-й категории';
			} elseif ($category == 7) {
				$category_result = 'Профессионал 3-й категории';
			} elseif ($category == 8) {
				$category_result = 'Эксперт 1-й категории';
			} elseif ($category == 9) {
				$category_result = 'Эксперт 2-й категории';
			} elseif ($category == 10) {
				$category_result = 'Эксперт 3-й категории';
			} elseif ($category == 11) {
				$category_result = 'Легенда 1-й категории';
			} elseif ($category == 12) {
				$category_result = 'Легенда 2-й категории';
			} elseif ($category == 13) {
				$category_result = 'Легенда 3-й категории';
			} elseif ($category == 14) {
				$category_result = 'Гагарин';
			} else {
				$category_result = '<b style="color:#E26E67;">Испытательный срок</b>';
			}

			if ($module == 1 && $module_type == 1) {
				$template .= '
				<div class="wrap-users" id="notif_id'.$id.'">
				    <div class="admin_page_overflow_control" style="margin-top: 8px;" onclick="users._dle_notif('.$id.');">
				        <div class="icons_tab icons_tab_del1"></div>
				    </div>
				    <div class="inner">
				        <div class="image"><img src="https://vtc-express.ru/images/archive/increase.png"></div>
				        <div class="title">Вы получили новую категорию → <b>'.$category_result.'</b>
				            <div class="rel_date">'.new_time($time).'</div>
				        </div>
				    </div>
				</div>
    ';
			} elseif ($module == 1 && $module_type == 2) {
				$template .= '
				<div class="wrap-users" id="notif_id'.$id.'">
				    <div class="admin_page_overflow_control" style="margin-top: 8px;" onclick="users._dle_notif('.$id.');">
				        <div class="icons_tab icons_tab_del1"></div>
				    </div>
				    <div class="inner">
				        <div class="image"><img src="https://vtc-express.ru/images/archive/plus.png?3"></div>
				        <div class="title">Вам начислено '. $upoints.' ' . declOfNum(abs($upoints), array('балл', 'балла', 'баллов')) . ' за пройденный конвой
				            <div class="rel_date">'.new_time($time).'</div>
				        </div>
				    </div>
				</div>
    ';
			} elseif ($module == 2 && $module_type == 3) {
				$template .= '
				<div class="wrap-users" id="notif_id'.$id.'">
				    <div class="admin_page_overflow_control" style="margin-top: 8px;" onclick="users._dle_notif('.$id.');">
				        <div class="icons_tab icons_tab_del1"></div>
				    </div>
				    <div class="inner">
				        <div class="image"><img src="https://pp.userapi.com/c841328/v841328750/289ba/IzwGUCDtVQg.jpg"></div>
				        <div class="title"><a href="#">Russian Express</a> одобрила вашу заявку по срочному заказу <b>#'.$urgent_id.'</b>
				            <div class="rel_date">'.new_time($time).'</div>
				        </div>
				    </div>
				</div>
    ';
			} elseif ($module == 2 && $module_type == 4) {
				$template .= '
				<div class="wrap-users" id="notif_id'.$id.'">
				    <div class="admin_page_overflow_control" style="margin-top: 8px;" onclick="users._dle_notif('.$id.');">
				        <div class="icons_tab icons_tab_del1"></div>
				    </div>
				    <div class="inner">
				        <div class="image"><img src="https://pp.userapi.com/c841328/v841328750/289ba/IzwGUCDtVQg.jpg"></div>
				        <div class="title"><a href="#">Russian Express</a> отклонила вашу заявку по срочному заказу <b>#'.$urgent_id.'</b>
				            <div class="rel_date">'.new_time($time).'</div>
				        </div>
				    </div>
				</div>
    ';
			}
		}

		return $template ? $template : '<div id="notify_no">Вы еще не получали уведомления.</div>';

	}
}

$complaints = new complaints;
