<?php
class top {
 public function users_list() {
  global $db;

  $act = $db->escape($_GET['act']);

  if($act == 'refs') {
   $q = $db->query("
    SELECT `users`.`uid`, `users`.`uavatar`, `users`.`uvk_id`, `users`.`uname`, `users`.`ulast_name`, `users`.`ulast_time`, `users`.`ugender`,
    COUNT(`ref`.`id`) as `count_refs`
    FROM `ref`
     INNER JOIN `users` ON (`ref`.`to` = `users`.`uid`)
    WHERE `users`.`uban_type` = 0 AND `users`.`uvk_id` > 0
    GROUP BY `ref`.`to`
    ORDER BY `count_refs` DESC
    LIMIT 100
   ");
   $i = 1;
   while($d = $db->assoc($q)) {
	$top_avatar = $d['uavatar'] ? $d['uavatar'] : $noavatar;
    $top_vk_id = $d['uvk_id'];
    $top_first_name = $d['uname'];
    $top_last_name = $d['ulast_name'];
    $top_last_time = $d['ulast_time'];
    $top_gender = $d['ugender'];
    $count_refs = $d['count_refs'];

    if($i == 1) {
     $top_user_num = '<div class="top_user_num_one">'.$i.'</div>';
    } elseif($i == 2) {
     $top_user_num = '<div class="top_user_num_two">'.$i.'</div>';
    } elseif($i == 3) {
     $top_user_num = '<div class="top_user_num_three">'.$i.'</div>';
    } else {
     $top_user_num = '<div class="top_user_num_no">'.$i.'</div>';
    }

    $template .= '
    <div class="top_user_overflow'.($i%2 ? '' : ' top_active').'">
        <div class="top_user_avatar">
            <a><img src="'.$uavatar.'"></a>
        </div>
        <div class="top_user_info">
            <div class="top_user_name"><span style="color:#5e7b9b;font-size:10pt;"><b>'.no_name($top_first_name.' '.$top_last_name).'</b></span> <span class="top_time">заходил'.($top_gender == 2 ? '' : 'а').' '.new_time($top_last_time).'</span></div>
            <div class="top_user_tasks">Пригласил'.($top_gender == 2 ? '' : 'а').' <b>'.$count_refs.' '.declOfNum($count_refs, array('друга', 'друга', 'друзей')).'</b></div>
        </div>
        <div class="top_user_num">'.$top_user_num.'</div>
    </div>
    ';
    $i++;
   }
  } else {
   $q = $db->query("select * from  orders ORDER BY id DESC LIMIT 3");
   $i = 1;
   while($d = $db->assoc($q)) {
    $orders_id = $d['number'];
	$orders_city = $d['city'];
	$orders_time = $d['time'];
	$orders_base = $d['base'];
	$orders_ton = $d['ton'];

	$users_nik = $d['nik'] ? $d['nik'] : 'не указан';
	$uvk_id = $d['uvk_id'];
	$uvk_id_page = $uvk_id ? '<a href="http://vk.com/id'.$uvk_id.'" target="_blank">vk.com/id'.$uvk_id.'</a>' : 'не прикреплена';
	$upoints = $d['upoints'];
    $top_last_name = $d['ulast_name'];
    $top_last_time = $d['ulast_time'];
    $top_gender = $d['ugender'];
	$ugroup = $d['ugroup'];
    $count_tasks_done = $d['count_tasks_done'];

    if($i == 1) {
     $top_user_num = '<div class="top_user_num_one">'.$i.'</div>';
    } elseif($i == 2) {
     $top_user_num = '<div class="top_user_num_two">'.$i.'</div>';
    } elseif($i == 3) {
     $top_user_num = '<div class="top_user_num_three">'.$i.'</div>';
    } else {
     $top_user_num = '<div class="top_user_num_no">'.$i.'</div>';
    }

    $template .= '
    <div class="top_user_overflow">
        <div class="top_user_avatar">
            <a><img src="http://vtc-express.ru/images/agents/agent_avatar1.png"></a>
        </div>
        <div class="top_user_info">
            <div class="top_user_name"><span style="color:#5e7b9b;font-size:10pt;"><b>Срочный заказ </b> <span class="top_time">'.$orders_time.'</span></span>
            </div>
            <div class="subheader">
                <p>
                    Старт: '.$orders_city.' '.$orders_base.'
                    <br> Любой прицеп более '.$orders_ton.'T!
                    <br> Действителен в течении текущего часа!
                </p>
            </div>
        </div>
        <div class="top_user_num">
            <div class="top_user_num_no">#'.$orders_id.'</div>
        </div>
    </div>
    ';
    $i++;
   }
  }
  return $template;
 }
}

$top = new top;
?>
