<?php
class convoys {
	public function add() {
		global $db, $dbName, $user_id, $time, $ugroup, $user_logged, $logs, $convoys;

		if (!$user_logged) {
			return json_encode(array('error_text' => 'login'));
			exit;
		}

		if ($ugroup != 4 && $ugroup != 3) {
			return json_encode(array('access' => 'denied'));
			exit;
		}

		$post_status = $_POST['post_status'];
		$udate_post = $_POST['udate_post'];

		$udate = $_POST['udate'];
		$new_date = strtotime($udate);
		$usbor = $_POST['usbor'];
		$usbor2 = $_POST['usbor2'];
		$ustart = $_POST['ustart'];
		$uoldstop = $_POST['uoldstop'];
        $ustop = $_POST['ustop'];
        $ulong = $_POST['ulong'];
        $uconvoys_desc = $_POST['uconvoys_field_text'];
		$status = $_POST['status'];
		$photo_attaches = $_POST['photo_attaches'];

        if ($post_status == 1) {
            $result_status = "INSERT INTO `$dbName`.`convoys` (`id`, `uid`, `udate`, `usbor`, `usbor2`, `ustart`, `uoldstop`, `ustop`, `utime_add`, `status`, `attaches_photo`, `ulong`, `udesc`, `del`, `add_time`) VALUES (NULL, '$user_id', '$new_date', '$usbor', '$usbor2', '$ustart', '$uoldstop', '$ustop', '$time', '$status', '$photo_attaches', '$ulong', '$uconvoys_desc', '1', '$udate_post');";
        } else {
            $result_status = "INSERT INTO `$dbName`.`convoys` (`id`, `uid`, `udate`, `usbor`, `usbor2`, `ustart`, `uoldstop`, `ustop`, `utime_add`, `status`, `attaches_photo`, `ulong`, `udesc`, `del`, `add_time`) VALUES (NULL, '$user_id', '$new_date', '$usbor', '$usbor2', '$ustart', '$uoldstop', '$ustop', '$time', '$status', '$photo_attaches', '$ulong', '$uconvoys_desc', '0', '0');";
        }

		if($status == 0) {
			$json = array('error_text' => 'Необходимо выбрать тип конвоя.');
		} elseif($photo_attaches < 1) {
			$json = array('error_text' => 'Необходимо загрузить изображения.');
		} elseif($new_date > 0) {
			if($db->query($result_status)) {

				$json = array('success' => 1);
			} else {
				$json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
			}
		}
		return jdecoder(json_encode($json));
	}

	public function check_add() {
		global $db, $dbName;

		$time = date("d.m.y H:i");

		$q = $db->query("SELECT `id` FROM `convoys` WHERE `del` = '1' AND `add_time` = '$time'");
		$d = $db->fetch($q);

		$id = $d['id'];

		if($id) {
            $db->query("UPDATE `$dbName`.`convoys` SET `del` = '0' WHERE `convoys`.`id` = '$id';");

            $json = array('success' => 1);
        } else {
            $json = array('error_text' => 'Ничего не опубликовано.');
        }

		return jdecoder(json_encode($json));
	}

	public function convoys_all($convoys_id = null) {
		global $db, $user_logged, $site_url;

    if($user_logged) {
        $q = $db->query("SELECT * FROM `convoys` INNER JOIN `users` ON convoys.uid = users.uid  WHERE convoys.id = '$convoys_id' AND convoys.status != 2 AND convoys.del = 0 ORDER BY convoys.id DESC");
    } elseif(!$user_logged) {
        $q = $db->query("SELECT * FROM `convoys` INNER JOIN `users` ON convoys.uid = users.uid  WHERE convoys.id = '$convoys_id' AND convoys.status = 3 AND convoys.del = 0 ORDER BY convoys.id DESC");
    }

    while ($d = $db->assoc($q)) {
        $id = $d['id'];
        $udate = $d['udate'];
        $usbor = $d['usbor'];
        $usbor2 = $d['usbor2'];
        $ustart = $d['ustart'];
        $uoldstop = $d['uoldstop'];
        $ustop = $d['ustop'];
        $status = $d['status'];
        $attaches_photo = $d['attaches_photo'];
        $uname = $d['uname'];
        $ulast_name = $d['ulast_name'];
        $uavatar = $d['uavatar'];
        $utime_add = $d['utime_add'];
        $qattaches_photo = normal_id_attaches($attaches_photo);
        $qattaches_photo_list = convoys::convoys_all_images($qattaches_photo);
        $new_date = date("d.m.Y", $udate);
        $ulong = $d['ulong'] ? '<li class="convoys_text_color">Пробег: <b>'.$d['ulong'].' Miles</b></li>' : '';
        $udesc = $d['udesc'] ? '<li class="convoys_text_color">Описание: <b>'.$d['udesc'].'</b></li>' : '';

        if($status == 1) {
            $status_result = 'закрытый конвой';
        } elseif ($status == 3) {
            $status_result = 'открытый конвой';
        } else {
            $status_result = 'открытый конвой';
        }

        $template .= '
        <div class="info-block">
          <div class="convoys_empty">
            <div id="new_top_user" class="convoys_users">
              <div class="convoys_users_avatar">
                <a href="' . $uavatar . '" target="_blank"><img src="' . $uavatar . '"></a>
              </div>
              <div class="convoys_users_last_time">
                ' . new_time($utime_add) . '
              </div>
                    <div class="new_top_other" style="margin-left: 10px;">
                        <div class="convoys_users_full_name">' . $uname . ' ' . $ulast_name . '</div>
                        <div class="convoys_users_info_convoys">' . $status_result . '
                            <div class="convoys_users_info_convoys_list">
                                <ul>
                                    <li class="convoys_text_color">Дата: <b>' . $new_date . '</b></li>
                                    <li class="convoys_text_color">Начало сбора: <b>' . $usbor . '</b></li>
                                    <li class="convoys_text_color">Выезд: <b>' . $usbor2 . '</b></li>
                                    <li class="convoys_text_color">Старт: <b>' . $ustart . '</b></li>
                                    <li class="convoys_text_color">Перерыв: <b>' . $uoldstop . '</b></li>
                                    <li class="convoys_text_color">Финиш: <b>' . $ustop . '</b></li>
                                    '.$ulong.'
                                    '.$udesc.'
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="convoys_img">
                    ' . ($qattaches_photo_list ? $qattaches_photo_list : '
                    <a href="' . $site_url . 'images/error/error_images.png" target="_blank"><img src="' . $site_url . 'images/error/error_images.png"></a>') . '
                </div>
                <div id="info_time">Что-бы посмотреть изображения в большом формате, нажмите на неё</div>
            </div>
        </div>
       ';
        }
        return $template;
	}


	public function convoys_index() {
		global $db, $user_logged;

		$page = (int)abs($_GET['page']) * 3;

    if ($user_logged) {
			$q = $db->query(
				"SELECT * FROM `convoys` INNER JOIN `users` ON convoys.uid = users.uid  WHERE convoys.utime_add > 0 AND convoys.status != 2 AND convoys.del = 0 ORDER BY convoys.id DESC LIMIT $page, 3"
			);
		} elseif (!$user_logged) {
			$q = $db->query(
				"SELECT * FROM `convoys` INNER JOIN `users` ON convoys.uid = users.uid  WHERE convoys.utime_add > 0 AND convoys.status = 3 AND convoys.del = 0 ORDER BY convoys.id DESC LIMIT $page, 3"
			);
		}

		while ($d = $db->assoc($q)) {
			$id = $d['id'];
			$udate = $d['udate'];
      $attaches_photo = $d['attaches_photo'];
			$qattaches_photo = normal_id_attaches($attaches_photo);
			$qattaches_photo_list = convoys::convoys_index_images($qattaches_photo);
			$new_date = date("d.m.Y", $udate);

			$template .= '
			<div class="new_info_company">
					<div class="new_info_inner" style="background-image: url(/'.$qattaches_photo_list.');height: 200px;"></div>
					<div class="new_info_footer">
							<div class="time">
									<div class="date">
											<div id="icon_con"></div>
											<div id="new_time_con">Мероприятие на '.$new_date.'</div>
									</div>
							</div>
							<div class="buttons">
									<div class="blue_button_wrap" onclick="convoys._loading_info('.$id.');">
											<div class="blue_button">Подробнее о мероприятии</div>
									</div>
							</div>
					</div>
			</div>
      ';
		}
		return $template;
	}

	public function convoys_history() {
		global $db;

		$page = (int)abs($_GET['page']) * 10;

		$q = $db->query("SELECT * FROM `convoys` WHERE convoys.utime_add > 0 AND convoys.del = 0 ORDER BY convoys.id DESC LIMIT $page, 10");

		while ($d = $db->assoc($q)) {
			$id = $d['id'];
			$udate = $d['udate'];
			$ustart = $d['ustart'];
			$ustop = $d['ustop'];
			$utime_add = $d['utime_add'];
			$status = $d['status'];
			$new_date = date("d.m.Y", $udate);
            $usbor = $d['usbor'];
            $usbor2 = $d['usbor2'];
            $ulong = $d['ulong'] ? $d['ulong'] : 0;

			if ($status == 1) {
				$status_result = 'закрытый конвой';
			} elseif ($status == 2) {
				$status_result = 'открытая покатушка';
			} elseif ($status == 3) {
				$status_result = 'открытый конвой';
			} else {
				$status_result = 'открытый конвой';
			}

			$template .= '    
      <div class="item" id="convoysids'.$id.'">
         <div class="clearfix">
           <div class="fl_l title">
             <span>Мероприятие</span> <i>Номер #'.$id.'</i>
           </div>
           <div class="fl_l start">
             <div class="count">Дата</div>
             <div class="label">'.$new_date.'</div>
           </div>
           <div class="fl_l trailer">
             <div class="count">Сбор</div>
             <div class="label">'.$usbor.'</div>
           </div>
           <div class="fl_l time">
             <div class="count">Выезд</div>
             <div class="label">'.$usbor2.'</div>
           </div>
           <div class="fl_l time">
             <div class="count">Пробег</div>
             <div class="label">'.$ulong.' км</div>
           </div>
           <div class="fl_r control_buttons">
             <div class="fl_r" id="request_icon"><div class="requests_icons ed" onclick="nav.go(\'\', \'/editconvoys'.$id.'\');"></div><div class="requests_icons no" onclick="convoys._delete('.$id.');"></div></div>
           </div>
         </div>
       </div>
   ';
		}
		return $template;
	}

	public function edit_convoys() {
		global $db, $ugroup, $user_logged;

		if (!$user_logged) {
			return 'login';
			exit;
		}

		if ($ugroup != 4) {
			return 'Access Denied';
			exit;
		}

		$id = (int)$_GET['id'];

		$q = $db->query("SELECT * FROM `convoys` WHERE `id` = '$id'");
		$d = $db->fetch($q);

		$udate = $d['udate'];
		$usbor = $d['usbor'];
		$usbor2 = $d['usbor2'];
		$ustart = $d['ustart'];
		$uoldstop = $d['uoldstop'];
		$ustop = $d['ustop'];
		$utime_add = $d['utime_add'];
        $status = $d['status'];
		$attaches_photo = $d['attaches_photo'];
        $ulong = $d['ulong'];
        $udesc = $d['udesc'];
        $add_time = $d['add_time'];
		$new_date = date("d.m.Y", $udate);

		if($add_time) {
		    $add_time_text = '<div class="legend pad">Дата публикации:</div><div class="field"><input type="text" id="udate_post" iplaceholder="24.01.18 08:00" value="'.$add_time.'"></div>';
        } else {
            $add_time_text = '';
        }

        $template .= '
                <div id="convoy_add">
                 <div id="convoys_add_inner">
                     <div id="task_add_error" class="error_msg error"></div>
                     <div class="legend">Дата проведения:</div>
                     <div class="field"><input type="text" id="udate" iplaceholder="23.01.2018" value="'.$new_date.'"></div>
                     <div class="legend pad">Сбор/Выезд:</div>
                     <div class="field"><input type="text" id="usbor" style="width: 148px;" iplaceholder="13:00" value="'.$usbor.'"><span style="margin: 0 5px 0 5px;">:</span><input type="text" id="usbor2" style="width: 148px;" iplaceholder="13:30" value="'.$usbor2.'"></div>
                     <div class="legend pad">Место старта:</div>
                     <div class="field"><input type="text" id="ustart" iplaceholder="Lyon(TRADE-AuX)" value="'.$ustart.'"></div>
                     <div class="legend pad">Место отдыха:</div>
                     <div class="field"><input type="text" id="uoldstop" iplaceholder="Leipzig(Tre-eT)" value="'.$uoldstop.'"></div>
                     <div class="legend pad">Место финиша:</div>
                     <div class="field"><input type="text" id="ustop" iplaceholder="Dortmund(карьер)" value="'.$ustop.'"></div>
                     <div class="legend pad">Протяженность маршрута:</div>
                     <div class="field"><input type="text" id="ulong" iplaceholder="2588" value="'.$ulong.'"></div>
                     <div class="legend pad">Тип:</div>
                     <div class="field"><div id="convoys_add"></div></div>
                     <div class="field"><div class="legend pad">Небольшое описание конвоя:</div><div class="field"><textarea iplaceholder="Небольшое описание конвоя..." id="uconvoys_field_text">'.$udesc.'</textarea></div>'.$add_time_text.'</div>
                     <div class="field"><div onclick="convoys._save_convoys('.$id.')" id="add_task_button" class="blue_button_wrap"><div class="blue_button">Сохранить изменения</div></div></div>
                     <input type="hidden" id="old_select_convoys_add_value" value="'.$status.'">
                 </div>
             </div>
        ';

   return $template;
 }

	public function edit_convoys_post() {
		global $db, $dbName, $user_id, $user_logged, $ugroup, $logs;

		if (!$user_logged) {
			return json_encode(array('error_text' => 'login'));
			exit;
		}

		if ($ugroup != 4) {
			return json_encode(array('access' => 'denied'));
			exit;
		}

		$id = (int) $_POST['id'];
		$udate = $_POST['udate'];
        $new_date = strtotime($udate);
		$usbor = $_POST['usbor'];
		$usbor2 = $_POST['usbor2'];
		$ustart = $_POST['ustart'];
		$uoldstop = $_POST['uoldstop'];
		$ustop = $_POST['ustop'];
		$ulong = $_POST['ulong'];
        $status = $_POST['status'];
        $new_status = $status ? $status : 1;
        $desc = $db->escape($_POST['desc']);
        $udate_post = $_POST['udate_post'];

        if($id) {
            $db->query("UPDATE `$dbName`.`convoys` SET  `udate` =  '$new_date', `usbor` =  '$usbor', `usbor2` =  '$usbor2', `ustart` =  '$ustart', `uoldstop` =  '$uoldstop', `ustop` =  '$ustop', `status` =  '$new_status', `ulong` =  '$ulong', `udesc` =  '$desc', `add_time` =  '$udate_post' WHERE `convoys`.`id` = '$id';");

            $json = array('success' => 1);
        } else {
            $json = array('error_msg' => 'Ошибка соединения с сервером. Попробуйте позже.');
        }

		return json_encode($json);
	}

    public function delete_convoys() {
        global $db, $dbName, $user_id, $user_logged, $logs, $ugroup;

        $id = (int)abs($_GET['id']);

        if (!$user_logged) {
            return json_encode(array('error_text' => 'login'));
            exit;
        }

        if ($ugroup != 4 && $ugroup != 3) {
            return json_encode(array('access' => 'denied'));
            exit;
        }

        $q = $db->query("SELECT `id` FROM `convoys` WHERE `id` = '$id' AND `del` = '0'");
        $d = $db->fetch($q);

        if ($d['id']) {
            if ($db->query("UPDATE `$dbName`.`convoys` SET `del` =  '1' WHERE  `convoys`.`id` = '$id' LIMIT 1;")) {
                $logs->convoys_del_post($user_id, $id);
                $json = array('success' => 1, 'id' => $id);
            } else {
                $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
            }
        } else {
            $json = array('error_text' => 'Ошибка доступа.');
        }
        return jdecoder(json_encode($json));
    }

	public function img_upload()
	{
		global $db, $dbName, $root, $picture, $user_logged, $user_id, $time;

		if (!$user_logged) {
			return json_encode(array('error_text' => 'login'));
			exit;
		}

		$__dir = $root . '/images/uploads/convoys/';
		$__format_explode = explode('.', $_FILES['file']['name']);
		$__format = $__format_explode[count($__format_explode) - 1]; // получаем формат изображения
		$__format_type = array('jpeg', 'JPEG', 'jpg', 'JPG', 'png', 'PNG', 'gif', 'GIF', 'bmp', 'BMP');
		$__file_rand_name = rand_str(10);
		$__file_mini_rand_name = rand_str(15);
		$__file_name = $__dir . '' . $__file_rand_name . '.jpg';

  // открываем последние загруженные изображения
		$qattach_img_limit = time() - (30 * 60);
		$qattach_img = $db->query("SELECT `id` FROM `attaches_img` WHERE `time` >= '$qattach_img_limit' AND `uid` = '$user_id'");
		$nattach_img = $db->num($qattach_img);

		if ($nattach_img >= 30) {
			$json = array(
				'error_text' => 'Вы загружаете слишком много изображений. Попробуйте позже.'
			);
		} elseif (!in_array($__format, $__format_type)) {
			$json = array('error_text' => 'Неизвестный формат изображения.');
		} else {
			if (copy($_FILES['file']['tmp_name'], $__file_name)) {
    // если файл загружен, то создаем уменьшенное изображение
				$new_image = new picture($__dir . '/' . $__file_rand_name . '.jpg');
				$new_image->autoimageresize(75, 75);
				$new_image->imagesave('jpeg', $__dir . '/' . $__file_mini_rand_name . '.jpg');
				$new_image->imageout();
				$db->query("INSERT INTO `$dbName`.`attaches_img` (`id`, `uid`, `small`, `big`, `time`, `modules`) VALUES ('', '$user_id', '$__file_mini_rand_name', '$__file_rand_name', '$time', '13');");
    // получаем id загруженного изображения
				$qImg = $db->query("SELECT `id` FROM `attaches_img` WHERE `small` = '$__file_mini_rand_name' AND `uid` = '$user_id'");
				$dImg = $db->fetch($qImg);
				$json = array(
					'success' => 1, 'id' => $dImg['id'], 'result_big_file' => $__file_rand_name . '.jpg', 'result_mini_file' => $__file_mini_rand_name . '.jpg'
				);
			} else {
				$json = array('error_text' => 'Неизвестная ошибка.');
			}
		}

		return json_encode($json);
	}

	public function convoys_all_images($list = null) {
		global $db, $site_url;
  //изображения конвоев
		$query_attaches_photo = $db->query(
			"SELECT `small`, `big` FROM `attaches_img` WHERE `id` IN(" . $db->escape($list) . ") AND `modules` = 13 ORDER BY `id` ASC"
		);
		while ($data_attaches_photo = $db->fetch($query_attaches_photo)) {
			$attaches_img_result .= '<a href="' . $site_url . 'images/uploads/convoys/' . $data_attaches_photo['big'] . '.jpg" target="_blank"><img src="' . $site_url . 'images/uploads/convoys/' . $data_attaches_photo['big'] . '.jpg"></a>';
		}
		return $attaches_img_result ? '' . $attaches_img_result . '' : '';
	}

	public function convoys_index_images($list = null) {
		global $db, $site_url;

		$query_attaches_photo = $db->query(
			"SELECT `small`, `big` FROM `attaches_img` WHERE `id` = '$list' AND `modules` = 13 ORDER BY `id` ASC"
		);
		while ($data_attaches_photo = $db->fetch($query_attaches_photo)) {
			$attaches_img_result .= 'images/uploads/convoys/'.$data_attaches_photo['big'].'.jpg';
		}
		return $attaches_img_result ? '' . $attaches_img_result . '' : '';
	}

	public function send_group_users()
	{
		global $db, $user_id, $user_logged;

		$token = "8a4bccfed5ea16433f9a0372f2214d5cecab6906c9b2001a8149cc1a76ffeece5c262fe3b044bdbd09843";
		$q = file_get_contents(
			"https://api.vk.com/method/wall.post?owner_id=-117117618&attachment=https://vtc-express.ru/convoys&from_group=1&message=&access_token=" . $token
		);

		return $q;
	}

	public function convoys_num() {
		global $db, $user_id, $user_logged;

		if ($user_logged) {
			$q = $db->query("SELECT * FROM convoys WHERE status = 1 AND del = 0");
		} else {
			$q = $db->query("SELECT * FROM convoys WHERE status = 3 AND del = 0");
		}

		$n = $db->num($q);

		return $n;
	}

	public function convoys_ride_num() {
		global $db, $user_id;

		$q = $db->query("SELECT * FROM convoys WHERE status = 2 AND del = 0");
		$n = $db->num($q);

		return $n;
	}
	public function convoys_history_num() {
		global $db, $user_id;

		$q = $db->query("SELECT * FROM convoys WHERE del = 0");
		$n = $db->num($q);

		return $n;
	}
	public function convoys_history_num_post() {
		global $db, $user_id;

		$q = $db->query("SELECT * FROM pause_convoys WHERE del = 0");
		$n = $db->num($q);

		return $n;
	}
}

$convoys = new convoys;
