<?php
class urgent
{
	public function voutes()
	{
		global $db, $redis, $logs, $dbName, $user_logged, $user_id, $uvk_id, $users_nik;

		$count = $_POST['count'];
		$count_nik = $_POST['count_nik'];
		$photo_attaches = $_POST['photo_attaches'];
		$penalty_id = $_POST['uid'];
		$upoints = $_POST['upoints'];
		$money = $_POST['money'];

		if (!$user_logged) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif (!$users_nik) {
			$error = array(
				'error_msg' => 'Необходимо  указать <b><a style="color: #bd0000;" href="/settings" onclick="nav.go(this); return false;">Никнейм</a></b> в настройках.'
			);
		} elseif ($photo_attaches < 1) {
			$error = array('error_msg' => 'Необходимо загрузить скриншот.');
		} elseif ($upoints < 1) {
			$error = array('error_msg' => 'Ошибка #1');
		} elseif ($count < 1) {
			$error = array('error_msg' => 'Необходимо указать ваш пробег.');
		} elseif ($money < 1) {
			$error = array('error_msg' => 'Необходимо указать ваш заработок.');
		} else {
			if ($db->query(
				"INSERT INTO `$dbName`.`penalty` (`id`, `user_id`, `vk_id`, `nik`, `count`, `points`, `time`, `status`, `no_active`, `attaches_photo`, `points_fine`, `from`, `penalty_id`, `money`) VALUES (NULL, '$user_id', '$uvk_id', '$count_nik', '$count', '$count', '" . time() . "', '1', '0', '$photo_attaches', '$upoints', '1', '$penalty_id', '$money');"
			)) {
				$db->query("UPDATE `$dbName`.`complaints` SET `type` = '1' WHERE `complaints`.`id` = '$penalty_id';");
				$redis->set('penalty_pay_id' . $unique_key . '_amount', 0);

				$counter = $db->query("SELECT COUNT(*) as `count` FROM `penalty` WHERE `no_active` = '0'");
				$counter = $db->assoc($counter);

				$counter = $counter['count'];

				$redis->set('admin_penalty_counter_left', $counter);

				$logs->urgent_request($user_id, $points);

				$error = array('response' => 1, 'count' => $count, 'points' => $points);
			} else {
				$error = array('error_msg' => 'Ошибка соединения с сервером. Попробуйте позже.');
			}
		}

		return json_encode($error);
	}

	public function user_requests()
	{
		global $db, $user_id;

		$q = $db->query("SELECT `count`, `points`, `status`, `attaches_photo` FROM `penalty` WHERE `user_id` = '$user_id' ORDER BY `id` DESC");

		while ($d = $db->assoc($q)) {
			$points = $d['points'];
			$status = $d['status'];
			$qattaches_photo = normal_id_attaches($d['attaches_photo']);
			$qattaches_photo_list = urgent::urgent_comment_attaches_url($qattaches_photo, $user_id);

			switch ($status) {
				case 1:
					$status_title = 'Рассматривается..';
					$status_class = 'wait';

					break;

				case 2:
					$status_title = 'Одобрено';
					$status_class = 'ok';

					break;

				case 3:
					$status_title = 'Отклонено';
					$status_class = 'error';

					break;

				case 4:
					$status_title = 'Штраф оплачен';
					$status_class = 'working';

					break;
			}

			$template .= '
      <div class="block" style="margin-right: 9px;width: 97%;margin-top: 15px;margin-bottom: -5px;">
          <div class="price_' . $status_class . '">' . $status_title . '</div>
          <div class="time">Отправка заявки</div>
          <ul style="padding-bottom: 0;">
              <li>Проехал км: <b>' . $points . ' км</b></li>
              <li>Скриншот: ' . $qattaches_photo_list . '</li>
          </ul>
      </div>
   ';
		}

		return $template;
	}

	public function admin_requests()
	{
		global $db, $site_url;

		$list_section = $_GET['section'];

		if ($list_section == 'deleted') {
			$delete_sql = 1;
			$status_sql = 'AND status = "3"';
		} elseif ($list_section == 'considered') {
			$delete_sql = 1;
			$status_sql = 'AND status = "2"';
		} else {
			$delete_sql = 0;
		}

		$user_ids = array();
		$requests = array();

		$users = array();

		$q = $db->query(
			"
   SELECT penalty.id, penalty.user_id, penalty.vk_id, penalty.nik, penalty.count, penalty.points, penalty.time, penalty.status, penalty.attaches_photo, penalty.points_fine, penalty.from, penalty.to, penalty.browser, penalty.ip, penalty.constime, penalty.no_active, penalty.penalty_id, penalty.money, users.uname, users.ulast_name
   FROM `penalty`
    INNER JOIN `users` ON penalty.from = users.uid
   WHERE penalty.no_active = '$delete_sql' " . $status_sql . "
   ORDER BY penalty.id DESC
  "
		);

		while ($d = $db->assoc($q)) {
			$user_ids[] = $d['user_id'];

			$requests[] = array(
				'id' => $d['id'], 'user_id' => $d['user_id'], 'vk_id' => $d['vk_id'], 'nik' => $d['nik'], 'count' => $d['count'], 'points' => $d['points'], 'time' => $d['time'], 'status' => $d['status'], 'attaches_photo' => $d['attaches_photo'], 'points_fine' => $d['points_fine'], 'browser' => $d['browser'], 'ip' => $d['ip'], 'constime' => $d['constime'], 'penalty_id' => $d['penalty_id'], 'money' => $d['money'], 'uname' => $d['uname'], 'ulast_name' => $d['ulast_name'], 'from' => $d['from']
			);
		}

		$users_get = $db->query(
			"SELECT `uid`, `uvk_id`, `uname`, `ulast_name`, `uavatar`, `nik` FROM `users` WHERE `uid` IN(" . implode(',', $user_ids) . ")"
		);

		while ($users_data = $db->assoc($users_get)) {
			$user_data_user_id = $users_data['uid'];
			$user_data_vk_id = $users_data['uvk_id'];
			$user_data_first_name = $users_data['uname'];
			$user_data_last_name = $users_data['ulast_name'];
			$user_data_avatar = $users_data['uavatar'];
			$user_data_nik = $users_data['nik'];

			$users[$user_data_user_id] = array(
				'user_id' => $user_data_user_id, 'vk_id' => $user_data_vk_id, 'first_name' => $user_data_first_name, 'last_name' => $user_data_last_name,
				'avatar' => $user_data_avatar, 'nik' => $user_data_nik
			);
		}

		for ($i = 0; $i < count($requests); $i++) {
			$request_id = $requests[$i]['id'];
			$request_user_id = $requests[$i]['user_id'];
			$request_penalty_id = $requests[$i]['penalty_id'];
			$request_points = $requests[$i]['points'];
			$request_time = $requests[$i]['time'];
			$qattaches_photo = normal_id_attaches($requests[$i]['attaches_photo']);
			$qattaches_photo_list = urgent::urgent_comment_attaches($qattaches_photo, $request_user_id);
			$points_fine = $requests[$i]['points_fine'];
			$constime = $requests[$i]['constime'];
			$uid_from = $requests[$i]['from'];
			$uname = $requests[$i]['uname'];
			$ulast_name = $requests[$i]['ulast_name'];
			$ufull_name = $uname ? $uname . ' ' . $ulast_name : 'Безымянный';
            $request_nik = $users[$request_user_id]['nik'];
			$request_users_nik = $request_nik ? $request_nik : 'Аккаунт удалён';
			$money = $requests[$i]['money'];

			if ($list_section == 'considered') {

        $template .= '
        <div class="item" id="admin_exchange_requests'.$request_id.'">
         <div class="clearfix">
           <div class="fl_l title">
             <span>'.$request_users_nik.'</span> <i>Кол-во баллов: '.$points_fine.' '.declOfNum($points_fine, array('балл', 'балла', 'баллов')).'</i>
           </div>
           <div class="fl_l start">
             <div class="count">Проехал</div>
             <div class="label">'.$request_points.' км</div>
           </div>
           <div class="fl_l trailer">
             <div class="count">Заработал</div>
             <div class="label">'.$money.' €</div>
           </div>
           <div class="fl_l time">
             <div class="count">Отправил</div>
             <div class="label">'.new_time($request_time).'</div>
           </div>
           <div class="fl_r control_buttons">
             <div class="question_attaches_img_result" style="margin-right: 10px;">
               '.$qattaches_photo_list.'
             </div>
           </div>
         </div>
         <div id="admin_considered" style="margin-top: 10px;"><a href="/admin/modules/users/?search='.$site_url.'id'.$uid_from.'" target="_blank"><b>'.$ufull_name.'</b></a> рассмотрел заявку <span style="color:grey;">'.new_time($constime).'</span></div>
       </div>
    ';
			} elseif ($list_section == 'deleted') {
				$template .= '
        <div class="item" id="admin_exchange_requests'.$request_id.'">
         <div class="clearfix">
           <div class="fl_l title">
             <span>'.$request_users_nik.'</span> <i>Кол-во баллов: '.$points_fine.' '.declOfNum($points_fine, array('балл', 'балла', 'баллов')).'</i>
           </div>
           <div class="fl_l start">
             <div class="count">Проехал</div>
             <div class="label">'.$request_points.' км</div>
           </div>
           <div class="fl_l trailer">
             <div class="count">Заработал</div>
             <div class="label">'.$money.' €</div>
           </div>
           <div class="fl_l time">
             <div class="count">Отправил</div>
             <div class="label">'.new_time($request_time).'</div>
           </div>
           <div class="fl_r control_buttons">
             <div class="question_attaches_img_result" style="margin-right: 10px;">
               '.$qattaches_photo_list.'
             </div>
           </div>
         </div>
         <div id="admin_considered" style="margin-top: 10px;"><a href="/admin/modules/users/?search='.$site_url.'id'.$uid_from.'" target="_blank"><b>'.$ufull_name.'</b></a> отклонил заявку <span style="color:grey;">'.new_time($constime).'</span></div>
       </div>
    ';
			} else {
				$template .= '
        <div class="item" id="admin_exchange_requests'.$request_id.'">
         <div class="clearfix">
           <div class="fl_l title">
             <span>'.$request_users_nik.'</span> <i>Кол-во баллов: '.$points_fine.' '.declOfNum($points_fine, array('балл', 'балла', 'баллов')).'</i>
           </div>
           <div class="fl_l start">
             <div class="count">Проехал</div>
             <div class="label">'.$request_points.' км</div>
           </div>
           <div class="fl_l trailer">
             <div class="count">Заработал</div>
             <div class="label">'.$money.' €</div>
           </div>
           <div class="fl_l time">
             <div class="count">Отправил</div>
             <div class="label">'.new_time($request_time).'</div>
           </div>
           <div class="fl_r control_buttons">
             <div class="question_attaches_img_result" style="margin-right: 10px;">
               '.$qattaches_photo_list.'
             </div>
             <div class="fl_r" id="request_icon"><div class="requests_icons ok"  onclick="penalty.set_status('.$request_id.', 2, '.$request_penalty_id.')"></div><div class="requests_icons no" onclick="penalty.set_status('.$request_id.', 3, '.$request_penalty_id.')"></div></div>
              <input type="hidden" value="'.$request_penalty_id.'" id="penalty_id'.$request_penalty_id.'">
           </div>
         </div>
       </div>
   ';
			}
		}
		return (($template) ? $template : '<div id="tasks_none"><div id="text_no">Не найдено ни одной заявки.</div></div>');
	}

	public function admin_set_status()
	{
		global $db, $redis, $user_id, $dbName, $user_logged, $ugroup, $browser, $ip_address, $time;

		$uid = (int)$_POST['uid'];
		$id = (int)$_GET['id'];
		$status = (int)$_GET['status'];
		$penalty_id = (int)$_GET['penalty_id'];

		$q = $db->query(
			"SELECT `id`, `user_id`, `points`, `status`, `points_fine`, `count`, `money` FROM `penalty` WHERE `id` = '$id' AND `no_active` = '0'"
		);
		$d = $db->assoc($q);

		$user_id2 = $d['user_id'];
		$points_fine = $d['points_fine'];
		$mile = $d['count'];
		$money = $d['money'];

		if (!$user_logged) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif ($ugroup != 4) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif ($d['id']) {
			if ($db->query("UPDATE `$dbName`.`penalty` SET `status` = '$status', `no_active` = '1' WHERE `penalty`.`id` = '$id';")) {
				$counter = $db->query("SELECT COUNT(*) as `count` FROM `penalty` WHERE `no_active` = '0'");
				$counter = $db->assoc($counter);

				$counter = $counter['count'];

				$redis->set('admin_penalty_counter_left', $counter);

				if ($status == 2) {
					$db->query(
						"UPDATE `$dbName`.`users` SET `upoints_fine` = `upoints_fine` - '$points_fine', `mile` = `mile` + '$mile', `money` = `money` + '$money' WHERE  `users`.`uid` = '$user_id2';"
					);
					$db->query(
						"UPDATE `$dbName`.`penalty` SET `from` = '$user_id', `to` = '$user_id2', `browser` = '$browser', `ip` = '$ip_address', `constime` = '$time' WHERE `penalty`.`id` = '$id';"
					);
					$db->query("UPDATE `$dbName`.`complaints` SET `type` = '2' WHERE `complaints`.`id` = '$penalty_id';");
				} elseif ($status == 3) {
					$db->query(
						"UPDATE `$dbName`.`penalty` SET `from` = '$user_id', `to` = '$user_id2', `browser` = '$browser', `ip` = '$ip_address', `constime` = '$time' WHERE `penalty`.`id` = '$id';"
					);
					$db->query("UPDATE `$dbName`.`complaints` SET `type` = '3' WHERE `complaints`.`id` = '$penalty_id';");
				}

				$error = array('response' => 1);
			} else {
				$error = array('error_msg' => 'Ошибка соединения с сервером. Попробуйте позже.');
			}
		} else {
			$error = array('error_msg' => 'Ошибка доступа.');
		}

		return json_encode($error);
	}

	public function voutes_appeal()
	{
		global $db, $redis, $logs, $dbName, $user_logged, $user_id, $uvk_id;

		$penalty_id = $_POST['uid'];
		$upoints = $_POST['upoints'];
		$admin_text = $_POST['admin_text'];

		if (!$user_logged) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} else {
			if ($db->query(
				"INSERT INTO `$dbName`.`appeal` (`id`, `user_id`, `vk_id`, `nik`, `count`, `points`, `time`, `status`, `no_active`, `attaches_photo`, `points_fine`, `from`, `penalty_id`, `text`) VALUES (NULL, '$user_id', '$uvk_id', '0', '3', '3', '" . time() . "', '1', '0', '0', '$upoints', '1', '$penalty_id', '$admin_text');"
			)) {
				$db->query("UPDATE `$dbName`.`complaints` SET `type` = '1' WHERE `complaints`.`id` = '$penalty_id';");

				$counter = $db->query("SELECT COUNT(*) as `count` FROM `appeal` WHERE `no_active` = '0'");
				$counter = $db->assoc($counter);

				$counter = $counter['count'];

				$redis->set('admin_penalty_appeal_counter_left', $counter);

				$logs->urgent_request($user_id, $points);

				$error = array('response' => 1, 'count' => $count, 'points' => $points);
			} else {
				$error = array('error_msg' => 'Ошибка соединения с сервером. Попробуйте позже.');
			}
		}

		return json_encode($error);
	}

	public function admin_requests_appeal()
	{
		global $db, $site_url;

		$list_section = $_GET['section'];

		if ($list_section == 'deleted') {
			$delete_sql = 1;
			$status_sql = 'AND status = "3"';
		} elseif ($list_section == 'considered') {
			$delete_sql = 1;
			$status_sql = 'AND status = "2"';
		} else {
			$delete_sql = 0;
		}

		$user_ids = array();
		$requests = array();

		$users = array();

		$q = $db->query(
			"
   SELECT appeal.id, appeal.user_id, appeal.vk_id, appeal.nik, appeal.count, appeal.points, appeal.time, appeal.status, appeal.attaches_photo, appeal.points_fine, appeal.from, appeal.to, appeal.browser, appeal.ip, appeal.constime, appeal.no_active, appeal.penalty_id, appeal.text, users.uname, users.ulast_name
   FROM `appeal`
    INNER JOIN `users` ON appeal.from = users.uid
	LEFT OUTER JOIN `complaints` ON complaints.id = appeal.user_id
   WHERE appeal.no_active = '$delete_sql' " . $status_sql . "
   ORDER BY appeal.id DESC
  "
		);

		while ($d = $db->assoc($q)) {
			$user_ids[] = $d['user_id'];

			$requests[] = array(
				'id' => $d['id'], 'user_id' => $d['user_id'], 'vk_id' => $d['vk_id'], 'nik' => $d['nik'], 'count' => $d['count'], 'points' => $d['points'], 'time' => $d['time'], 'status' => $d['status'], 'attaches_photo' => $d['attaches_photo'], 'points_fine' => $d['points_fine'], 'browser' => $d['browser'], 'ip' => $d['ip'], 'constime' => $d['constime'], 'penalty_id' => $d['penalty_id'], 'uname' => $d['uname'], 'ulast_name' => $d['ulast_name'], 'from' => $d['from'], 'text' => $d['text']
			);
		}

		$users_get = $db->query(
			"SELECT `uid`, `uvk_id`, `uname`, `ulast_name`, `uavatar`, `nik` FROM `users` WHERE `uid` IN(" . implode(',', $user_ids) . ")"
		);

		while ($users_data = $db->assoc($users_get)) {
			$user_data_user_id = $users_data['uid'];
			$user_data_vk_id = $users_data['uvk_id'];
			$user_data_first_name = $users_data['uname'];
			$user_data_last_name = $users_data['ulast_name'];
			$user_data_avatar = $users_data['uavatar'];
			$user_data_nik = $users_data['nik'];

			$users[$user_data_user_id] = array(
				'user_id' => $user_data_user_id, 'vk_id' => $user_data_vk_id, 'first_name' => $user_data_first_name, 'last_name' => $user_data_last_name,
				'avatar' => $user_data_avatar, 'nik' => $user_data_nik
			);
		}

		for ($i = 0; $i < count($requests); $i++) {
			$request_id = $requests[$i]['id'];
			$request_user_id = $requests[$i]['user_id'];
			$request_penalty_id = $requests[$i]['penalty_id'];
			$points_fine = $requests[$i]['points_fine'];
			$constime = $requests[$i]['constime'];
			$uid_from = $requests[$i]['from'];
			$uname = $requests[$i]['uname'];
			$ulast_name = $requests[$i]['ulast_name'];
			$text_admin = $requests[$i]['text'];
			$ufull_name = $uname ? $uname . ' ' . $ulast_name : 'Безымянный';
            $request_nik = $users[$request_user_id]['nik'];
            $request_users_nik = $request_nik ? $request_nik : 'Аккаунт удалён';

			if ($list_section == 'considered') {
				$template .= '
       <div class="item" id="admin_exchange_requests'.$request_id.'">
         <div class="clearfix">
           <div class="fl_l title">
             <span>'.$request_users_nik.'</span> <i>Подал на апелляцию</i>
           </div>
           <div class="fl_l start">
             <div class="count">Кол-во баллов:</div>
             <div class="label">'.$points_fine. ' '.declOfNum($points_fine, array('балл', 'балла', 'баллов')).'</div>
           </div>
           <div class="fl_l trailer" style="width: 320px;">
             <div class="count">Причина администратора:</div>
             <div class="label">'.$text_admin.'</div>
           </div>
         </div>
         <div id="admin_considered" style="margin-top: 10px;"><a href="/admin/modules/users/?search='.$site_url.'id'.$uid_from.'" target="_blank"><b>'.$ufull_name.'</b></a> рассмотрел заявку <span style="color:grey;">'.new_time($constime).'</span></div>
       </div>
    ';
			} elseif ($list_section == 'deleted') {
				$template .= '
        <div class="item" id="admin_exchange_requests'.$request_id.'">
         <div class="clearfix">
           <div class="fl_l title">
             <span>'.$request_users_nik.'</span> <i>Подал на апелляцию</i>
           </div>
           <div class="fl_l start">
             <div class="count">Кол-во баллов:</div>
             <div class="label">'.$points_fine. ' '.declOfNum($points_fine, array('балл', 'балла', 'баллов')).'</div>
           </div>
           <div class="fl_l trailer" style="width: 320px;">
             <div class="count">Причина администратора:</div>
             <div class="label">'.$text_admin.'</div>
           </div>
         </div>
         <div id="admin_considered" style="margin-top: 10px;"><a href="/admin/modules/users/?search='.$site_url.'id'.$uid_from.'" target="_blank"><b>'.$ufull_name.'</b></a> отклонил заявку <span style="color:grey;">'.new_time($constime).'</span></div>
       </div>
    ';
			} else {
				$template .= '
       <div class="item" id="admin_exchange_requests'.$request_id.'">
         <div class="clearfix">
           <div class="fl_l title">
             <span>'.$request_users_nik.'</span> <i>Подал на апелляцию</i>
           </div>
           <div class="fl_l start">
             <div class="count">Кол-во баллов:</div>
             <div class="label">'.$points_fine. ' '.declOfNum($points_fine, array('балл', 'балла', 'баллов')).'</div>
           </div>
           <div class="fl_l trailer" style="width: 320px;">
             <div class="count">Причина администратора:</div>
             <div class="label">'.$text_admin.'</div>
           </div>
           <div class="fl_r control_buttons">
             <div class="fl_r" id="request_icon"><div class="requests_icons ok" onclick="penalty.set_appeal('.$request_id.', 2, '.$request_penalty_id.')"></div><div class="requests_icons no" onclick="penalty.set_appeal('.$request_id.', 3, '.$request_penalty_id.')"></div></div>
              <input type="hidden" value="'.$request_penalty_id.'" id="penalty_id'.$request_penalty_id.'">
           </div>
         </div>
       </div>
   ';
			}
		}
		return (($template) ? $template : '<div id="tasks_none"><div id="text_no">Не найдено ни одной заявки.</div></div>');
	}

	public function admin_set_status_appeal()
	{
		global $db, $redis, $logs, $user_id, $dbName, $user_logged, $ugroup, $urgent_one_voute, $exchange_one_voute, $browser, $ip_address, $time;

		$id = (int)$_GET['id'];
		$status = (int)$_GET['status'];
		$penalty_id = (int)$_GET['penalty_id'];

		$q = $db->query("SELECT `id`, `user_id`, `points`, `status`, `points_fine` FROM `appeal` WHERE `id` = '$id' AND `no_active` = '0'");
		$d = $db->assoc($q);

		$user_id2 = $d['user_id'];
		$points_fine = $d['points_fine'];

		if (!$user_logged) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif ($ugroup != 4) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif ($d['id']) {
			if ($db->query("UPDATE `$dbName`.`appeal` SET `status` = '$status', `no_active` = '1' WHERE `appeal`.`id` = '$id';")) {
				$counter = $db->query("SELECT COUNT(*) as `count` FROM `appeal` WHERE `no_active` = '0'");
				$counter = $db->assoc($counter);

				$counter = $counter['count'];

				$redis->set('admin_penalty_appeal_counter_left', $counter);

				if ($status == 2) {
					$db->query("UPDATE `$dbName`.`users` SET `upoints_fine` = `upoints_fine` - '$points_fine' WHERE  `users`.`uid` = '$user_id2';");
					$db->query(
						"UPDATE `$dbName`.`appeal` SET `from` = '$user_id', `to` = '$user_id2', `browser` = '$browser', `ip` = '$ip_address', `constime` = '$time' WHERE `appeal`.`id` = '$id';"
					);
					$db->query("UPDATE `$dbName`.`complaints` SET `type` = '5' WHERE `complaints`.`id` = '$penalty_id';");
					$db->query("UPDATE `$dbName`.`users` SET `complaints` = `complaints` + '1' WHERE  `users`.`uid` = '$user_id2';");
				} elseif ($status == 3) {
					$db->query(
						"UPDATE `$dbName`.`appeal` SET `from` = '$user_id', `to` = '$user_id2', `browser` = '$browser', `ip` = '$ip_address', `constime` = '$time' WHERE `appeal`.`id` = '$id';"
					);
					$db->query("UPDATE `$dbName`.`complaints` SET `type` = '6' WHERE `complaints`.`id` = '$penalty_id';");
					$db->query("UPDATE `$dbName`.`users` SET `complaints` = `complaints` + '1' WHERE  `users`.`uid` = '$user_id2';");
				}

				$error = array('response' => 1);
			} else {
				$error = array('error_msg' => 'Ошибка соединения с сервером. Попробуйте позже.');
			}
		} else {
			$error = array('error_msg' => 'Ошибка доступа.');
		}

		return json_encode($error);
	}

	public function img_upload()
	{
		global $db, $dbName, $root, $user_logged, $user_id, $time;

		if (!$user_logged) {
			return json_encode(array('error_text' => 'login'));
			exit;
		}

		$__dir = $root . '/images/uploads/penalty/';
		$__format_explode = explode('.', $_FILES['file']['name']);
		$__format = $__format_explode[count($__format_explode) - 1]; // получаем формат изображения
		$__format_type = array('jpeg', 'JPEG', 'jpg', 'JPG', 'png', 'PNG', 'gif', 'GIF', 'bmp', 'BMP');
		$__file_rand_name = rand_str(10);
		$__file_mini_rand_name = rand_str(15);
		$__file_name = $__dir . '' . $__file_rand_name . '.jpg';

  // открываем последние загруженные изображения
		$qattach_img_limit = time() - (30 * 60);
		$qattach_img = $db->query("SELECT `id` FROM `attaches_img` WHERE `time` >= '$qattach_img_limit' AND `uid` = '$user_id'");
		$nattach_img = $db->num($qattach_img);

		if ($nattach_img >= 30) {
			$json = array(
				'error_text' => 'Вы загружаете слишком много изображений. Попробуйте позже.'
			);
		} elseif (!in_array($__format, $__format_type)) {
			$json = array('error_text' => 'Неизвестный формат изображения.');
		} else {
			if (copy($_FILES['file']['tmp_name'], $__file_name)) {
    // если файл загружен, то создаем уменьшенное изображение
				$new_image = new picture($__dir . '/' . $__file_rand_name . '.jpg');
				$new_image->autoimageresize(75, 75);
				$new_image->imagesave('jpeg', $__dir . '/' . $__file_mini_rand_name . '.jpg');
				$new_image->imageout();
				$db->query(
					"INSERT INTO `$dbName`.`attaches_img` (`id`, `uid`, `small`, `big`, `time`, `modules`) VALUES ('', '$user_id', '$__file_mini_rand_name', '$__file_rand_name', '$time', '12');"
				);
    // получаем id загруженного изображения
				$qImg = $db->query("SELECT `id` FROM `attaches_img` WHERE `small` = '$__file_mini_rand_name' AND `uid` = '$user_id'");
				$dImg = $db->fetch($qImg);
				$json = array(
					'success' => 1, 'id' => $dImg['id'], 'result_big_file' => $__file_rand_name . '.jpg', 'result_mini_file' => $__file_mini_rand_name . '.jpg'
				);
			} else {
				$json = array('error_text' => 'Неизвестная ошибка.');
			}
		}

		return json_encode($json);
	}

	public function urgent_comment_attaches($list = null, $uid = null)
	{
		global $db;

  // аттачи к комментариям
		$query_attaches_photo = $db->query(
			"SELECT `small`, `big` FROM `attaches_img` WHERE `uid` = '$uid' AND `id` IN(" . $db->escape($list) . ") AND `modules` = 12 ORDER BY `id` ASC"
		);
		while ($data_attaches_photo = $db->fetch($query_attaches_photo)) {
			$attaches_img_result .= '<a href="/images/uploads/penalty/' . $data_attaches_photo['big'] . '.jpg" target="_blank"><img src="/images/uploads/penalty/' . $data_attaches_photo['small'] . '.jpg"></a>';
		}
		return $attaches_img_result ? '<div class="question_attaches_img_result">' . $attaches_img_result . '</div>' : '';
	}

	public function urgent_comment_attaches_url($list = null, $uid = null)
	{
		global $db;

  // аттачи к комментариям
		$query_attaches_photo = $db->query(
			"SELECT `small`, `big` FROM `attaches_img` WHERE `uid` = '$uid' AND `id` IN(" . $db->escape($list) . ") AND `modules` = 12 ORDER BY `id` ASC"
		);
		while ($data_attaches_photo = $db->fetch($query_attaches_photo)) {
			$attaches_img_result .= '<a href="/images/uploads/penalty/' . $data_attaches_photo['big'] . '.jpg" target="_blank">Посмотреть скриншот...</a>';
		}
		return $attaches_img_result ? '' . $attaches_img_result . '' : '';
	}
}

$urgent = new urgent;
