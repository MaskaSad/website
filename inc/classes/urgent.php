<?php
class urgent
{
	public function voutes() {
		global $db, $redis, $logs, $dbName, $exchange_one_voute, $user_logged, $user_id, $uvk_id, $users_nik, $session, $upoints, $ugroup;

		$count = $_POST['count'];
		$money = $_POST['money'];
		$count_nik = $_POST['count_nik'];
		$photo_attaches = $_POST['photo_attaches'];
		$urgent_id = $_POST['uid'];

		if (!$user_logged) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif (!$users_nik) {
			$error = array('error_msg' => 'Необходимо указать Никнейм в настройках.');
		} elseif ($count < 1) {
			$error = array('error_msg' => 'Необходимо указать ваш пробег.');
		} elseif ($money < 1) {
			$error = array('error_msg' => 'Необходимо указать сколько вы заработали.');
		} elseif ($photo_attaches < 1) {
			$error = array('error_msg' => 'Необходимо загрузить скриншот.');
		} elseif ($ugroup == 6) {
			$error = array('error_msg' => 'У вас отпуск, вы не можете отправить отчёт.');
		} elseif (!$uvk_id) {
			$error = array('error_msg' => 'Необходимо привязать страницу ВКонтакте.');
		} else {
			if ($db->query(
				"INSERT INTO `$dbName`.`urgent` (`id`, `user_id`, `vk_id`, `nik`, `count`, `points`, `time`, `status`, `no_active`, `attaches_photo`, `urgent_id`, `money`) VALUES (NULL, '$user_id', '$uvk_id', '$count_nik', '$count', '$count', '" . time() . "', '1', '0', '$photo_attaches', '$urgent_id', '$money');"
			)) {
				$db->query("UPDATE `$dbName`.`users` SET `upoints` = `upoints` - '$points' WHERE  `users`.`uid` = '$user_id';");

				$counter = $db->query("SELECT COUNT(*) as `count` FROM `urgent` WHERE `no_active` = '0'");
				$counter = $db->assoc($counter);

				$counter = $counter['count'];

				$redis->set('admin_urgent_counter', $counter);

				$logs->urgent_request($user_id, $points);

				$error = array('response' => 1, 'count' => $count, 'points' => $points);
			} else {
				$error = array('error_msg' => 'Ошибка соединения с сервером. Попробуйте позже.');
			}
		}

		return json_encode($error);
	}

	public function user_requests()
	{
		global $db, $user_id;

		$q = $db->query("SELECT `count`, `points`, `status`, `attaches_photo` FROM `urgent` WHERE `user_id` = '$user_id' ORDER BY `id` DESC");

		while ($d = $db->assoc($q)) {
			$count = $d['count'];
			$points = $d['points'];
			$status = $d['status'];
			$qattaches_photo = normal_id_attaches($d['attaches_photo']);
			$qattaches_photo_list = urgent::urgent_comment_attaches_url($qattaches_photo, $user_id);

			switch ($status) {
				case 1:
					$status_title = 'Рассматривается..';
					$status_class = 'wait';

					break;

				case 2:
					$status_title = 'Одобрено';
					$status_class = 'ok';

					break;

				case 3:
					$status_title = 'Отклонено';
					$status_class = 'error';

					break;
			}

			$template .= '
      <div class="block" style="margin-right: 9px;width: 97%;margin-top: 15px;margin-bottom: -5px;">
          <div class="price_' . $status_class . '">' . $status_title . '</div>
          <div class="time">Отправка заявки</div>
          <ul style="padding-bottom: 0;">
              <li>Проехал км: <b>' . $points . ' км</b></li>
              <li>Скриншот: ' . $qattaches_photo_list . '</li>
          </ul>
      </div>
   ';
		}

		return $template;
	}

	public function admin_requests()
	{
		global $db;

		$user_ids = array();
		$requests = array();

		$users = array();

		$q = $db->query(
			"SELECT `id`, `user_id`, `vk_id`, `nik`, `count`, `points`, `time`, `status`, `attaches_photo`, `urgent_id`, `money` FROM `urgent` WHERE `no_active` = '0' ORDER BY `id` DESC"
		);

		while ($d = $db->assoc($q)) {
			$user_ids[] = $d['user_id'];

			$requests[] = array(
				'id' => $d['id'], 'user_id' => $d['user_id'], 'vk_id' => $d['vk_id'], 'nik' => $d['nik'], 'count' => $d['count'], 'points' => $d['points'], 'time' => $d['time'], 'status' => $d['status'], 'attaches_photo' => $d['attaches_photo'], 'urgent_id' => $d['urgent_id'], 'money' => $d['money']
			);
		}

		$users_get = $db->query(
			"SELECT `uid`, `uvk_id`, `uname`, `ulast_name`, `uavatar` , `nik` FROM `users` WHERE `uid` IN(" . implode(',', $user_ids) . ")"
		);

		while ($users_data = $db->assoc($users_get)) {
			$user_data_user_id = $users_data['uid'];
			$user_data_vk_id = $users_data['uvk_id'];
			$user_data_first_name = $users_data['uname'];
			$user_data_last_name = $users_data['ulast_name'];
			$user_data_avatar = $users_data['uavatar'];
			$user_data_nik = $users_data['nik'];

			$users[$user_data_user_id] = array(
				'user_id' => $user_data_user_id, 'vk_id' => $user_data_vk_id, 'first_name' => $user_data_first_name, 'last_name' => $user_data_last_name,
				'avatar' => $user_data_avatar, 'nik' => $user_data_nik
			);
		}

		for ($i = 0; $i < count($requests); $i++) {
			$request_id = $requests[$i]['id'];
			$request_user_id = $requests[$i]['user_id'];
			$request_points = $requests[$i]['points'];
			$request_time = $requests[$i]['time'];
			$urgent_id = $requests[$i]['urgent_id'];
			$money = $requests[$i]['money'];
			$qattaches_photo = normal_id_attaches($requests[$i]['attaches_photo']);
			$qattaches_photo_list = urgent::urgent_comment_attaches($qattaches_photo, $request_user_id);
			$request_users_nik = $users[$request_user_id]['nik'];

			$template .= '               
        <div class="item" id="admin_exchange_requests'.$request_id.'">
         <div class="clearfix">
           <div class="fl_l title">
             <span>'.$request_users_nik.'</span> <i>Номер заказа #'.$urgent_id.'</i>
           </div>
           <div class="fl_l start">
             <div class="count">Проехал</div>
             <div class="label">'.$request_points.' км</div>
           </div>
           <div class="fl_l trailer">
             <div class="count">Заработал</div>
             <div class="label">'.$money.' €</div>
           </div>
           <div class="fl_l time">
             <div class="count">Отправил</div>
             <div class="label">'.new_time($request_time).'</div>
           </div>
           <div class="fl_r control_buttons">
             <div class="question_attaches_img_result" style="margin-right: 10px;">
               '.$qattaches_photo_list.'
             </div>
             <div class="fl_r" id="request_icon"><div class="requests_icons ok" onclick="urgent.set_status(' . $request_id . ', 2)"></div><div class="requests_icons no" onclick="urgent.set_status(' . $request_id . ', 3)"></div></div>
           </div>
         </div>
       </div>
   ';
		}

		return (($template) ? $template : '<div id="tasks_none"><div id="text_no">Не найдено ни одной заявки.</div></div>');
	}

	public function admin_set_status() {
		global $db, $redis, $logs, $user_id, $dbName, $user_logged, $ugroup, $urgent_one_voute, $exchange_one_voute, $time, $browser, $ip_address;

		$uid = (int) $_POST['uid'];
		$id = (int) $_GET['id'];
		$status = (int) $_GET['status'];

		$q = $db->query(
			"SELECT `id`, `user_id`, `points`, `status`, `urgent_id`, `count`, `money` FROM `urgent` WHERE `id` = '$id' AND `no_active` = '0'"
		);
		$d = $db->assoc($q);

		$user_id2 = $d['user_id'];
		$points = $d['points'];
		$urgent_id = $d['urgent_id'];
		$mile = $d['count'];
		$money = $d['money'];
                
		if (!$user_logged) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif ($ugroup != 4) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif ($d['id']) {
			if ($db->query("UPDATE `$dbName`.`urgent` SET `status` = '$status', `no_active` = '1' WHERE `urgent`.`id` = '$id';")) {
				$counter = $db->query("SELECT COUNT(*) as `count` FROM `urgent` WHERE `no_active` = '0'");
				$counter = $db->assoc($counter);

				$counter = $counter['count'];

				$redis->set('admin_urgent_counter', $counter);

				if($status == 2) {
					$db->query("UPDATE `$dbName`.`users` SET `mile` = `mile` + '$mile', `money` = `money` + '$money', `archive` = `archive` + '1', `count_tasks` = `count_tasks` + '1' WHERE  `users`.`uid` = '$user_id2';");
                    $test = addBalancePoints($urgent_one_voute, $user_id2, 2);
					$db->query("INSERT INTO `$dbName`.`archive` (`id`, `from`, `to`, `section`, `url`, `tid`, `time`, `type`, `upoints`, `browser`, `ip`, `ucategory`, `module`, `urgent_id`, `points_plus`, `urgent_money`) VALUES (NULL, '$user_id', '$user_id2', '0', '0', '0', '$time', '3', '0', '$browser', '$ip_address', '0', '2', '$urgent_id', '$urgent_one_voute', '$money');");
				} elseif($status == 3) {
					$db->query("INSERT INTO `$dbName`.`archive` (`id`, `from`, `to`, `section`, `url`, `tid`, `time`, `type`, `upoints`, `browser`, `ip`, `ucategory`, `module`, `urgent_id`, `points_plus`) VALUES (NULL, '$user_id', '$user_id2', '0', '0', '0', '$time', '4', '0', '$browser', '$ip_address', '0', '2', '$urgent_id', '$exchange_one_voute');");
					$db->query("UPDATE `$dbName`.`users` SET `archive` = `archive` + '1' WHERE  `users`.`uid` = '$user_id2';");
                    $db->query("UPDATE `$dbName`.`users` SET `count_tasks` = `count_tasks` + '1' WHERE  `users`.`uid` = '$user_id2';");
				}//test

                $qq = $db->query("SELECT `count_tasks`, `category`, `money`, `mile` FROM `users` WHERE `uid` = '$user_id2'");
		        $dd = $db->assoc($qq);

		        $count_tasks2 = $dd['count_tasks'];
		        $category2 = $dd['category'];
			    $money2 = $dd['money'];
			    $mile2 = $dd['mile'];
                
                $check_award = award($user_id2, $count_tasks2, 3);
                $check_award_money = award($user_id2, $money2, 4);
                $check_award_mile = award($user_id2, $mile2, 5);

                if($check_award && $check_award_money && $check_award_mile) {
                    $error = array('response' => 1);
                } else {
                    $error = array('error_text' => 'Не могу проверить достижения...');
                }

			} else {
				$error = array('error_msg' => 'Ошибка соединения с сервером. Попробуйте позже.');
			}
		} else {
			$error = array('error_msg' => 'Ошибка доступа.');
		}

		return json_encode($error);
	}

	public function img_upload() {
		global $db, $dbName, $root, $picture, $user_logged, $user_id, $time;

		if (!$user_logged) {
			return json_encode(array('error_text' => 'login'));
			exit;
		}

		$__dir = $root . '/images/uploads/urgent/';
		$__format_explode = explode('.', $_FILES['file']['name']);
		$__format = $__format_explode[count($__format_explode) - 1]; // получаем формат изображения
		$__format_type = array('jpeg', 'JPEG', 'jpg', 'JPG', 'png', 'PNG', 'gif', 'GIF', 'bmp', 'BMP');
		$__file_rand_name = rand_str(10);
		$__file_mini_rand_name = rand_str(15);
		$__file_name = $__dir . '' . $__file_rand_name . '.jpg';

  // открываем последние загруженные изображения
		$qattach_img_limit = time() - (30 * 60);
		$qattach_img = $db->query("SELECT `id` FROM `attaches_img` WHERE `time` >= '$qattach_img_limit' AND `uid` = '$user_id'");
		$nattach_img = $db->num($qattach_img);

		if ($nattach_img >= 30) {
			$json = array(
				'error_text' => 'Вы загружаете слишком много изображений. Попробуйте позже.'
			);
		} elseif (!in_array($__format, $__format_type)) {
			$json = array('error_text' => 'Неизвестный формат изображения.');
		} else {
			if (copy($_FILES['file']['tmp_name'], $__file_name)) {
    // если файл загружен, то создаем уменьшенное изображение
				$new_image = new picture($__dir . '/' . $__file_rand_name . '.jpg');
				$new_image->autoimageresize(75, 75);
				$new_image->imagesave('jpeg', $__dir . '/' . $__file_mini_rand_name . '.jpg');
				$new_image->imageout();
				$db->query(
					"INSERT INTO `$dbName`.`attaches_img` (`id`, `uid`, `small`, `big`, `time`, `modules`) VALUES ('', '$user_id', '$__file_mini_rand_name', '$__file_rand_name', '$time', '11');"
				);
    // получаем id загруженного изображения
				$qImg = $db->query("SELECT `id` FROM `attaches_img` WHERE `small` = '$__file_mini_rand_name' AND `uid` = '$user_id'");
				$dImg = $db->fetch($qImg);
				$json = array(
					'success' => 1, 'id' => $dImg['id'], 'result_big_file' => $__file_rand_name . '.jpg', 'result_mini_file' => $__file_mini_rand_name . '.jpg'
				);
			} else {
				$json = array('error_text' => 'Неизвестная ошибка.');
			}
		}

		return json_encode($json);
	}

	public function urgent_comment_attaches($list = null, $uid = null)
	{
		global $db;

  // аттачи к комментариям
		$query_attaches_photo = $db->query(
			"SELECT `small`, `big` FROM `attaches_img` WHERE `uid` = '$uid' AND `id` IN(" . $db->escape($list) . ") AND `modules` = 11 ORDER BY `id` ASC"
		);
		while ($data_attaches_photo = $db->fetch($query_attaches_photo)) {
			$attaches_img_result .= '<a href="/images/uploads/urgent/' . $data_attaches_photo['big'] . '.jpg" target="_blank"><img src="/images/uploads/urgent/' . $data_attaches_photo['small'] . '.jpg"></a>';
		}
		return $attaches_img_result ? '<div class="question_attaches_img_result">' . $attaches_img_result . '</div>' : '';
	}

	public function urgent_comment_attaches_url($list = null, $uid = null)
	{
		global $db;

  // аттачи к комментариям
		$query_attaches_photo = $db->query(
			"SELECT `small`, `big` FROM `attaches_img` WHERE `uid` = '$uid' AND `id` IN(" . $db->escape($list) . ") AND `modules` = 11 ORDER BY `id` ASC"
		);
		while ($data_attaches_photo = $db->fetch($query_attaches_photo)) {
			$attaches_img_result .= '<a href="/images/uploads/urgent/' . $data_attaches_photo['big'] . '.jpg" target="_blank">Посмотреть скриншот...</a>';
		}
		return $attaches_img_result ? '' . $attaches_img_result . '' : '';
	}
}

$urgent = new urgent;
