<?php
class tasks {
    public function check_number_reports() {
        global $db, $user_id;

        $q = $db->query("SELECT `id` FROM `urgent` WHERE `user_id` = '$user_id' AND `status` = '2' AND UNIX_TIMESTAMP(CURRENT_DATE()) < `time`");
        $n = $db->num($q);

        return $n;
    }
    public function my_num() {
        global $db;

        $q = $db->query("SELECT * FROM orders");
        $n = $db->num($q);

        return $n;
    }

    public function all_report() {
        global $db;

        $q = $db->query("SELECT * FROM urgent WHERE `status` = '2' AND `no_active` = '1'");
        $n = $db->num($q);

        return $n;
    }

    public function profileList() {
        global $db;

        $q = $db->query("SELECT `id`, `city_id`, `created_time` FROM `orders` ORDER BY `id` DESC LIMIT 7");

        $template = '';

        while($d = $db->assoc($q)) {
            $cities_q = $db->query("SELECT `city`, `ton` FROM `cities` WHERE `id` = '".$d['city_id']."'");
            $cities_d = $db->assoc($cities_q);

            $urgent_ids = $d['id'];
            $time = $d['created_time'];
            $time_limit = time() - (59 * 60);

            if($time < $time_limit) {
                $button = '<div class="blue_button_wrap" id="buttons_wight" style="opacity: 0.7;"><div class="blue_button">Отправить отчет <div class="reg_form_white_breg_right"></div></div></div>';
            } else {
                $button = '<div class="blue_button_wrap" id="buttons_wight" onclick="urgent.addurgent('.$d['id'].');"><div class="blue_button">Отправить отчет <div class="reg_form_white_breg_right"></div></div></div>';
            }
            $template .= '
             <div class="item">
               <div class="clearfix">
                 <div class="fl_l title">
                   <span>Срочный заказ</span>
                   <i>#'.$urgent_ids.'</i>
                 </div>
                 <div class="fl_l start">
                  <div class="count">Старт</div> 
                  <div class="label">'.((isset($cities_d['city'])) ? $cities_d['city'] : '—').'</div>
                 </div>
                 <div class="fl_l trailer">
                  <div class="count">Прицеп</div> 
                  <div class="label">Больше '.((isset($cities_d['ton'])) ? $cities_d['ton'] : '—').' тонн</div>
                 </div>
                 <div class="fl_l time">
                  <div class="count">Время</div> 
                  <div class="label">'.date('H', $time).':00</div>
                 </div>
                 <div class="fl_r control_buttons">
                  '.$button.'
                </div>
               </div>
             </div>
   ';
        }

        return $template;
    }

    public function create() {
        global $db, $dbName, $time;

        $cities_q = $db->query("SELECT `id` FROM `cities` WHERE `status` <= 0 ORDER BY RAND() LIMIT 1");
        $cities_d = $db->assoc($cities_q);

        if(isset($cities_d['id'])) {
            if($db->query("INSERT INTO `$dbName`.`orders` (`id`, `city_id`, `created_time`) VALUES (NULL, '".$cities_d['id']."', '$time');")) {
                $db->query("UPDATE `$dbName`.`cities` SET `status` = '1' WHERE `cities`.`id` = '".$cities_d['id']."';");

                return true;
            }
        }
    }
    public function update_bd() {
        global $db, $user_id;

        $q = $db->query("UPDATE `cities` SET `status` = REPLACE(`status`, '1', '0');");

        return $q;
    }
}

$tasks = new tasks;
?>