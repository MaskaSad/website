<?php
class user {
 public function check_login($login = null) {
  global $db;
  $query = $db->query("SELECT `ulogin` FROM `users` WHERE `ulogin` = '$login'");
  $num = $db->num($query);
  // в случае, если логин существует, возвращаем 1
  return $num ? 1 : 0;
 }

 public function check_email($email = null) {
  global $db;
  $query = $db->query("SELECT `uemail` FROM `users` WHERE `uemail` = '$email'");
  $num = $db->num($query);
  // в случае, если email существует, возвращаем 1
  return $num ? 1 : 0;
 }

 public function check_vk_id($vk_id = null) {
  global $db;
  $query = $db->query("SELECT `uvk_id` FROM `users` WHERE `uvk_id` = '$vk_id'");
  $num = $db->num($query);
  // в случае, если вк ид существует, возвращаем 1
  return $num ? 1 : 0;
 }

 public function get_user_id($login) {
  global $db;
  $query = $db->query("SELECT `uid` FROM `users` WHERE `ulogin` = '$login'");
  $data = $db->fetch($query);
  // в случае, если id существует, возвращаем его
  return $data['uid'] ? $data['uid'] : '';
 }

 public function auth_cookies($user_id, $hash) {
  // куки авторизации
  setCookie('user_id', $user_id, time() + 60 * 60 * 24 * 365, '/');
  setCookie('user_hash', $hash, time() + 60 * 60 * 24 * 365, '/');
 }

 public function check_steam($steam = null) {
  global $db;
  $query = $db->query("SELECT `steamid` FROM `users` WHERE `steamid` = '$steam'");
  $num = $db->num($query);
  // в случае, если логин существует, возвращаем 1
  return $num ? 1 : 0;
 }

 public function login($params) {
  global $db, $logs;
  $login = $db->escape(trim($params['login']));
  $password = $db->escape(trim(md5(md5($params['password']))));

  $query = $db->query("SELECT `uid`, `upassword`, `uhash` FROM `users` WHERE `ulogin` = '$login'");
  $data = $db->fetch($query);

  $user_id = $data['uid'];
  $hash = $data['uhash'];

  if($db->error()) {
   $json = array('error_text' => 'Ошибка соединения с базой данных.');
  } elseif($password == $data['upassword']) {
   user::auth_cookies($user_id, $hash);
   $logs->user_login($user_id);
   $json = array('response' => 1);
  } else {
   $json = array('error_text' => 'Неправильный логин или пароль.');
  }

  return json_encode($json);
 }

 public function auth_vk() {
  global $db, $logs;

  $uvkid = (int) abs($_POST['uvk_id']);

  $q = $db->query("SELECT `uid`, `uvk_id`, `uhash` FROM `users` WHERE `uvk_id` = '$uvkid'");
  $d = $db->fetch($q);

  $user_id = $d['uid'];
  $vkid = $d['uvk_id'];
  $hash = $d['uhash'];

  if($db->error()) {
   $json = array('error_text' => 'Ошибка соединения с базой данных.');
  } elseif($uvkid == !$vkid) {
   $json = array('error_text' => 'Такой сотрудник не зарегистрирован.');
  } elseif($uvkid == $vkid) {
   user::auth_cookies($user_id, $hash);
   $logs->user_login($user_id);
   $json = array('response' => 1);
  } else {
   $json = array('error_text' => 'Неизвестная ошибка.');
  }

  return json_encode($json);
 }

 public function reemail() {
  global $uemail, $ureg_time, $user_login, $site_url, $user_logged, $user_id;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  $unique_key = md5("$ureg_time+$user_login+$uemail");
  // отправляем на почту
  $email_title = 'Регистрация на Russian Express завершена!';
  $email_text = '
   Здравствуйте, <b>'.$user_login.'</b>!
   <br /> <br />
   Чтобы подтвердить регистрацию, перейдите по ссылке <a href="'.$site_url.'activate?key='.$unique_key.'&uid='.$user_id.'">'.$site_url.'activate?key='.$unique_key.'&uid='.$user_id.'</a>
   <br /> <br />
   С уважением, <br />
   <a href="'.$site_url.'">Команда Russian Express</a>.
  ';
  send_email($uemail, $email_title, $email_text);
  return json_encode(array('success' => 1));
 }

 public function reg($params) {
  global $db, $dbName, $ip_address, $browser, $ref_points, $logs, $session, $support, $site_url;
  $time = time();

  $first_name = $db->escape(trim($params['first_name']));
  $last_name = $db->escape(trim($params['last_name']));
  $login = $db->escape(trim($params['login']));
  $password = $db->escape(trim($params['password']));
  $password_md5 = md5(md5($password));
  $email = $db->escape(trim($params['email']));
  $reg_nickname = $db->escape(trim($params['reg_nickname']));
  $users_day = $db->escape(trim($params['users_day']));
  $users_month = $db->escape(trim($params['users_month']));
  $users_year = $db->escape(trim($params['users_year']));
  $vk_uid = $db->escape(trim($params['vk_uid']));
  $invited_nickname = $db->escape(trim($params['invited_nickname']));
  $captcha_code = $db->escape(trim($params['captcha_code']));
  $captcha_key = (int) abs($_POST['captcha_key']);

  $ref = (int) $params['ref'];
  $hash = md5("$login+$password+$time");
  $unique_key = md5("$time+$login+$email");

  if(user::check_login($login)) {
   $json = array('error_msg' => 'Такой логин уже занят');
  } elseif(mb_strlen($password, 'UTF-8') < 6) {
   $json = array('error_msg' => 'Слишком <b>короткий пароль</b>. Пароль должен состоять не менее, чем из 6 символов.');
  } elseif(user::check_email($email)) {
   $json = array('error_msg' => 'Такой <b>e-mail уже зарегистрирован</b>  в системе.');
  } elseif(!$vk_uid) {
   $json = array('error_msg' => 'Необходимо получить адрес вашей страницы ВК.');
  } elseif(user::check_vk_id($vk_uid)) {
   $json = array('error_msg' => 'Такая страницы ВК уже существует');
  } elseif(!$captcha_code) {
   $json = array('error_msg' => 'Неверно введен код безопасности.');
  } elseif(mb_strtolower($session->get('captcha_code', $captcha_key), 'UTF-8') != mb_strtolower($captcha_code, 'UTF-8')) {
   $json = array('error_msg' => 'Неверно введен код безопасности.');
  } else {
   $session->delete('captcha_code', $captcha_key);
   // регистрируем пользователя в таблицу users
   $query = "INSERT INTO  `$dbName`.`users` (
    `uid` ,
    `ulogin` ,
    `upassword` ,
    `uemail` ,
    `temp_password` ,
    `uemail_activated` ,
    `uemail_helper` ,
    `uvk_id` ,
    `uname` ,
    `ulast_name` ,
    `uip_address` ,
    `ureg_time` ,
    `ulast_time` ,
    `ugender` ,
    `ugroup` ,
    `upoints` ,
    `uban_type` ,
    `uban_time` ,
    `uban_text` ,
    `uhash`,
    `uavatar`,
    `uagent_id`,
    `uagent_avatar`,
    `uagent_rate_plus`,
    `uagent_rate_minus`,
    `udel` ,
    `city` ,
    `ubrowser`,
    `ubyear`,
    `complaints`,
    `account`,
    `vk_time_update`,
    `nik`,
    `uage`,
    `steamid`,
    `steamurl`,
    `upoints_fine`,
    `year`,
    `category`,
    `archive`,
    `nik_activated`,
    `chat_ban`,
    `convoys_time`,
    `convoys_count`,
    `rules`,
    `mile`,
    `money`,
    `count_tasks`,
    `ref_uid`,
    `server`,
    `truck`,
    `day`,
    `month`,
    `cover`,
    `partner`
    )
    VALUES (
    NULL ,  '$login', '$password_md5', '$email', '0', '0', '0', '$vk_uid', '$first_name', '$last_name', '$ip_address', '$time', '$time', '0', '0', '0', '1', '0', '', '$hash', '', '', '', '', '', '', '', '', '$browser', '', '', '', '$reg_nickname', '0', '0', '0', '0', '$users_year', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$ref', '0', '0', '$users_day', '$users_month', '0', '$invited_nickname'
    );";
   if($db->query($query)) {
    $user_id = $db->insert_id(); // определяем id нового пользователя
    user::auth_cookies($user_id, $hash); // записываем куки для авторизации
    $logs->user_login($user_id);

    $db->query("INSERT INTO `$dbName`.`users_number` (`number`, `uid`, `time`) VALUES (NULL, '$user_id', '".time()."');");

    // отправляем на почту
    $email_title = 'Регистрация на Russian Express завершена!';
    $email_text = '
     Здравствуйте, <b>'.$first_name.'</b>!
     <br /> <br />
     Чтобы подтвердить регистрацию, перейдите по ссылке <a href="'.$site_url.'activate?key='.$unique_key.'&uid='.$user_id.'">'.$site_url.'activate?key='.$unique_key.'&uid='.$user_id.'</a>
     <br /> <br />
     С уважением, <br />
     <a href="'.$site_url.'">Команда Russian Express</a>.
    ';
    send_email($email, $email_title, $email_text);

    $json = array('response' => 1);
   } else {
    $json = array('error_msg' => 'Ошибка соединения с сервером. Попробуйте позже.');
   }
  }

  return json_encode($json);
 }

 public function change_password() {
  global $db, $dbName, $user_id, $user_login, $upassword, $user_logged, $logs, $session;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  $time = time();
  $old_password = $db->escape($_POST['old_password']);
  $new_password = $db->escape($_POST['new_password']);
  $new_password2 = $db->escape($_POST['new_password2']);
  $ssid = (int) abs($_POST['ssid']);
  $md5_new_password = md5(md5($new_password));
  $new_hash = md5("$user_login+$new_password+$time");

  if(md5(md5($old_password)) != $upassword) {
   $json = array('error_text' => 'Пароль не изменён, так как прежний пароль введён неправильно.');
  } elseif(mb_strlen($new_password, 'UTF-8') < 6) {
   $json = array('error_text' => 'Пароль должен состоять не менее, чем из 6 символов.');
  } elseif(!preg_match('/^([a-zA-Z0-9]){6,32}$/i', $new_password)) {
   $json = array('error_title' => 'Неправильный логин.', 'error_text' => 'Пароль должен содержать только латинские символы или цифры и не превышать 32 символа.');
  } elseif($new_password != $new_password2) {
   $json = array('error_text' => 'Пароль не изменён, так как новый пароль повторен неправильно.');
  } elseif($session->get('usession') != $ssid) {
    $json = array('error_text' => 'Истек период сессии. Обновите страницу или попробуйте позже.');
   } else {
   if($db->query("UPDATE  `$dbName`.`users` SET  `upassword` =  '$md5_new_password', `uhash` = '$new_hash' WHERE  `users`.`uid` = '$user_id' LIMIT 1 ;")) {
    user::auth_cookies($user_id, $new_hash);
    $logs->change_password($user_id, $user_id, '{"old_password":"'.$old_password.'", "new_password":"'.$new_password.'"}');
    $json = array('success' => 1);
   } else {
    $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
   }
  }
  return jdecoder(json_encode($json));
 }

 public function change_login() {
  global $db, $dbName, $user_login, $user_id, $user_logged, $logs, $session;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  $login = $db->escape($_POST['login']);
  $ssid = (int) abs($_POST['ssid']);

  if(mb_strlen($login, 'UTF-8') < 1) {
   $json = array('error_text' => 'Слишком короткий логин.');
  } elseif(!preg_match('/^([@a-zA-Z0-9]){1,30}$/i', $login)) {
   $json = array('error_text' => 'Логин должен содержать только латинские символы или цифры и не превышать 30 символов.');
  } elseif($user_login == $login) {
   $json = array('error_text' => 'Не совершайте глупость, пожалуйста.');
  } elseif(user::check_login($login)) {
   $json = array('error_text' => 'Такой логин уже занят.');
  } elseif($session->get('usession') != $ssid) {
   $json = array('error_text' => 'Истек период сессии. Обновите страницу или попробуйте позже.');
  } else {
   if($db->query("UPDATE  `$dbName`.`users` SET  `ulogin` =  '$login' WHERE  `users`.`uid` = '$user_id' LIMIT 1 ;")) {
    $logs->change_login($user_id, $user_id, '{"old_login":"'.$user_login.'", "new_login":"'.$login.'"}');
    $json = array('success' => 1);
   } else {
    $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
   }
  }
  return jdecoder(json_encode($json));
 }

  public function change_steam() {
  global $db, $dbName, $user_id, $user_logged, $logs, $session, $steamid;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  $steam = $db->escape($_POST['steam']);
  $ssid = (int) abs($_POST['ssid']);

  if(!preg_match('/^[0-9]{7,30}$/', $steam)) {
   $json = array('error_text' => 'Steamid должен содержать только цифры и не превышать 20 символов.');
  } elseif($steamid == $steam) {
   $json = array('error_text' => 'Не совершайте глупость, пожалуйста.');
  } elseif(user::check_steam($steam)) {
   $json = array('error_text' => 'Такой steamid уже существует.');
  } elseif($session->get('usession') != $ssid) {
   $json = array('error_text' => 'Истек период сессии. Обновите страницу или попробуйте позже.');
  } else {
   if($db->query("UPDATE  `$dbName`.`users` SET  `steamid` =  '$steam' WHERE  `users`.`uid` = '$user_id' LIMIT 1 ;")) {
    $logs->change_steamid($user_id, $user_id, '{"old_steamid":"'.$steamid.'", "new_steamid":"'.$steam.'"}');
    $json = array('success' => 1);
   } else {
    $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
   }
  }
  return jdecoder(json_encode($json));
 }

 public function change_name() {
  global $db, $dbName, $user_id, $user_logged, $logs, $session, $ufirst_name, $ulast_name;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  $edit_name = $db->escape($_POST['edit_name']); //Имя
  $edit_lastname = $db->escape($_POST['edit_lastname']); //Фамилия
  $edit_city = $db->escape($_POST['edit_city']); 
  $edit_date = $_POST['edit_date'];
  $edit_day = $_POST['edit_day'];
  $edit_month = $_POST['edit_month'];
  $edit_year = $_POST['edit_year'];

  if(!preg_match('/^[а-яА-Я]+$/iu', $edit_name)) {
   $json = array('error_name' => 1);
  } elseif(!preg_match('/^[а-яА-Я]+$/iu', $edit_lastname)) {
   $json = array('error_lastname' => 1);
  } elseif(!$edit_day) {
   $json = array('error_date' => 1);
  } elseif(!$edit_month) {
   $json = array('error_date' => 1);
  } elseif(!$edit_year) {
   $json = array('error_date' => 1);
  } elseif(!$edit_city) {
   $json = array('error_city' => 1);
  } else {
   if($db->query("UPDATE  `$dbName`.`users` SET  `uname` =  '$edit_name', `ulast_name` =  '$edit_lastname', `city` = '$edit_city', `year` = '$edit_year', `day` = '$edit_day', `month` = '$edit_month' WHERE `users`.`uid` = '$user_id' LIMIT 1 ;")) {
    $logs->change_name($user_id, $user_id, '{"old_name":"'.$ufirst_name.'", "new_name":"'.$edit_name.'", "old_surname":"'.$ulast_name.'", "new_surname":"'.$edit_lastname.'"}');
    $json = array('success' => 1);
   } else {
    $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
   }
  }
  return jdecoder(json_encode($json));
 }

 public function change_interests() {
  global $db, $dbName, $user_id, $user_logged;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  $server = $_POST['server'];
  $truck = $_POST['truck'];

  if($server < 0) {
   $json = array('error_text' => 'Необходимо выбрать сервер');
  } elseif($truck < 0) {
   $json = array('error_text' => 'Необходимо выбрать грузовик');
  } else {
   if($db->query("UPDATE  `$dbName`.`users` SET  `server` =  '$server', `truck` =  '$truck' WHERE `users`.`uid` = '$user_id' LIMIT 1 ;")) {

    $json = array('success' => 1);
   } else {
    $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
   }
  }
  return jdecoder(json_encode($json));
 }

 public function delete_account() {
  global $db, $dbName, $user_id, $udel, $session, $logs;

  $time = time();
  $ssid = (int) abs($_GET['ssid']);

  if($udel) {
   $type = 'return';
   $query = "UPDATE `$dbName`.`users` SET  `udel` =  '0' WHERE  `users`.`uid` = '$user_id' LIMIT 1 ;";
  } else {
   $type = 'del';
   $query = "UPDATE `$dbName`.`users` SET  `udel` =  '$time' WHERE  `users`.`uid` = '$user_id' LIMIT 1 ;";
  }

  if($session->get('usession') != $ssid) {
   $json = array('error_text' => 'Истек период сессии. Обновите страницу или попробуйте позже.');
  } elseif($db->query($query)) {
   if($udel) {
    $logs->return_account($user_id, $user_id);
   } else {
    $logs->delete_account($user_id, $user_id);
   }
   $json = array('success' => 1, 'type' => $type);
  } else {
   $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
  }
  return jdecoder(json_encode($json));
 }

 public function admin_users_list_num() {
  global $db, $online_limit, $vk;

  $section = $db->escape($_GET['section']);
  $search = $db->escape($_GET['search']);

  if($vk->url($search) == 1) {
   $search_end = json_decode($vk->explode($search), true);
   $user_get_info_vk = json_decode($vk->user_info($search_end['url']), true);
   $user_get_info_vk_id = (int) $user_get_info_vk['id'];
   if($user_get_info_vk_id) {
    $search_sql = "AND `uvk_id` = '$user_get_info_vk_id'";
   } else {
    $search_sql = "";
   }
  } elseif(preg_match('/^https?:\/\/vtc-express.ru/', $search) || preg_match('/^vtc-express.ru/', $search)) {
   $search_explode = explode('id', $search);
   $search_explode_id = (int) $search_explode[1];
   if($search_explode_id) {
    $search_sql = "AND `uid` = '$search_explode_id'";
   } else {
    $search_sql = "";
   }
  } else {
   $search_sql = $search ? "AND `ulogin` = '$search'" : '';
   $search_sql = $search ? "AND `nik` = '$search'" : '';
   $search_sql = $search ? "AND `uname` = '$search'" : '';
  }

  if($section == 'online') {
   $section_sql = "AND `ulast_time` >= '$online_limit'";
  } elseif($section == 'admin') {
   $section_sql = "AND `ugroup` = 4";
  } elseif($section == 'instructor') {
   $section_sql = "AND `ugroup` = 3";
  } elseif($section == 'agent') {
   $section_sql = "AND `category` = 0";
  } elseif($section == 'uban_type') {
   $section_sql = "AND `uban_type` = 1";
  } else {
   $section_sql = "";
  }

  $q = $db->query("
   SELECT `uid` FROM `users`
   WHERE `uid` > 0 AND `udel` = 0 ".$section_sql." ".$search_sql."
  ");
  $n = $db->num($q);

  return $n;
 }

 public function admin_users_list() {
  global $db, $noavatar, $online_limit, $vk, $ugroup, $user_id;

  $sort = $db->escape($_GET['sort']);
  $section = $db->escape($_GET['section']);
  $search = $db->escape($_GET['search']);

  $page = (int) $_GET['page'];
  $start_page = (!$page) ? 0 : $page - 1;
  $start_limit = $start_page * 10;

  if($vk->url($search) == 1) {
   $search_end = json_decode($vk->explode($search), true);
   $user_get_info_vk = json_decode($vk->user_info($search_end['url']), true);
   $user_get_info_vk_id = (int) $user_get_info_vk['id'];
   if($user_get_info_vk_id) {
    $search_sql = "AND `uvk_id` = '$user_get_info_vk_id'";
   } else {
    $search_sql = "";
   }
  } elseif(preg_match('/^https?:\/\/vtc-express.ru/', $search) || preg_match('/^vtc-express.ru/', $search)) {
   $search_explode = explode('id', $search);
   $search_explode_id = (int) $search_explode[1];
   if($search_explode_id) {
    $search_sql = "AND `uid` = '$search_explode_id'";
   } else {
    $search_sql = "";
   }
  } else {
   $search_sql = $search ? "AND `ulogin` = '$search'" : '';
   $search_sql = $search ? "AND `nik` = '$search'" : '';
   $search_sql = $search ? "AND `uname` = '$search'" : '';
  }

  if($section == 'online') {
   $section_sql = "AND `ulast_time` >= '$online_limit'";
  } elseif($section == 'admin') {
   $section_sql = "AND `ugroup` = 4";
  } elseif($section == 'instructor') {
   $section_sql = "AND `ugroup` = 3";
  } elseif($section == 'agent') {
   $section_sql = "AND `category` = 0";
  } elseif($section == 'uban_type') {
   $section_sql = "AND `uban_type` = 1";
  } else {
   $section_sql = "";
  }

  if($sort == 'asc') {
   $sort_sql = "ORDER BY `uid` ASC";
  } elseif($sort == 'desc') {
   $sort_sql = "ORDER BY `uid` DESC";
  } elseif($sort == 'max_points') {
   $sort_sql = "ORDER BY `upoints` DESC";
  } elseif($sort == 'min_points') {
   $sort_sql = "ORDER BY `upoints` ASC";
  } else {
   $sort_sql = "ORDER BY `uid` ASC";
  }

  $q = $db->query("
   SELECT `uid`, `ulogin`, `uvk_id`, `uname`, `ulast_name`, `ureg_time`, `ulast_time`, `ugroup`, `upoints`, `uavatar`, `uagent_id`, `uip_address`, `uemail`, `uemail_activated`, `uban_type`, `complaints`, `blacklist_notif`, `nik`, `upoints_fine`, `year`, `category`, `archive`, `convoys_time`, `partner`
   FROM `users` WHERE `uid` > 0 AND `udel` = 0 ".$section_sql." ".$search_sql."
   ".$sort_sql."
   LIMIT $start_limit, 10
  ");
  while($d = $db->fetch($q)) {
   $uid = $d['uid'];
   $ulogin = $d['ulogin'];
   $uemail = $d['uemail'];
   $uemail_activated = $d['uemail_activated'];
   $uvk_id = $d['uvk_id'];
   $uvk_id_page = $uvk_id ? 'vk.com/id'.$uvk_id.'' : 'не прикреплена';
   $uvk_id_icon = $uvk_id ? '<div class="vk_url"><div class="vk_url_icon" onclick="window.open(\'//vk.com/id'.$uvk_id.'\')"></div></div>' : '';
   $uname = $d['uname'];
   $nik_s = $d['nik'];
   $ulast_name = $d['ulast_name'];
   $ureg_time = $d['ureg_time'];
   $ulast_time = $d['ulast_time'];
   $ufull_name = $uname ? $uname.' '.$ulast_name : 'Безымянный';
   $users_nik = $d['nik'] ? $d['nik'] : 'не указан';
   $ugroup = $d['ugroup'];
   $category = $d['category'];
   $upoints = $d['upoints'];
   $fpoints = $d['upoints_fine'];
   $uavatar = $d['uavatar'] ? $d['uavatar'] : $noavatar;
   $uip_address = $d['uip_address'];
   $uagent_id = $d['uagent_id'];
   $uban_type = $d['uban_type'];
   $ucomplaints = $d['complaints'];
   $ublacklist_notif = $d['archive'];
   $convoys_time = $d['convoys_time'];
   $users_number = user::info_number($uid);
   $partner = $d['partner'];
   $qpartner = $partner ? $partner : 'Никто';

   if($ugroup == 4) {
    $ugroup_result = 'Администратор';
   } elseif($ugroup == 3) {
    $ugroup_result = 'Инструктор';
   } elseif($ugroup == 5) {
    $ugroup_result = 'Агент поддержки #'.$uagent_id;
   } else {
    $ugroup_result = 'Сотрудник';
   }

   $category_result = getCategoryTitle($category);

  $qq = $db->query("
   SELECT users.nik, ucategory_history.from
   FROM `ucategory_history`
    INNER JOIN `users` ON ucategory_history.from = users.uid
   WHERE ucategory_history.uid = '$uid'
   ORDER BY ucategory_history.id DESC
  ");
  $dd = $db->fetch($qq);
   $ucategory_nik = $dd['nik'];
   $user_nik = $ucategory_nik ? $ucategory_nik : 'еще не принимали';

   $template .= '
         <div id="users_id'.$uid.'" class="admin_users_user_mini">
          <div class="admin_users_user_mini_avatar">
          '.$uvk_id_icon.'
          <img src="'.$uavatar.'">
          </div>
          <div class="admin_users_user_mini_info">
           <div class="fl_l admin_users_name">
            <span>'.$ufull_name.'</span>
            <i>'.$ugroup_result.'</i>
           </div>
           <div class="fl_l admin_users_mini_info">
           <div class="fl_l count_points">
             <div class="count">Баллы</div>
             <div class="label">'.$upoints.' '.declOfNum(abs($upoints), array('балл', 'балла', 'баллов')).'</div>
           </div>
           <div class="fl_l count_fine">
             <div class="count">Штрафы</div>
             <div class="label">'.$fpoints.' '.declOfNum(abs($fpoints), array('балл', 'балла', 'баллов')).'</div>
           </div>
           <div class="fl_l users_auth">
             <div class="count">Последний вход</div>
             <div class="label">'.new_time($ulast_time).'</div>
           </div>
           </div>
           <div class="admin_users_icon result_amount fl_l">
           <span id="script_click'.$uid.'" class="tooltip_users_info'.$uid.' tooltip_users_info_result'.$uid.'" onclick="tooltip.create(this, \'users_info\', {msg: \'Информация о сотруднике: <br /> ID: '.$uid.' <br /> Логин: '.$ulogin.' <br /> Никнейм: '.$users_nik.' <br /> E-mail: '.$uemail.' '.($uemail_activated ? '+' : '-').' <br />Номер в колонне: '.$users_number.' <br />Страница ВКонтакте: '.$uvk_id_page.' <br /> IP: '.$uip_address.' <br /> Регистрация: '.new_time($ureg_time).' <br /> На конвое был: '.new_time($convoys_time).' <br /> Непрочитанные уведомления: '.$ublacklist_notif.' <br /> Принял ВТК: '.$qpartner.' <br /> Категория: '.$category_result.'\', position: \'left\'})">
            <div class="admin_users_icons info trns15s" id="test"></div>
           </span>
            <div class="admin_users_icons edit trns15s" onclick="users._action_menu_show('.$uid.');"></div>
            <div class="admin_users_icons delete trns15s" onclick="admin_users._delete_users('.$uid.')"></div>
            <div class="admin_users_action_menu" id="action_menu'.$uid.'">
              <a href="javascript://" onclick="admin_users._edit_balance('.$uid.')">Изменить баланс</a>
              <a href="javascript://" onclick="admin_users._edit_info('.$uid.')">Редактировать</a>
              <a href="javascript://" onclick="admin_users._send_points('.$uid.')">Баллы за конвой</a>
              <a href="javascript://" onclick="admin_users._blocked_history('.$uid.')">История приёма</a>
              <a href="javascript://" onclick="admin_users._ucategory('.$uid.')">Изменить категорию</a>
              <a href="javascript://" onclick="admin_users._upoints_fine('.$uid.')">Выписать штраф</a>
              '.($uban_type ? '<a href="javascript://" onclick="admin_users._unblocked('.$uid.')">Принять в компанию</a>' : '<a href="javascript://" onclick="admin_users._blocked('.$uid.')">Заблокировать</a>').'
              '.($user_id == 3 ? '<a href="javascript://" onclick="admin_users._edit_admin('.$uid.')">Назначить админом</a>' : '').'
            </div>
           </div>
          </div>
         </div>
   ';
  }
  return $template ? $template : '<div class="empty">Не найдено ни одного сотрудника.</div>';
 }

 public function history_balance_num($uid = null) {
  global $db;

  $q = $db->query("
   SELECT `lid` FROM `logs`
   WHERE `lto` = '$uid' AND `lpoints` > 0
  ");
  $n = $db->num($q);

  return $n;
 }

 public function history_balance() {
  global $db, $ugroup, $user_logged, $user_id;

  if(!$user_logged) {
   return 'login';
   exit;
  }

  if($ugroup != 4) {
   return 'Access Denied';
   exit;
  }

  $uid = (int) $_GET['uid'];
  $page = (int) $_GET['page'];
  $start_page = (!$page) ? 0 : $page - 1;
  $start_limit = $start_page * 10;

  $q = $db->query("
   SELECT logs.lid, logs.lfrom, logs.lto, logs.lmodule, logs.lmodule_type, logs.lpoints, logs.lbrowser, logs.ltime, logs.lip_address, logs.lmid, logs.ltext, users1.uname as uname1, users1.ulast_name as ulast_name1, users2.uname as uname2, users2.ulast_name as ulast_name2 FROM `logs`
    INNER JOIN users users1 ON logs.lfrom = users1.uid
    LEFT JOIN users users2 ON logs.lto = users2.uid
   WHERE logs.lto = '$uid' AND logs.lpoints > 0
   ORDER BY logs.lid DESC
   LIMIT $start_limit, 10
  ");
  while($d = $db->assoc($q)) {
   $logs_lid = $d['lid'];
   $logs_lfrom = $d['lfrom'];
   $logs_lto = $d['lto'];
   $logs_lmodule = $d['lmodule'];
   $logs_lmodule_type = $d['lmodule_type'];
   $logs_lpoints = $d['lpoints'];
   $logs_lbrowser = $d['lbrowser'];
   $logs_ltime = $d['ltime'];
   $logs_lip_address = $d['lip_address'];
   $logs_lmid = $d['lmid'];
   $logs_ltext = $d['ltext'];
   $users_uname1 = $d['uname1'];
   $users_ulast_name1 = $d['ulast_name1'];
   $users_uname2 = $d['uname2'];
   $users_ulast_name2 = $d['ulast_name2'];
   $users1_fullname = $users_uname1 ? $users_uname1.' '.$users_ulast_name1 : 'Безымянный';
   $users2_fullname = $users_uname2 ? $users_uname2.' '.$users_ulast_name2 : 'Безымянный';

  if($logs_lmodule == 1 && $logs_lmodule_type == 5) {
    $result_points = $logs_ltext == 1 ? '<b class="settings_balance_admin_users_history_points_plus">+'.$logs_lpoints.'</b>' : '<b class="settings_balance_admin_users_history_points_minus">-'.$logs_lpoints.'</b>';
    $template .= '
     <div class="settings_balance_admin_users_history">
      <div class="settings_balance_admin_users_history_title"><a href="/admin/modules/users/?search=https://vtc-express.ru/id'.$logs_lfrom.'" target="_blank"><b>'.$users1_fullname.'</b></a> изменил баланс</div>
      <div class="settings_balance_admin_users_history_text">
       <span class="settings_balance_admin_users_history_label">Баллы:</span>
       <span class="settings_balance_admin_users_history_field">'.$result_points.'</span>
       <br />
       <span class="settings_balance_admin_users_history_label">IP:</span>
       <span class="settings_balance_admin_users_history_field">'.$logs_lip_address.'</span>
       <br />
       <span class="settings_balance_admin_users_history_label">Браузер:</span>
       <span class="settings_balance_admin_users_history_field">'.$logs_lbrowser.'</span>
       <br />
       <span class="settings_balance_admin_users_history_label">Время:</span>
       <span class="settings_balance_admin_users_history_field">'.new_time($logs_ltime).'</span>
      </div>
     </div>
    ';
   } elseif($logs_lmodule == 9 && $logs_lmodule_type == 4) {
     $template .= '
     <div class="settings_balance_admin_users_history">
      <div class="settings_balance_admin_users_history_title"><a href="/admin/modules/users/?search=https://vtc-express.ru/id'.$logs_lto.'" target="_blank"><b>'.$users2_fullname.'</b></a> сделал пожертвования через «ROBOKASSA»</div>
      <div class="settings_balance_admin_users_history_text">
       <span class="settings_balance_admin_users_history_label">Баллы:</span>
       <span class="settings_balance_admin_users_history_field"><b class="settings_balance_admin_users_history_points_plus">+'.$logs_lpoints.'</b></span>
       <br />
       <span class="settings_balance_admin_users_history_label">IP:</span>
       <span class="settings_balance_admin_users_history_field">'.$logs_lip_address.'</span>
       <br />
       <span class="settings_balance_admin_users_history_label">Браузер:</span>
       <span class="settings_balance_admin_users_history_field">'.$logs_lbrowser.'</span>
       <br />
       <span class="settings_balance_admin_users_history_label">Время:</span>
       <span class="settings_balance_admin_users_history_field">'.new_time($logs_ltime).'</span>
      </div>
     </div>
    ';
   }
  }
  return $template ? '
   <div id="settings_balance_admin_users_pages">
    '.pages_ajax(array('ents_count' => user::history_balance_num($uid), 'ents_print' => 10, 'page' => $page)).'
    <span class="pages_ajax"><div class="upload"></div></span>
   </div>
   '.$template : '<div id="box_none">История баланса пуста.</div>';
 }

 public function edit_balance() {
  global $db, $user_logged, $ugroup;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  if($ugroup != 4) {
   return json_encode(array('access' => 'denied'));
   exit;
  }

  $uid = (int) $_POST['uid'];
  $type = $db->escape($_POST['type']);
  $num = (int) $_POST['num'];

  $t = 1;
  if($num < 1) {
   $json = array('error_text' => 'Введите количество.');
  } else {
   if($t) {
    subBalancePoints($num, $uid, $type);
    $json = array('success' => 1);
   } else {
    $json = array('error_text' => 'Ошибка соединения с базой данных.');
   }
  }
  return jdecoder(json_encode($json));
 }

 public function history_login_num() {
  global $db;

  $uid = (int) $_GET['uid'];

  $q = $db->query("SELECT `lid` FROM `logs` WHERE `lmodule` = '1' AND `lmodule_type` = '7' AND `lto` = '$uid' ORDER BY `lid` DESC");
  $n = $db->num($q);

  return $n;
 }

 public function history_login() {
  global $db, $ugroup, $user_logged;

  if(!$user_logged) {
   return 'login';
   exit;
  }

  if($ugroup != 4) {
   return 'Access Denied';
   exit;
  }

  $uid = (int) $_GET['uid'];
  $page = (int) $_GET['page'];
  $start_page = (!$page) ? 0 : $page - 1;
  $start_limit = $start_page * 10;
  $num = user::history_login_num();

  $q = $db->query("SELECT `lto`, `lbrowser`, `ltime`, `lip_address` FROM `logs` WHERE `lmodule` = '1' AND `lmodule_type` = '7' AND `lto` = '$uid' ORDER BY `lid` DESC LIMIT $start_limit, 10");
  while($d = $db->fetch($q)) {
   $browser = $d['lbrowser'];
   $time = $d['ltime'];
   $ip = $d['lip_address'];

   $template .= '
      <div class="admin_history_login_column_content_overflow">
       <div class="admin_history_login_column_content1">Браузер '.mb_substr($browser, 0, 20).'</div>
       <div class="admin_history_login_column_content2">'.new_time($time).'</div>
       <div class="admin_history_login_column_content3">'.$ip.'</div>
      </div>
   ';
  }
  return '
   <div id="admin_history_login_info">
    <b>История активности</b> показывает информацию о том, с каких устройств и в какое время пользователь заходил на сайт.
   </div>
   <div id="admin_history_login_pages">
    '.pages_ajax(array('ents_count' => $num, 'ents_print' => 10, 'page' => $page)).'
    <span class="pages_ajax"><div class="upload"></div></span>
   </div>
   <div id="admin_history_login">
    <div id="admin_history_login_table">
     <div id="admin_history_login_table_column">
      <div class="admin_history_login_column" id="admin_history_login_column1">Тип доступа</div>
      <div class="admin_history_login_column" id="admin_history_login_column2">Время</div>
      <div class="admin_history_login_column" id="admin_history_login_column3">IP-адрес</div>
     </div>
     <div id="admin_history_login_table_content">
     '.$template.'
     </div>
    </div>
   </div>
  ';
 }

 public function add_vk() {
  global $db, $dbName, $user_id, $user_logged;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }
  $uvkid = (int) abs($_POST['uvk_id']);
  $id = 1;

  if(!$id) {
   $json = array('error_text' => 'Неизвестная ошибка.');
  } elseif($db->query("UPDATE  `$dbName`.`users` SET `uvk_id` = '$uvkid' WHERE `users`.`uid` = '$user_id';")) {
    $json = array('success' => 1, 'text' => 'К Вашему аккаунту прикреплена страница <a href="https://vk.com/id'.$uvkid.'" target="_blank"><b>vk.com/id'.$uvkid.'</b></a>.', 'vkid' => $uvkid);
  } else {
   $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
  }

  return json_encode($json);
 }

 public function vk_delete() {
  global $db, $dbName, $user_id, $user_logged;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }
  $id = 1;

  if(!$id) {
   $json = array('error_text' => 'Неизвестная ошибка.');
  } elseif($db->query("UPDATE  `$dbName`.`users` SET `uvk_id` = '0' WHERE `users`.`uid` = '$user_id';")) {
   $json = array('success' => 1);
  } else {
   $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
  }

  return json_encode($json);
 }

public function admin_users_delete() {
 global $db, $dbName, $user_logged, $ugroup;

 $uid = (int) abs($_GET['uid']);

 if(!$user_logged) {
  return json_encode(array('error_text' => 'login'));
  exit;
 }

 if($ugroup != 4) {
  return 'Access Denied';
  exit;
 }

 if(!$uid) {
  $json = array('error_text' => 'Неизвестная ошибка.');
} elseif($db->query("DELETE FROM `adminsroot`.`users` WHERE `users`.`uid` = '$uid';")) {
  $json = array('success' => 1);
 } else {
  $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
 }

 return json_encode($json);
}

 public function edit_info() {
  global $db, $ugroup, $user_logged;

  if(!$user_logged) {
   return 'login';
   exit;
  }

  if($ugroup != 4) {
   return 'Access Denied';
   exit;
  }

  $uid = (int) $_GET['uid'];

  $q = $db->query("SELECT `ulogin`, `uemail`, `uemail_activated`, `uvk_id`, `uname`, `ulast_name`, `uavatar`, `ugender`, `nik` FROM `users` WHERE `uid` = '$uid'");
  $d = $db->fetch($q);

  $login = $d['ulogin'];
  $email = $d['uemail'];
  $email_activated = $d['uemail_activated'];
  $vk_id = $d['uvk_id'];
  $first_name = $d['uname'];
  $last_name = $d['ulast_name'];
  $nike = $d['nik'];
  $avatar = $d['uavatar'];
  $gender = $d['ugender'];

  if($login) {
   return '
    <div id="admin_user_edit_info_error"></div>
    <div class="overflow_field">
     <div class="label label_f">Имя:</div>
     <div class="field"><input iplaceholder="Не указано" id="user_edit_info_name" type="text" value="'.fxss($first_name).'"></div>
    </div>
    <div class="overflow_field">
     <div class="label label_f">Фамилия:</div>
     <div class="field"><input iplaceholder="Не указано" id="user_edit_info_last_name" type="text" value="'.fxss($last_name).'"></div>
    </div>
	<div class="overflow_field">
     <div class="label label_f">Russian Express:</div>
     <div class="field"><input iplaceholder="Не указано" id="user_edit_info_last_nik" type="text" value="'.$nike.'"></div>
    </div>
    <div class="overflow_field">
     <div class="label label_f">Логин:</div>
     <div class="field"><input iplaceholder="Не указано" id="user_edit_info_login" type="text" value="'.$login.'"></div>
    </div>
    <div class="overflow_field">
     <div class="label label_f">E-mail:</div>
     <div class="field"><input iplaceholder="Не указано" id="user_edit_info_email" type="text" value="'.$email.'"></div>
    </div>
    <div class="overflow_field">
     <div class="label label_f">ID ВКонтакте:</div>
     <div class="field"><input iplaceholder="Не указано" id="user_edit_info_vk" type="text" value="'.($vk_id ? $vk_id : '').'"></div>
    </div>
    <div class="overflow_field">
     <div class="label label_f">Ссылка на аватар:</div>
     <div class="field"><input iplaceholder="Не указано" id="user_edit_info_avatar" type="text" value="'.fxss($avatar).'"></div>
    </div>
    <div class="overflow_field">
     <div class="label label_f">Пол:</div>
     <div class="field">
      <div id="edit_info_gender">
	 
      </div>
      <input type="hidden" id="old_edit_info_gender" value="'.$gender.'">
     </div>
    </div>
   ';
  } else {
   return 'Ошибка доступа.';
  }
 }

 public function edit_info_post() {
  global $db, $dbName, $user_id, $user_logged, $ugroup, $logs;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  if($ugroup != 4) {
   return json_encode(array('access' => 'denied'));
   exit;
  }

  $uid = (int) $_POST['uid'];
  $post_name = $db->escape(trim($_POST['name']));
  $post_last_name = $db->escape(trim($_POST['last_name']));
  $post_avatar = $db->escape(trim($_POST['avatar']));
  $post_login = $db->escape(trim($_POST['login']));
  $post_email = $db->escape(trim($_POST['email']));
  $post_vk_id = (int) abs($_POST['id_vk']);
  $post_gender = (int) abs($_POST['gender']);
  $post_nike = $db->escape(trim($_POST['last_nik']));

  $q = $db->query("SELECT `ulogin`, `uemail`, `uemail_activated`, `uvk_id`, `uname`, `ulast_name`, `uavatar`, `ugender`, `nik` FROM `users` WHERE `uid` = '$uid'");
  $d = $db->fetch($q);

  $login = $d['ulogin'];
  $email = $d['uemail'];
  $email_activated = $d['uemail_activated'];
  $vk_id = $d['uvk_id'];
  $first_name = $d['uname'];
  $last_name = $d['ulast_name'];
  $avatar = $d['uavatar'];
  $gender = $d['ugender'];
  $nike = $d['nik'];

  if($login) {
   if($first_name == $post_name && $last_name == $post_last_name && $login == $post_login && $email == $post_email && $vk_id == $post_vk_id && $avatar == $post_avatar && $nike == $post_nike && $gender == $post_gender) {
    $json = array('error_text' => 'Вы ничего не изменили.');
   } elseif(user::check_login($post_login) && $post_login != $login) {
    $json = array('error_text' => 'Такой логин уже занят.');
   } elseif(!preg_match('/^([@a-zA-Z0-9]){1,30}$/i', $post_login)) {
    $json = array('error_text' => 'Поле «логин» может содержать только латинские символы или цифры и не превышать 30 символов.');
   } elseif(user::check_email($post_email) && $post_email != $email) {
    $json = array('error_text' => 'Такой e-mail уже занят.');
   } elseif(!preg_match("/^[a-zA-Z0-9_\.\-]+@([a-zA-Z0-9\-]+\.)+[a-zA-Z]{2,6}$/i", $post_email)) {
    $json = array('error_text' => 'Поле «e-mail» имеет неправильный формат.');
   } elseif(user::check_vk_id($post_vk_id) && $post_vk_id != $vk_id && $post_vk_id) {
    $json = array('error_text' => 'Такая страница ВКонтакте уже зарегистрирована.');
   } else {
    if($db->query("UPDATE  `$dbName`.`users` SET  `ulogin` =  '$post_login',
	 `nik` =  '$post_nike',
     `uemail` =  '$post_email',
     `uemail_activated` = '1',
     `uvk_id` =  '$post_vk_id',
     `uname` =  '$post_name',
     `ulast_name` =  '$post_last_name',
     `ugender` = '$post_gender',
     `uavatar` =  '$post_avatar' WHERE  `users`.`uid` = '$uid' LIMIT 1 ;
    ")) {
     $logs_edit = '{"new_name":"'.$post_name.'", "new_last_name":"'.$post_last_name.'", "new_login":"'.$post_login.'", "new_email":"'.$post_email.'", "new_vk_id":"'.$post_vk_id.'", "new_avatar":"'.$post_avatar.'", "new_gender":"'.$post_gender.'", "old_name":"'.$first_name.'", "old_last_name":"'.$last_name.'", "old_login":"'.$login.'", "old_email":"'.$email.'", "old_vk_id":"'.$vk_id.'", "old_avatar":"'.$avatar.'", "old_gender":"'.$gender.'"}';
     $logs->user_edit_info($user_id, $uid, $logs_edit);
     $json = array('success' => 1);
    } else {
     $json = array('error_text' => 'Ошибка соединения с сервером. Попробуйте позже.');
    }
   }
  } else {
   $json = array('error_text' => 'id');
  }

  return json_encode($json);
 }

 public function edit_info_history_num() {
  global $db;

  $uid = (int) $_GET['uid'];

  $q = $db->query("SELECT `lid` FROM `logs` WHERE (`lmodule` = '1' AND `lmodule_type` = '8' OR `lmodule` = '1' AND `lmodule_type` = '1' OR `lmodule` = '1' AND `lmodule_type` = '2') AND `lto` = '$uid'");
  $n = $db->num($q);

  return $n;
 }

 public function edit_info_history() {
  global $db, $user_id, $user_logged, $ugroup, $logs, $site_url;

  if(!$user_logged) {
   return 'login';
   exit;
  }

  if($ugroup != 4) {
   return 'Access Denied';
   exit;
  }

  $uid = (int) $_GET['uid'];
  $page = (int) $_GET['page'];
  $start_page = (!$page) ? 0 : $page - 1;
  $start_limit = $start_page * 10;
  $num = user::edit_info_history_num();

  $q = $db->query("
   SELECT logs.lfrom, logs.ltext, logs.lip_address, logs.lbrowser, logs.ltime, logs.lmodule, logs.lmodule_type, users.uname, users.ulast_name FROM `logs`
    INNER JOIN `users` ON logs.lfrom = users.uid
   WHERE (logs.lmodule = '1' AND logs.lmodule_type = '8' OR logs.lmodule = '1' AND logs.lmodule_type = '1' OR logs.lmodule = '1' AND logs.lmodule_type = '2') AND logs.lto = '$uid'
   ORDER BY logs.lid DESC
   LIMIT $start_limit, 10
  ");
  while($d = $db->assoc($q)) {
   $from = $d['lfrom'];
   $text = json_decode($d['ltext'], true);
   $ip = $d['lip_address'];
   $browser = $d['lbrowser'];
   $time = $d['ltime'];
   $name = $d['uname'];
   $last_name = $d['ulast_name'];
   $module = $d['lmodule'];
   $module_type = $d['lmodule_type'];

   // Старый пол
   if(!$text['old_gender']) {
    $old_gender = 'Не выбран';
   } elseif($text['old_gender'] == 2) {
    $old_gender = 'Мужской';
   } else {
    $old_gender = 'Женский';
   }

   // Новый пол
   if(!$text['new_gender']) {
    $new_gender = 'Не выбран';
   } elseif($text['new_gender'] == 2) {
    $new_gender = 'Мужской';
   } else {
    $new_gender = 'Женский';
   }

   if($module == 1 && $module_type == 8) {
    $template .= '
     <div class="admin_edit_info_history_overflow">
      <div class="admin_edit_info_history_overflow_ftype">
       <a href="/admin/modules/users/?search='.$site_url.'id'.$from.'" onclick="nav.go(this); return false"><b>'.($name ? $name : 'Безымянный').' '.$last_name.'</b></a> изменил информацию:
       <div class="admin_edit_info_history_overflow_fsystem"><span>IP:</span> '.$ip.'; <span>Браузер:</span> '.$browser.'; <span>Время:</span> '.new_time($time).';</div>
      </div>
      <div class="overflow_field">
       <div class="label">Имя:</div>
       <div class="field"><span class="admin_edit_info_history_overflow_new_string">'.($text['new_name'] ? $text['new_name'] : 'Безымянный').'</span> / '.($text['old_name'] ? $text['old_name'] : 'Безымянный').'</div>
      </div>
      <div class="overflow_field">
       <div class="label">Фамилия:</div>
       <div class="field"><span class="admin_edit_info_history_overflow_new_string">'.($text['new_last_name'] ? $text['new_last_name'] : 'Не указано').'</span> / '.($text['old_last_name'] ? $text['old_last_name'] : 'Не указано').'</div>
      </div>
      <div class="overflow_field">
       <div class="label">Логин:</div>
       <div class="field"><span class="admin_edit_info_history_overflow_new_string">'.$text['new_login'].'</span> / '.$text['old_login'].'</div>
      </div>
      <div class="overflow_field">
       <div class="label">E-mail:</div>
       <div class="field"><span class="admin_edit_info_history_overflow_new_string">'.$text['new_email'].'</span> <br /> '.$text['old_email'].'</div>
      </div>
      <div class="overflow_field">
       <div class="label">ID ВКонтакте:</div>
       <div class="field"><span class="admin_edit_info_history_overflow_new_string">id'.$text['new_vk_id'].'</span> / id'.$text['old_vk_id'].'</div>
      </div>
      <div class="overflow_field">
       <div class="label">Ссылка на аватар:</div>
       <div class="field"><span class="admin_edit_info_history_overflow_new_string">'.($text['new_avatar'] ? $text['new_avatar'] : 'Не указано').'</span> <br /> '.($text['old_avatar'] ? $text['old_avatar'] : 'Не указано').'</div>
      </div>
      <div class="overflow_field">
       <div class="label">Пол:</div>
       <div class="field"><span class="admin_edit_info_history_overflow_new_string">'.$new_gender.'</span> / '.$old_gender.'</div>
      </div>
     </div>
    ';
   } elseif($module == 1 && $module_type == 1) {
    $template .= '
     <div class="admin_edit_info_history_overflow">
      <div class="admin_edit_info_history_overflow_ftype">
       <a href="/admin/modules/users/?search='.$site_url.'id'.$from.'" onclick="nav.go(this); return false"><b>'.($name ? $name : 'Безымянный').' '.$last_name.'</b></a> изменил информацию:
       <div class="admin_edit_info_history_overflow_fsystem"><span>IP:</span> '.$ip.'; <span>Браузер:</span> '.$browser.'; <span>Время:</span> '.new_time($time).';</div>
      </div>
      <div class="overflow_field">
       <div class="label">Пароль:</div>
       <div class="field"><span class="admin_edit_info_history_overflow_new_string">'.$text['new_password'].'</span> / '.$text['old_password'].'</div>
      </div>
     </div>
    ';
   } elseif($module == 1 && $module_type == 2) {
    $template .= '
     <div class="admin_edit_info_history_overflow">
      <div class="admin_edit_info_history_overflow_ftype">
       <a href="/admin/modules/users/?search='.$site_url.'id'.$from.'" onclick="nav.go(this); return false"><b>'.($name ? $name : 'Безымянный').' '.$last_name.'</b></a> изменил информацию:
       <div class="admin_edit_info_history_overflow_fsystem"><span>IP:</span> '.$ip.'; <span>Браузер:</span> '.$browser.'; <span>Время:</span> '.new_time($time).';</div>
      </div>
      <div class="overflow_field">
       <div class="label">Логин:</div>
       <div class="field"><span class="admin_edit_info_history_overflow_new_string">'.$text['new_login'].'</span> / '.$text['old_login'].'</div>
      </div>
     </div>
    ';
   }
  }
  return $template ? '
   <div id="admin_edit_info_box_pages">
    '.pages_ajax(array('ents_count' => $num, 'ents_print' => 10, 'page' => $page)).'
    <span class="pages_ajax"><div class="upload"></div></span>
   </div>
   '.$template.'
  ' : '<div id="admin_edit_info_box_none">История редактирований пуста.</div>';
 }

 public function blocked() {
  global $db, $dbName, $user_id, $user_logged, $ugroup, $time, $browser, $ip_address;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  if($ugroup != 4) {
   return json_encode(array('access' => 'denied'));
   exit;
  }

  $uid = (int) $_GET['uid'];
  $text = $db->escape(trim($_GET['text']));

  $q = $db->query("SELECT `uban_type` FROM `users` WHERE `uid` = '$uid'");
  $d = $db->fetch($q);

  $uban_type = $d['uban_type'];

  if(mb_strlen($text, 'UTF-8') < 3) {
   $json = array('error_text' => 'Слишком короткая причина.');
  } elseif($uban_type) {
   $json = array('error_text' => 'Аккаунт уже заблокирован.');
  } else {
   if($db->query("UPDATE  `$dbName`.`users` SET  `uban_type` =  '1', `uban_text` = '$text' WHERE  `users`.`uid` = '$uid';")) {
    $db->query("INSERT INTO `$dbName`.`logs_blocked` (`id`, `uid`, `to`, `type`, `text`, `time`, `browser`, `ip`) VALUES (NULL, '$user_id', '$uid', '1', '$text', '$time', '$browser', '$ip_address');");
    $json = array('success' => 1);
   } else {
    $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
   }
  }

  return json_encode($json);
 }

  public function upoints_fine() {
  global $db, $dbName, $user_id, $user_logged, $ugroup, $time, $browser, $ip_address;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  if($ugroup != 4) {
   return json_encode(array('access' => 'denied'));
   exit;
  }

  $uid = (int) $_GET['uid'];
  $upoints_fine = $db->escape(trim($_GET['upoints_fine']));
  $text = $db->escape(trim($_GET['text']));
  $section = '1';
  $tid = '1';
  $type = '3';
  $we = '1';

  if(mb_strlen($text, 'UTF-8') < 3) {
   $json = array('error_text' => 'Слишком короткая причина.');
  } elseif($upoints_fine < 1) {
   $json = array('error_text' => 'Необходимо ввести количество штрафных баллов.');
 } elseif($uid == 1) {
   $json = array('error_text' => 'Ага а хуй тебе.');
  } else {
   if($db->query("UPDATE `$dbName`.`users` SET `complaints` = `complaints` + '$we' WHERE `users`.`uid` = '$uid';")) {
    $db->query("INSERT INTO `$dbName`.`complaints` (`id`, `from`, `to`, `section`, `url`, `tid`, `time`, `type`, `upoints`, `browser`, `ip`) VALUES (NULL, '$user_id', '$uid', '$section', '$text', '$tid', '".time()."', '$type', '$upoints_fine', '$browser', '$ip_address');");
    $db->query("UPDATE `$dbName`.`users` SET `upoints_fine` = `upoints_fine` + '$upoints_fine' WHERE `users`.`uid` = '$uid';");
	$json = array('success' => 1);
   } else {
    $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
   }
  }
  return json_encode($json);
 }

 public function ucategory() {
  global $db, $dbName, $user_id, $user_logged, $ugroup, $time, $browser, $ip_address;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  if($ugroup != 3 && $ugroup != 4) {
   return json_encode(array('access' => 'denied'));
   exit;
  }

  $uid = (int) $_GET['uid'];
  $ucategory = $db->escape(trim($_GET['ucategory']));

  if($ucategory < 1) {
   $json = array('error_text' => 'Необходимо указать № категории');
  } else {
   if($db->query("UPDATE `$dbName`.`users` SET `archive` = `archive` + '1', `category` = '$ucategory'  WHERE `users`.`uid` = '$uid';")) {
      $db->query("INSERT INTO `$dbName`.`archive` (`id`, `from`, `to`, `section`, `url`, `tid`, `time`, `type`, `upoints`, `browser`, `ip`, `ucategory`, `module`) VALUES (NULL, '$user_id', '$uid', '1', '$text', '1', '".time()."', '1', '0', '$browser', '$ip_address', '$ucategory', '1');");
      $db->query("INSERT INTO `$dbName`.`ucategory_history` (`id`, `from`, `uid`, `ucategory`, `time`) VALUES (NULL, '$user_id', '$uid', '$ucategory', '".time()."');");
	 
    $check_award = award($uid, 0, 6);
       if($check_award) {
           $json = array('success' => 1);
       } else {
           $error = array('error_text' => 'Не могу проверить достижения...');
       }
   } else {
    $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
   }
  }
  return json_encode($json);
 }

  public function send_points() {
   global $db, $dbName, $user_id, $user_logged, $ugroup, $time, $browser, $ip_address, $award;

    if(!$user_logged) {
     return json_encode(array('error_text' => 'login'));
     exit;
    }

    if($ugroup != 4) {
     return json_encode(array('access' => 'denied'));
     exit;
    }

    $uid = (int) $_GET['uid'];
    $upoints = $db->escape(trim($_GET['upoints']));

    $section = '1';
    $tid = '1';
    $type = '2';
    $we = '1';

  if($upoints < 1) {
   $json = array('error_text' => 'Необходимо ввести количество баллов');
  } else {
   if($db->query("UPDATE `$dbName`.`users` SET `archive` = `archive` + '$we', `convoys_time` = '$time', `convoys_count` = `convoys_count` + '1' WHERE `users`.`uid` = '$uid';")) {
    $db->query("INSERT INTO `$dbName`.`archive` (`id`, `from`, `to`, `section`, `url`, `tid`, `time`, `type`, `upoints`, `browser`, `ip`, `ucategory`, `module`) VALUES (NULL, '$user_id', '$uid', '$section', '$text', '$tid', '".time()."', '$type', '$upoints', '$browser', '$ip_address', '0', '1');");

    addBalancePoints($upoints, $uid, 1);

    $q = $db->query("SELECT `convoys_count`, `category` FROM `users` WHERE `uid` = '$uid'");
    $d = $db->assoc($q);
    $convoys_count = $d['convoys_count'];
    $category = $d['category'];
    $award = award($uid, $convoys_count, 2); //достижения

    if($award) {
       $json = array('success' => 1);
    } else {
       $json = array('error_text' => 'Не могу проверить достижения...');
    }
   } else {
    $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
   }
  }
  return json_encode($json);
 }

 public function unblocked() {
  global $db, $dbName, $user_id, $user_logged, $ugroup, $time, $browser, $ip_address;

  $uid = (int) $_GET['uid'];

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  if($ugroup != 4) {
   return json_encode(array('access' => 'denied'));
   exit;
  }

  $q = $db->query("SELECT `uban_type`, `ref_uid` FROM `users` WHERE `uid` = '$uid'");
  $d = $db->fetch($q);

  $uban_type = $d['uban_type'];
  $ref_uid = $d['ref_uid'];
  $count_ref = user::ref_num($ref_uid);

  if($uban_type) {
   if($db->query("UPDATE  `$dbName`.`users` SET  `uban_type` =  '0', `uban_text` = '' WHERE  `users`.`uid` = '$uid';")) {
    $db->query("INSERT INTO `$dbName`.`logs_blocked` (`id`, `uid`, `to`, `type`, `text`, `time`, `browser`, `ip`) VALUES (NULL, '$user_id', '$uid', '2', '$text', '$time', '$browser', '$ip_address');");
    $check_award = award($ref_uid, $count_ref, 1);
    $json = array('success' => 1);
   } else {
    $json = array('error' => 1);
   }
  } else {
   $json = array('error' => 1);
  }

  return json_encode($json);
 }

 public function blocked_history_num() {
  global $db;

  $uid = (int) $_GET['uid'];

  $q = $db->query("SELECT `id` FROM `logs_blocked` WHERE `to` = '$uid'");

  return $db->num($q);
 }

 public function blocked_history() {
  global $db, $dbName, $user_id, $user_logged, $ugroup, $site_url;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  if($ugroup != 4) {
   return json_encode(array('access' => 'denied'));
   exit;
  }

  $uid = (int) $_GET['uid'];
  $page = (int) $_GET['page'];
  $start_page = (!$page) ? 0 : $page - 1;
  $start_limit = $start_page * 10;
  $num = user::blocked_history_num();

  $q = $db->query("
   SELECT users.uname, users.ulast_name, logs_blocked.uid, logs_blocked.type, logs_blocked.text, logs_blocked.time, logs_blocked.browser, logs_blocked.ip
   FROM `logs_blocked`
    INNER JOIN `users` ON logs_blocked.uid = users.uid
   WHERE logs_blocked.to = '$uid'
   ORDER BY logs_blocked.id DESC
   LIMIT $start_limit, 10
  ");

  while($d = $db->fetch($q)) {
   $from = $d['uid'];
   $first_name = $d['uname'];
   $last_name = $d['ulast_name'];
   $type = $d['type'];
   $text = $d['text'];
   $time = $d['time'];
   $browser = $d['browser'];
   $ip = $d['ip'];

   if($type == 1) {
    $template .= '
     <div class="admin_edit_info_history_overflow">
      <div class="admin_edit_info_history_overflow_ftype">
       <a href="/admin/modules/users/?search='.$site_url.'id'.$from.'" onclick="nav.go(this); return false"><b>'.no_name($first_name.' '.$last_name).'</b></a> <div class="user_list_ban_status">заблокировал доступ</div> пользователя:
       <div class="admin_edit_info_history_overflow_fsystem"><span>IP:</span> '.$ip.'; <span>Браузер:</span> '.$browser.'; <span>Время:</span> '.new_time($time).';</div>
      </div>
      Причина: <b>'.nl2br($text).'</b>
     </div>';
   } elseif($type == 2) {
    $template .= '
     <div class="admin_edit_info_history_overflow">
      <div class="admin_edit_info_history_overflow_ftype">
       <a href="/admin/modules/users/?search='.$site_url.'id'.$from.'" onclick="nav.go(this); return false"><b>'.no_name($first_name.' '.$last_name).'</b></a> <div class="user_list_ok_status">принял в компанию</div> пользователя:
       <div class="admin_edit_info_history_overflow_fsystem"><span>IP:</span> '.$ip.'; <span>Браузер:</span> '.$browser.'; <span>Время:</span> '.new_time($time).';</div>
      </div>
     </div>';
   }
  }
  return $template ? '
   <div id="admin_edit_info_box_pages">
    '.pages_ajax(array('ents_count' => $num, 'ents_print' => 10, 'page' => $page)).'
    <span class="pages_ajax"><div class="upload"></div></span>
   </div>
   '.$template.'
  ' : '<div id="admin_edit_info_box_none">История блокировок пуста.</div>';
 }

  public function fine_history_num() {
  global $db;

  $uid = (int) $_GET['uid'];

  $q = $db->query("SELECT `id` FROM `complaints` WHERE `to` = '$uid'");

  return $db->num($q);
 }

  public function fine_history() {
  global $db, $dbName, $user_id, $user_logged, $ugroup, $site_url;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  if($ugroup != 4) {
   return json_encode(array('access' => 'denied'));
   exit;
  }

  $uid = (int) $_GET['uid'];
  $page = (int) $_GET['page'];
  $start_page = (!$page) ? 0 : $page - 1;
  $start_limit = $start_page * 10;
  $num = user::fine_history_num();

  $q = $db->query("
   SELECT users.uname, users.ulast_name, complaints.from, complaints.to, complaints.url, complaints.time, complaints.type, complaints.upoints, complaints.browser, complaints.ip
   FROM `complaints`
    INNER JOIN `users` ON complaints.from = users.uid
   WHERE complaints.to = '$uid'
   ORDER BY complaints.id DESC
   LIMIT $start_limit, 10
  ");

  while($d = $db->fetch($q)) {
   $from = $d['from'];
   $uto = $d['to'];
   $url = $d['url'];
   $first_name = $d['uname'];
   $last_name = $d['ulast_name'];
   $type = $d['type'];
   $time = $d['time'];
   $upoints = $d['upoints'];
   $browser = $d['browser'];
   $ip = $d['ip'];

    $template .= '
     <div class="admin_edit_info_history_overflow">
      <div class="admin_edit_info_history_overflow_ftype">
       <a href="/admin/modules/users/?search='.$site_url.'id'.$from.'" onclick="nav.go(this); return false"><b>'.no_name($first_name.' '.$last_name).'</b></a> <div class="user_list_ban_status">выписал штраф</div> пользователю:
       <div class="admin_edit_info_history_overflow_fsystem"><span>IP:</span> '.$ip.'; <span>Браузер:</span> '.$browser.'; <span>Время:</span> '.new_time($time).';</div>
      </div>
	  Количество баллов: <b>'.$upoints.'</b> '.declOfNum(abs($upoints), array('балл', 'балла', 'баллов')).'<br>
      Причина: <b>'.nl2br($url).'</b>
     </div>';
  }
  return $template ? '
   <div id="admin_edit_info_box_pages">
    '.pages_ajax(array('ents_count' => $num, 'ents_print' => 10, 'page' => $page)).'
    <span class="pages_ajax"><div class="upload"></div></span>
   </div>
   '.$template.'
  ' : '<div id="admin_edit_info_box_none">История выписывания пуста.</div>';
 }

 public function send_points_history_num() {
  global $db;

  $uid = (int) $_GET['uid'];

  $q = $db->query("SELECT `id` FROM `balance_history` WHERE `to` = '$uid' AND `type` = '1'");

  return $db->num($q);
 }

  public function send_points_history() {
  global $db, $user_logged, $ugroup, $site_url;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  if($ugroup != 4) {
   return json_encode(array('access' => 'denied'));
   exit;
  }

  $uid = (int) $_GET['uid'];
  $page = (int) $_GET['page'];
  $start_page = (!$page) ? 0 : $page - 1;
  $start_limit = $start_page * 10;
  $num = user::send_points_history_num();

  $q = $db->query("
   SELECT users.uname, users.ulast_name, balance_history.from, balance_history.to, balance_history.points, balance_history.time, balance_history.type
   FROM `balance_history`
    INNER JOIN `users` ON balance_history.from = users.uid
   WHERE balance_history.to = '$uid'
   ORDER BY balance_history.id DESC
   LIMIT $start_limit, 10
  ");

  while($d = $db->fetch($q)) {
   $from = $d['from'];
   $first_name = $d['uname'];
   $last_name = $d['ulast_name'];
   $time = $d['time'];
   $upoints = $d['points'];

    $template .= '
     <div class="admin_edit_info_history_overflow">
      <div class="admin_edit_info_history_overflow_ftype">
       <a href="/admin/modules/users/?search='.$site_url.'id'.$from.'" onclick="nav.go(this); return false"><b>'.no_name($first_name.' '.$last_name).'</b></a> <div class="user_list_ok_status">начислил баллы</div> сотруднику:
       <div class="admin_edit_info_history_overflow_fsystem"><span>Время:</span> '.new_time($time).';</div>
      </div>
	  Количество баллов: <b>'.$upoints.'</b> '.declOfNum(abs($upoints), array('балл', 'балла', 'баллов')).'<br>
     </div>';
  }
  return $template ? '
   <div id="admin_edit_info_box_pages">
    '.pages_ajax(array('ents_count' => $num, 'ents_print' => 10, 'page' => $page)).'
    <span class="pages_ajax"><div class="upload"></div></span>
   </div>
   '.$template.'
  ' : '<div id="admin_edit_info_box_none">История выписывания пуста.</div>';
 }

 public function ucategory_history() {
  global $db, $user_logged, $ugroup, $site_url;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  if($ugroup != 4) {
   return json_encode(array('access' => 'denied'));
   exit;
  }

  $uid = (int) $_GET['uid'];
  $page = (int) $_GET['page'];
  $start_page = (!$page) ? 0 : $page - 1;
  $start_limit = $start_page * 10;
  $num = user::send_points_history_num();

  $q = $db->query("
   SELECT users.uname, users.ulast_name, archive.from, archive.to, archive.time, archive.type, archive.ucategory, archive.browser, archive.ip
   FROM `archive`
    INNER JOIN `users` ON archive.from = users.uid
   WHERE archive.to = '$uid'
   ORDER BY archive.id DESC
   LIMIT $start_limit, 10
  ");

  while($d = $db->fetch($q)) {
   $from = $d['from'];
   $first_name = $d['uname'];
   $last_name = $d['ulast_name'];
   $time = $d['time'];
   $category = $d['ucategory'];
   $browser = $d['browser'];
   $ip = $d['ip'];
   $category_result = getCategoryTitle($category);

    $template .= '
     <div class="admin_edit_info_history_overflow">
      <div class="admin_edit_info_history_overflow_ftype">
       <a href="/admin/modules/users/?search='.$site_url.'id'.$from.'" onclick="nav.go(this); return false"><b>'.no_name($first_name.' '.$last_name).'</b></a> <div class="user_list_ok_status">изменил категорию</div> пользователю:
       <div class="admin_edit_info_history_overflow_fsystem"><span>IP:</span> '.$ip.'; <span>Браузер:</span> '.$browser.'; <span>Время:</span> '.new_time($time).';</div>
      </div>
	  Новая категория: <b>'.$category_result.'</b><br>
     </div>';
  }
  return $template ? '
   <div id="admin_edit_info_box_pages">
    '.pages_ajax(array('ents_count' => $num, 'ents_print' => 10, 'page' => $page)).'
    <span class="pages_ajax"><div class="upload"></div></span>
   </div>
   '.$template.'
  ' : '<div id="admin_edit_info_box_none">История изменений пуста.</div>';
 }

 public function rules_add() {
  global $db, $logs, $dbName, $user_logged, $users_nik;

  $uid = $_POST['uid'];
  $rules = $_POST['rules'];

  $q = $db->query("SELECT `nik` FROM `users` WHERE `uid` = '$uid'");
  $d = $db->fetch($q);
  $users_nik = $d['nik'];

  if(!$user_logged) {
   $error = array('error_msg' => 'Ошибка доступа.');
  } else {
   if($rules == 1) {
    $db->query("UPDATE `$dbName`.`users` SET `rules` = '1' WHERE `users`.`uid` = '$uid';");

	$logs->rules_add($uid, $uid, ''.$users_nik.', принял правила компании');

    $error = array('response' => 1, 'count' => $count, 'points' => $points);
   } else {
    $error = array('error_msg' => 'Ошибка соединения с сервером. Попробуйте позже.');
   }
  }

  return json_encode($error);
 }

 public function search() {
  global $db, $user_logged, $ugroup, $noavatar;

  $search = $_GET['search'];

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  $q = $db->query("SELECT * FROM `users` WHERE (`uname` LIKE '%" . $search ."%' OR `nik` LIKE '%" . $search ."%' OR `ulast_name` LIKE '%" . $search ."%') AND `udel` NOT LIKE '%1%' LIMIT 0, 5");
  while($d = $db->assoc($q)) {

  $uid = $d['uid'];
  $ulogin = $d['ulogin'];
  $first_name = $d['uname'];
  $ulast_name = $d['ulast_name'];
  $uavatar = $d['uavatar'] ? $d['uavatar'] : $noavatar;
  $users_nik = $d['nik'];
  $ulast_time = $d['ulast_time'];
  $upoints = $d['upoints'];
  $fpoints = $d['upoints_fine'];

  if($ugroup == 4) {
    $user_url = 'nav.go(\'\', \'/admin/modules/users/?search=https://vtc-express.ru/id'.$uid.'\')';
  } else {
    $user_url = 'nav.go(\'\', \'/id'.$uid.'\')';
  }

    $template .='
    <div class="task_mini_info" onclick="'.$user_url.'">
	   <div class="task_mini_info_avatar"><img src="'.$uavatar.'">
		 </div><div class="task_mini_info_title">'.$first_name.' '.$ulast_name.'
		 </div>
		<div class="task_mini_info_desc">Никнейм: '.$users_nik.' | <b>'.$upoints.'</b> '.declOfNum(abs($upoints), array('балл', 'балла', 'баллов')).' и <b>'.$fpoints.'</b> штрафных '.declOfNum(abs($fpoints), array('балл', 'балла', 'баллов')).'</div></div>
    ';
  }
  return json_encode(array('messages' => $template));
 }

 public function admin_ugroup() {
  global $db, $ugroup, $user_logged;

  if(!$user_logged) {
   return 'login';
   exit;
  }

  if($ugroup != 4) {
   return 'Access Denied';
   exit;
  }

  $uid = (int) $_GET['uid'];

  $q = $db->query("SELECT `ugroup` FROM `users` WHERE `uid` = '$uid'");
  $d = $db->fetch($q);

  $response_ugroup = $d['ugroup'];
  $null = 1;

  if($null) {
   return '
    <div id="admin_user_edit_info_error"></div>
     <div class="overflow_field">
      <div class="label label_f">Выберите тип:</div>
       <div class="field">
       <div id="user_edit_info_gender">

	  <input type="hidden" id="asddsa123" value="'.$response_ugroup.'">
	  </div>
     </div>
     <input type="hidden" id="user_edit_info_gender_value" value="'.$response_ugroup.'">
    </div>
   ';
  } else {
   return 'Ошибка доступа.';
  }
 }

public function edit_admin_post() {
 global $db, $dbName, $user_id, $user_logged, $ugroup, $logs;

 if(!$user_logged) {
  return json_encode(array('error_text' => 'login'));
  exit;
 }

 if($ugroup != 4 || $user_id != 3) {
  return json_encode(array('access' => 'denied'));
  exit;
 }

 $uid = (int) $_POST['uid'];
 $post_ugroup = $db->escape(trim($_POST['info_admin']));

 $q = $db->query("SELECT `ugroup` FROM `users` WHERE `uid` = '$uid'");
 $d = $db->fetch($q);

 $qugroup = $d['ugroup'];
 $null = 1;

 if($qugroup == 3) {
  $text_admin = 'Инструктором';
 } elseif($qugroup == 4) {
  $text_admin = 'Администратором';
 } else {
  $text_admin = 'Сотрудником';
 }

 if($null) {
  if($qugroup == $post_ugroup) {
   $json = array('error_text' => 'Сотрудник уже является '.$text_admin);
  } else {
   if($db->query("UPDATE  `$dbName`.`users` SET `ugroup` = '$post_ugroup' WHERE  `users`.`uid` = '$uid' LIMIT 1 ;")) {
    $json = array('success' => 1);
   } else {
    $json = array('error_text' => 'Ошибка соединения с сервером. Попробуйте позже.');
   }
  }
 } else {
  $json = array('error_text' => 'id');
 }

 return json_encode($json);
}


 public function new_users_num() {
  global $db;

  $uid = (int) $_GET['uid'];

  $q = $db->query("SELECT `uid` FROM `users` WHERE `uban_type` > 0");

  return $db->num($q);
 }

 public function new_employee_num() {
  global $db;

  $uid = (int) $_GET['uid'];

  $q = $db->query("SELECT `uid` FROM `users` WHERE `uban_type` = 0");

  return $db->num($q);
 }

 public function users_convoy_num() {
  global $db, $user_id;

  $q = $db->query("SELECT `id` FROM `archive` WHERE `module` = '1' AND `type` = '2' AND `to` = '$user_id' ORDER BY `id` DESC");
  $n = $db->num($q);

  return $n;
 }

 public function users_urgent_num() {
  global $db, $user_id;

  $q = $db->query("SELECT `id` FROM `urgent` WHERE `status` = '2' AND `no_active` = '1' AND `user_id` = '$user_id' ORDER BY `id` DESC");
  $n = $db->num($q);

  return $n;
 }

 public function users_chat_num() {
  global $db, $user_id;

  $q = $db->query("SELECT `id` FROM `chat_messages` WHERE `user_id` = '$user_id'");

  return $db->num($q);
 }

 public function ref_num($user_id = null) {
   global $db;

   $q = $db->query("SELECT `uid` FROM `users` WHERE `ref_uid` = '$user_id' AND `uban_type` = '0'");
   $n = $db->num($q);

   return $n;
 }


 public function users_number() {
  global $db, $dbName, $time;

  $section_sql = "ORDER BY upoints DESC";
  $q = $db->query("SELECT * FROM users WHERE `udel` = '0' AND `uban_type` = '0' ".$section_sql." ");

  $a = 1;
  while($d = $db->fetch($q)) {

    $array[] = $d['uid'];
    $query = "INSERT INTO `$dbName`.`users_number` (`number`, `uid`, `time`) VALUES ";
    foreach ($array as $i => $value) {
    $query .= "(Null, '{$value}', '$time')";
    if ($i + 1 < count($array)) {
      $query .= ", ";
    }
   }

  $a++;
  }

  $db->query($query);
  $json = array('success' => 1);

  return json_encode($json);
 }

 public function number_delete() {
  global $db;

  $q = $db->query("TRUNCATE TABLE `users_number`");

  return $q;
 }

 public function info_number($user_id = null) {
  global $db;

  $q = $db->query("SELECT `number` FROM users_number WHERE `uid` = '$user_id'");
  $d = $db->fetch($q);

  $number = $d['number'];

  return $number ? $number : 0;
 }
}

$user = new user;
?>
