<?php
class top {
 public function users_num($top_uid) {
  global $db, $user_id;
  $top_date = 'AND UNIX_TIMESTAMP(INTERVAL - 30 day + CURRENT_DATE()) < `time`';
  $q = $db->query("SELECT `upoints` FROM `archive` WHERE `to` = '$top_uid' ".$top_date."");

  return $db->num($q);
 }

 public function users_list() {
  global $db, $noavatar; {
   $section_sql = "ORDER BY upoints DESC";
   $top_date = 'AND UNIX_TIMESTAMP(INTERVAL - 30 day + CURRENT_DATE()) < `time`';

   $q = $db->query("SELECT * FROM users WHERE `upoints` > 0 AND `udel` = '0' AND `uban_type` = '0' ".$section_sql." LIMIT 100");

   /*$q = $db->query("
    SELECT users.nik, users.uavatar, users.ugender, users.ugroup, users.upoints, archive.to, archive.upoints, archive.type, archive.time
    FROM `archive`
     INNER JOIN `users` ON users.uid = archive.to
    WHERE users.ugroup < 4 AND users.upoints > 0 AND `udel` = '0' AND `uban_type` = '0' AND UNIX_TIMESTAMP(INTERVAL - 30 day + CURRENT_DATE()) < archive.time
    ORDER BY archive.upoints DESC
    LIMIT 100
   ");*/

   /*$q = $db->query("
    SELECT `users`.`uid`, `users`.`uavatar`, `users`.`uvk_id`, `users`.`uname`, `users`.`ulast_name`, `users`.`ulast_time`, `users`.`ugender`, `users`.`nik`,
    COUNT(`archive`.`upoints`) as `count_refs`
    FROM `archive`
     INNER JOIN `users` ON (`archive`.`to` = `users`.`uid`)
    WHERE `users`.`uban_type` = 0 AND `users`.`uvk_id` > 0 ".$top_date."
    GROUP BY `to`
    ORDER BY `count_refs` DESC
    LIMIT 100
   ");*/

   $i = 1;
   while($d = $db->assoc($q)) {
	  $top_uid = $d['uid'];
    $top_avatar = $d['uavatar'] ? $d['uavatar'] : $noavatar;
    $top_vk_id = $d['uvk_id'];
    $top_first_name = $d['uname'];
    $top_last_name = $d['ulast_name'];
    $top_last_time = $d['ulast_time'];
    $top_gender = $d['ugender'];
	  $top_users_nik = $d['nik'];
	  $top_users_upoints = $d['upoints'];
	  $top_test = top::users_num($top_uid);

    if($i == 1) {
     $top_user_num = '<span style="color:#daa520;">'.$i.'</span>';
    } elseif($i == 2) {
     $top_user_num = '<span style="color:#959292;">'.$i.'</span>';
    } elseif($i == 3) {
     $top_user_num = '<span style="color:#c46e1a;">'.$i.'</span>';
    } else {
     $top_user_num = '<span style="color:#333;">'.$i.'</span>';
    }

    $template .= '
       <div class="wrap-users">
           <div class="inner">
               <div class="image"><img src="'.$top_avatar.'"></div>
               <div class="title"><a href="/id'.$top_uid.'" onclick="nav.go(this); return false">Russian Express: '.$top_users_nik.'</a> в топе на → <b>'.$top_user_num.' месте</b></div>
               <div class="amount">'.$top_users_upoints.' '.declOfNum($top_users_upoints, array('балл', 'балла', 'баллов')).'</div>
           </div>
       </div>
    ';
    $i++;
   }
  }
  return $template;
 }
}

$top = new top;
?>
