<?php
class donation
{
	public function donation_list()
	{
		global $db, $noavatar;

		$q = $db->query(
		 "
    SELECT logs_pay_points.id, logs_pay_points.uid, logs_pay_points.points, logs_pay_points.time, logs_pay_points.status, users.uname, users.ulast_name, users.uavatar
    FROM `logs_pay_points`
     INNER JOIN `users` ON logs_pay_points.uid = users.uid
    WHERE logs_pay_points.points > 0 AND logs_pay_points.status = 1
    ORDER BY logs_pay_points.points DESC
     "
		);

		while ($d = $db->assoc($q)) {
			$donation_uid = $d['uid'];
			$donation_points = $d['points'];
			$donation_time = $d['time'];
			$donation_uname = $d['uname'];
			$donation_ulast_name = $d['ulast_name'];
			$donation_uavatar = $d['uavatar'];
			$donation_avatar = $donation_uavatar ? $donation_uavatar : $noavatar;

			$template .= '
			<div class="wrap-users">
					<div class="inner">
							<div class="image"><img src="'.$donation_avatar.'"></div>
							<div class="title"><a href="/id'.$donation_uid.'" onclick="nav.go(this); return false">'.$donation_uname.' '.$donation_ulast_name.'</a> сделал пожертвования на сумму <b>'.$donation_points.' '.declOfNum($donation_points, array('рубль', 'рубля', 'рублей')).'</b></div>
							<div class="icon-love"><img src="/images/love_donation.png"></div>
					</div>
			</div>
    ';
		}
		return $template;
	}
}

$donation = new donation;
