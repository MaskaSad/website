<?php
class profile
{
	public function users_info($id_users = null) {
		global $db, $noavatar, $user_id, $user_logged;

		$uid = $id_users;
		$section = $db->escape($_GET['section']);
		$list_section_t = ($section == 'edit') ? 1 : 0;

		$q = $db->query(
			"SELECT `uid`, `uvk_id`, `uname`, `ulast_name`, `uban_type`, `city`, `ugender`, `year`, `uage`, `ureg_time`, `uavatar`, `upoints`, `category`, `ugroup`, `upoints_fine`, `uban_type`, `convoys_count`, `mile`, `money`, `count_tasks`, `nik`, `server`, `truck`, `day`, `month`, `cover` FROM `users` WHERE `uid` = '$uid'"
		);
		while ($d = $db->assoc($q)) {
			$qid = $d['uid'];
			$uname = $d['uname'];
			$ulast_name = $d['ulast_name'];
			$ugender = $d['ugender'];
			$uage = $d['uage'];
			$uban_type = $d['uban_type'];
			$qage = $uage ? $uage : 'неизвестен';
			$ureg_time = $d['ureg_time'];
			$uavatar = $d['uavatar'];
			$upoints = $d['upoints'];
			$upoints_fine = $d['upoints_fine'];
			$qavatar = $uavatar ? $uavatar : $noavatar;
			$qufirst_name = $uname ? $uname . ' ' . $ulast_name : 'Безымянный';
			$category = $d['category'];
			$ugroup = $d['ugroup'];
			$num_convoys = profile::users_convoy_num($qid);
			$num_urget = profile::users_urgent_num($qid);
			$num_chat = profile::users_chat_num($qid);
			$mile = $d['mile'];
			$money = $d['money'];
			$count_tasks = $d['count_tasks'];
			$convoys_count = $d['convoys_count'];
      $nik = $d['nik'];
      $qnik = $nik ? $nik : 'неизвестен';
      $uvk_id = $d['uvk_id'];
      $uvk_id_page = $uvk_id ? '<a href="https://vk.com/id'.$uvk_id.'" target="_blank">vk.com/id'.$uvk_id.'</a>' : 'не прикреплена';
      $uban_type = $d['uban_type'];
      $city = $d['city'];
      $qcity = $city ? $city : 'не указан';
      $server = $d['server'];
      $truck = $d['truck'];
      $day = $d['day'];
      $month = $d['month'];
      $year = $d['year'];
      $cover2 = $d['cover'];
      $cover = $cover2 ? $cover2 : '/images/no_cover.jpg';

      if($month == 1) {
        $result_month = 'января';
      } elseif($month == 2) {
        $result_month = 'февраля';
      } elseif($month == 3) {
        $result_month = 'марта';        
      } elseif($month == 4) {
        $result_month = 'апреля';        
      } elseif($month == 5) {
        $result_month = 'мая';
      } elseif($month == 6) {
        $result_month = 'июня';
      } elseif($month == 7) {
        $result_month = 'июля';
      } elseif($month == 8) {
        $result_month = 'августа';
      } elseif($month == 9) {
        $result_month = 'сентября';
      } elseif($month == 10) {
        $result_month = 'октября';
      } elseif($month == 11) {
        $result_month = 'ноября';
      } elseif($month == 12) {
        $result_month = 'декабря';
      } else {
        $result_month = '';
      }

      if($day > 0 && $month > 0 && $year > 0) {
        $result_date = $day.' '.$result_month;
        $result_age = ' '.$year.' '.declOfNum(abs($get_year), array('г.', 'г.', 'г.')).' ';
      } else {
        $result_date = 'не указана';
      }

			if (!$ugender) {
				$result_ugender = 'не указан';
			} elseif ($ugender == 2) {
				$result_ugender = 'Мужской';
			} else {
				$result_ugender = 'Женский';
			}

            $category_result = getCategoryTitle($category);

			if ($ugroup == 4) {
				$ugroup_result = 'Администратор';
			} elseif ($ugroup == 3) {
				$ugroup_result = 'Инструктор';
			} elseif ($ugroup == 5) {
				$ugroup_result = 'Агент поддержки #' . $uagent_id;
			} else {
				$ugroup_result = 'Сотрудник';
			}

      if($server == 1) {
        $result_server = 'Europe 1 [ETS2];';
      } elseif ($server == 2) {
        $result_server = 'Europe 2 [ETS2];';
      } elseif ($server == 3) {
        $result_server = 'Europe 3 [ETS2];';
      } elseif ($server == 4) {
        $result_server = 'United States [ETS2];';
      } elseif ($server == 5) {
        $result_server = 'South America [ETS2];';
      } elseif ($server == 6) {
        $result_server = 'Hong Kong [ETS2];';
      } else {
        $result_server = 'Сервер не выбран.';
      }
       
      if ($truck == 1) {
        $result_truck = 'Daf XF 105 (2005 г.);';
      } elseif ($truck == 2) {
        $result_truck = 'Daf XF Euro 6 (2013 г.);';
      } elseif ($truck == 3) {
        $result_truck = 'Iveco Stralis (2002 г.);';
      } elseif ($truck == 4) {
        $result_truck = 'Iveco Stralis Hi-Way (2013 г.);';
      } elseif ($truck == 5) {
        $result_truck = 'Man TGX (2008 г.);';
      } elseif ($truck == 6) {
        $result_truck = 'Mercedes-Benz Actros MP3 (2009 г.);';
      } elseif ($truck == 7) {
        $result_truck = 'Mercedes-Benz Actros MP4 (2014 г.);';
      } elseif ($truck == 8) {
        $result_truck = 'Renault Magnum IV (2005 г.);';
      } elseif ($truck == 9) {
        $result_truck = 'Renault Premium Route (2006 г.);';
      } elseif ($truck == 10) {
        $result_truck = 'Scania R (2004 г.);';
      } elseif ($truck == 11) {
        $result_truck = 'Scania Streamline (2013 г.);';
      } elseif ($truck == 12) {
        $result_truck = 'Volvo FH16 (2012 г.);';
      } elseif ($truck == 13) {
        $result_truck = 'Volvo FH16 (2009 г.);';
      } elseif ($truck == 14) {
        $result_truck = 'Skoda Superb';
      } else {
        $result_truck = 'Тягач не выбран.';
      }


			if($uid == $user_id && $user_logged && $list_section_t == 0) {
			   $edit = '<div class="icons"><div class="edit" title="Редактировать" onclick="nav.go(\'\', \'/id'.$uid.'?section=edit\'); return false"><i></i></div></div>';
			} elseif($uid == $user_id && $user_logged && $list_section_t == 1) {
			   $edit = '<div class="icons"><div class="exit" title="Закрыть" onclick="nav.go(\'\', \'/id'.$uid.'\'); return false"><i></i></div></div>';
			} else {
				 $edit = '';
			}

      if($user_id == $uid) {
        $up_avatar = '<label for="avatar-upload"><div class="overlay"><span class="profile_upload_avatar"></span></div></label>';
        $up_cover = '<label for="cover-upload"><div class="page_cover_action delete"></div></label>';
      } else {
        $up_avatar = '';
        $up_cover = '';
      }

   if($list_section_t == 1 && $user_id == $uid) {
			$template .= '

<div class="header_profile" id="result_cover" style="background-image: url('.$cover.');">
    <div class="header_cover_menu">
        '.$up_cover.'
    </div>
     <iframe id="support_upload_iframe" name="cover_upload_iframe"></iframe>
     <form id="avatar_upload" method="post" enctype="multipart/form-data" action="/profile/cover.upload" target="cover_upload_iframe">
        <input id="cover-upload" onchange="users.upload_cover(); return false;" type="file" name="file">
        <input id="cover_upload_iframe_submit" style="display: none;" type="submit">
     </form>
    <div class="user_img">
     <div class="profile_avatar" id="result_avatar" style="background-image: url('.$qavatar.');">
     <div id="upload_avatar_loader"></div>
      '.$up_avatar.'
     </div>
     <iframe id="support_upload_iframe" name="avatar_upload_iframe"></iframe>
     <form id="avatar_upload" method="post" enctype="multipart/form-data" action="/profile/avatar.upload" target="avatar_upload_iframe">
        <input id="avatar-upload" onchange="users.upload_avatar(); return false;" type="file" name="file">
        <input id="avatar_upload_iframe_submit" style="display: none;" type="submit">
     </form>
        <h3>'.$qufirst_name.'</h3>
        <h5>'.$qnik.'</h5>
        <div class="fl_r" id="upload_cover_loader"></div>
    </div>
</div>

<div class="user_counts">
    <div class="counts">
        <a class="page_counter" href="#">
            <div class="count">'.$convoys_count.'</div>
            <div class="label">'.declOfNum($convoys_count, array('конвой', 'конвоя', 'конвоев')).'</div>
        </a>
        <a class="page_counter" href="#">
            <div class="count">'.$count_tasks.'</div>
            <div class="label">срочных заказов</div>
        </a>
        <a class="page_counter" href="#">
            <div class="count">'.$mile.'</div>
            <div class="label">проехал</div>
        </a>
        <a class="page_counter" href="#">
            <div class="count">'.$money.'</div>
            <div class="label">заработал</div>
        </a>
        <a class="page_counter" href="#">
            <div class="count">'.$upoints.'</div>
            <div class="label">'.declOfNum($upoints, array('балл', 'балла', 'баллов')).'</div>
        </a>
        <a class="page_counter" href="#">
            <div class="count">'.$upoints_fine.'</div>
            <div class="label">штрафов</div>
        </a>
    </div>
   </div>

<div class="profile_width">
  <div class="clearfix">

    <div id="profile_left" class="fl_l">
      <div id="user_info_wrap">
        <div class="user_tabs">
          <a class="tab  active" href="#" onclick="nav.go(this); return false;">
            <div class="tabdiv">Информация</div>
          </a>
          <a class="tab" href="#" onclick="nav.go(this); return false;">
            <div class="tabdiv">Заявления</div>
          </a>
          <a class="tab" href="#" onclick="nav.go(this); return false;">
            <div class="tabdiv">Отзывы</div>
          </a>
          '.$edit.'
        </div>
        <div class="page_info">
      <div id="form_edit_inner">
       <div id="form_edit_error"></div>
       <div class="field_title">Имя:</div>
       <div class="field_input">
        <input type="text" id="edit_name" maxlength="20" iplaceholder="Введите Имя" class="placeholder" value="'.$uname.'">
       <div class="error" id="error_name"></div>
      </div>
      <div class="field_title">Фамилия:</div>
       <div class="field_input">
        <input type="text" id="edit_lastname" maxlength="30" iplaceholder="Введите Фамилию" class="placeholder" value="'.$ulast_name.'">
       <div class="error" id="error_lastname"></div>
      </div>
      <div class="field_title">Дата рождения:</div>
       <div class="field_input" style="width: 400px;">
        <div id="users_day"></div>
        <input type="hidden" id="old_users_day_value" value="'.$day.'">
        <div id="users_separator">—</div>
        <div id="users_month"></div>
        <input type="hidden" id="old_users_month_value" value="'.$month.'">
        <div id="users_separator">—</div>
        <div id="users_year"></div>
        <input type="hidden" id="old_users_year_value" value="'.$year.'">
       <div class="error" id="error_date"></div>
      </div>
      <div class="field_title">Город:</div>
       <div class="field_input">
        <input type="text" id="edit_city" iplaceholder="Укажите город" class="placeholder" value="'.$qcity.'">
       <div class="error" id="error_city"></div>
      </div>
      <div id="success"></div>
      <div onclick="users.change_name();" id="edit_button" class="blue_button_wrap">
       <div class="blue_button">
        Сохранить информацию
       <div class="blue_button_arrow_right"></div>
       </div>
      </div>
     </div>

        </div>
      </div>
    </div>

    <div id="profile_right" class="fl_r">
      <div id="user_info_wrap">
        <div class="user_tabs">
          <a class="tab  active" href="#" onclick="nav.go(this); return false;">
            <div class="tabdiv">Интересы</div>
          </a>
        </div>
        <div class="page_info">
      <div id="form_edit_inner" style="width: 221px;">
       <div id="form_edit_error_track"></div>
       <div class="field_title">Игровой сервер:</div>
       <div class="field_input">
        <div id="server">
      
        <input type="hidden" value="'.$server.'" id="old_server"/>
        </div>
       <div class="error reg_login"></div>
      </div>
      <div class="field_title">Выберите тягач:</div>
       <div class="field_input"> 
        <div id="truck">

        <input type="hidden" value="'.$truck.'" id="old_truck"/>
        </div>
       <div class="error reg_password"></div>
      </div>
      <div id="success_truck"></div>
      <div onclick="users.user_interests();" id="edit_track" class="blue_button_wrap" style="width: 233px;">
       <div class="blue_button">
        Сохранить информацию
       <div class="blue_button_arrow_right"></div>
       </div>
      </div>
     </div>


        </div>
      </div>
    </div>
    
  </div>
</div>
    ';

} elseif(!$list_section_t) {

			$template .= '

<div class="header_profile" id="result_cover" style="background-image: url('.$cover.');">
    <div class="header_cover_menu">
        '.$up_cover.'
    </div>
     <iframe id="support_upload_iframe" name="cover_upload_iframe"></iframe>
     <form id="avatar_upload" method="post" enctype="multipart/form-data" action="/profile/cover.upload" target="cover_upload_iframe">
        <input id="cover-upload" onchange="users.upload_cover(); return false;" type="file" name="file">
        <input id="cover_upload_iframe_submit" style="display: none;" type="submit">
     </form>
    <div class="user_img">
     <div class="profile_avatar" id="result_avatar" style="background-image: url('.$qavatar.');">
     <div id="upload_avatar_loader"></div>
      '.$up_avatar.'
     </div>
     <iframe id="support_upload_iframe" name="avatar_upload_iframe"></iframe>
     <form id="avatar_upload" method="post" enctype="multipart/form-data" action="/profile/avatar.upload" target="avatar_upload_iframe">
        <input id="avatar-upload" onchange="users.upload_avatar(); return false;" type="file" name="file">
        <input id="avatar_upload_iframe_submit" style="display: none;" type="submit">
     </form>
        <h3>'.$qufirst_name.'</h3>
        <h5>'.$qnik.'</h5>
        <div class="fl_r" id="upload_cover_loader"></div>
    </div>
</div>


<div class="user_counts">
    <div class="counts">
        <a class="page_counter" href="#">
            <div class="count">'.$convoys_count.'</div>
            <div class="label">'.declOfNum($convoys_count, array('конвой', 'конвоя', 'конвоев')).'</div>
        </a>
        <a class="page_counter" href="#">
            <div class="count">'.$count_tasks.'</div>
            <div class="label">срочных заказов</div>
        </a>
        <a class="page_counter" href="#">
            <div class="count">'.$mile.'</div>
            <div class="label">проехал</div>
        </a>
        <a class="page_counter" href="#">
            <div class="count">'.$money.'</div>
            <div class="label">заработал</div>
        </a>
        <a class="page_counter" href="#">
            <div class="count">'.$upoints.'</div>
            <div class="label">'.declOfNum($upoints, array('балл', 'балла', 'баллов')).'</div>
        </a>
        <a class="page_counter" href="#">
            <div class="count">'.$upoints_fine.'</div>
            <div class="label">штрафов</div>
        </a>
    </div>
   </div>

<div class="profile_width">
  <div class="clearfix">

    <div id="profile_left" class="fl_l">
      <div id="user_info_wrap">
        <div class="user_tabs">
          <a class="tab  active" href="#" onclick="nav.go(this); return false;">
            <div class="tabdiv">Информация</div>
          </a>
          <a class="tab" href="#" onclick="nav.go(this); return false;">
            <div class="tabdiv">Заявления</div>
          </a>
          <a class="tab" href="#" onclick="nav.go(this); return false;">
            <div class="tabdiv">Отзывы</div>
          </a>
          '.$edit.'
        </div>
        <div class="page_info">
          <div class="title_info">Дата рождения:</div>
          <div class="info_value">'.$result_date.' '.$result_age.'</div>
          <div class="title_info">Город:</div>
          <div class="info_value">'.$qcity.'</div>
          <div class="title_info">Сотрудник:</div>
          <div class="info_value">RussianExpress</div>
          <div class="title_info">ВКонтакте:</div>
          <div class="info_value">'.$uvk_id_page.'</div>
          <div class="title_info">Категория:</div>
          <div class="info_value">'.$category_result.'</div>
          <div class="title_info">Статус аккаунта:</div>
          <div class="info_value">'.($uban_type ? '<div class="user_list_ban_status">не активирован</div>' : '<div class="user_list_ok_status">активен</div>').'</span></div>
        </div>
      </div>
    </div>

    <div id="profile_right" class="fl_r">
      <div id="user_info_wrap">
        <div class="user_tabs">
          <a class="tab  active" href="#" onclick="nav.go(this); return false;">
            <div class="tabdiv">Интересы</div>
          </a>
        </div>
        <div class="page_info">
          <div class="title_interests">Игровой сервер:</div>
          <div class="interests_value">'.$result_server.'</div>
          <div class="title_interests">Любимый тягач:</div>
          <div class="interests_value">'.$result_truck.'</div>
          <div class="title_interests">Компания:</div>
          <div class="interests_value"><a href="#" id="company">Russian Express</a></div>
        </div>
      </div>
    </div>

  </div>

 <div id="user_info_wrap">
   <div id="profile_award">
        <div class="user_tabs">
          <a class="tab  active" href="#" onclick="nav.go(this); return false;">
            <div class="tabdiv">Content</div>
          </a>
        </div>
        <div class="page_info">
        <div class="empty">Content</div>
        </div>
   </div>
  </div>

</div>
    ';

 }

		}
		if (!$qid) {
			return array(
				'title' => '404 Not found', 'del' => 1, 'template' => '<div id="site_page" class="main"><div id="site_page_content_none">Страница удалена либо ещё не создана.</div></div>'
			);
		} elseif ($qid) {
			return array('response' => 1, 'title' => $qufirst_name, 'template' => $template);
		} else {
			return array(
				'title' => '404 Not found', 'template' => '<div id="site_page" class="main"><div id="site_page_content_none">Страница удалена либо ещё не создана.</div></div>'
			);
		}
	}

	public function users_convoy_num($uid = null) {
		global $db;

		$q = $db->query("SELECT `id` FROM `archive` WHERE `module` = '1' AND `type` = '2' AND `to` = '$uid' ORDER BY `id` DESC");
		$n = $db->num($q);

		return $n;
	}

	public function users_urgent_num($uid = null) {
		global $db;

		$q = $db->query("SELECT `id` FROM `urgent` WHERE `status` = '2' AND `no_active` = '1' AND `user_id` = '$uid' ORDER BY `id` DESC");
		$n = $db->num($q);

		return $n;
	}

	public function users_chat_num($uid = null) {
		global $db;

		$q = $db->query("SELECT `id` FROM `chat_messages` WHERE `user_id` = '$uid'");

		return $db->num($q);
	}

 public function upload_avatar() {
  global $db, $dbName, $root, $picture, $user_logged, $user_id, $time;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  $__dir = $root.'/images/uploads/avatars/';
  $__format_explode = explode('.', $_FILES['file']['name']);
  $__format = $__format_explode[count($__format_explode) - 1]; // получаем формат изображения
  $__format_type = array('jpeg', 'JPEG', 'jpg', 'JPG', 'png', 'PNG', 'gif', 'GIF', 'bmp', 'BMP');
  $__file_rand_name = rand_str(10);
  $__file_mini_rand_name = rand_str(15);
  $__file_name = $__dir.''.$__file_rand_name.'.jpg';
  $size = getimagesize ($_FILES['file']['tmp_name']);
  $width = $size[0];
  $height = $size[1];

  // открываем последние загруженные изображения
  $qattach_img_limit = time() - (30 * 60);
  $qattach_img = $db->query("SELECT `id` FROM `attaches_img` WHERE `time` >= '$qattach_img_limit' AND `uid` = '$user_id'");
  $nattach_img = $db->num($qattach_img);

  if($nattach_img >= 30) {
   $json = array('error_text' => 'Вы загружаете слишком много изображений. Попробуйте позже.');
  } elseif(!in_array($__format, $__format_type)) {
   $json = array('error_text' => 'Неизвестный формат изображения.');
  } elseif($width < 800) {
   $json = array('error_text' => 'Размеры изображения должно быть не меньше 800x800');
  } elseif($height < 800) {
   $json = array('error_text' => 'Размеры изображения должно быть не меньше 800x800');
  } else {
   if(copy($_FILES['file']['tmp_name'], $__file_name)) {
    // если файл загружен, то создаем уменьшенное изображение
    $new_image = new picture($__dir.'/'.$__file_rand_name.'.jpg');
    $new_image->autoimageresize(260, 260);
    $new_image->imagesave('jpeg', $__dir.'/'.$__file_mini_rand_name.'.jpg');
    $new_image->imageout();
    $db->query("INSERT INTO `$dbName`.`attaches_img` (`id`, `uid`, `small`, `big`, `time`, `modules`) VALUES ('', '$user_id', '$__file_mini_rand_name', '$__file_rand_name', '$time', '14');");
    // получаем id загруженного изображения
    $qImg = $db->query("SELECT `id` FROM `attaches_img` WHERE `small` = '$__file_mini_rand_name' AND `uid` = '$user_id'");
    $dImg = $db->fetch($qImg);
    $db->query("UPDATE  `$dbName`.`users` SET `uavatar` = '/images/uploads/avatars/$__file_rand_name.jpg' WHERE  `users`.`uid` = '$user_id' LIMIT 1 ;");
    $json = array('success' => 1, 'id' => $dImg['id'], 'avatar_src' => '/images/uploads/avatars/'.$__file_rand_name.'.jpg', 'result_mini_file' => $__file_mini_rand_name.'.jpg', 'user_id' => $user_id);

   } else {
    $json = array('error_text' => 'Неизвестная ошибка.');
   }
  }

  return json_encode($json);
 }

  public function upload_cover() {
  global $db, $dbName, $root, $picture, $user_logged, $user_id, $time;

  if(!$user_logged) {
   return json_encode(array('error_text' => 'login'));
   exit;
  }

  $__dir = $root.'/images/uploads/covers/';
  $__format_explode = explode('.', $_FILES['file']['name']);
  $__format = $__format_explode[count($__format_explode) - 1]; // получаем формат изображения
  $__format_type = array('jpeg', 'JPEG', 'jpg', 'JPG', 'png', 'PNG', 'gif', 'GIF', 'bmp', 'BMP');
  $__file_rand_name = rand_str(10);
  $__file_mini_rand_name = rand_str(15);
  $__file_name = $__dir.''.$__file_rand_name.'.jpg';
  $size = getimagesize ($_FILES['file']['tmp_name']);
  $width = $size[0];
  $height = $size[1];

  // открываем последние загруженные изображения
  $qattach_img_limit = time() - (30 * 60);
  $qattach_img = $db->query("SELECT `id` FROM `attaches_img` WHERE `time` >= '$qattach_img_limit' AND `uid` = '$user_id'");
  $nattach_img = $db->num($qattach_img);

  if($nattach_img >= 30) {
   $json = array('error_text' => 'Вы загружаете слишком много изображений. Попробуйте позже.');
  } elseif(!in_array($__format, $__format_type)) {
   $json = array('error_text' => 'Неизвестный формат изображения.');
  } elseif($width < 1590) {
   $json = array('error_text' => 'Размеры изображения должно быть не меньше 1590×400.');
  } elseif($height < 400) {
   $json = array('error_text' => 'Размеры изображения должно быть не меньше 1590×400.');
  } else {
   if(copy($_FILES['file']['tmp_name'], $__file_name)) {
    // если файл загружен, то создаем уменьшенное изображение
    $new_image = new picture($__dir.'/'.$__file_rand_name.'.jpg');
    $new_image->autoimageresize(260, 260);
    $new_image->imagesave('jpeg', $__dir.'/'.$__file_mini_rand_name.'.jpg');
    $new_image->imageout();
    $db->query("INSERT INTO `$dbName`.`attaches_img` (`id`, `uid`, `small`, `big`, `time`, `modules`) VALUES ('', '$user_id', '$__file_mini_rand_name', '$__file_rand_name', '$time', '15');");
    // получаем id загруженного изображения
    $qImg = $db->query("SELECT `id` FROM `attaches_img` WHERE `small` = '$__file_mini_rand_name' AND `uid` = '$user_id'");
    $dImg = $db->fetch($qImg);
    $db->query("UPDATE  `$dbName`.`users` SET `cover` = '/images/uploads/covers/$__file_rand_name.jpg' WHERE  `users`.`uid` = '$user_id' LIMIT 1;");
    $json = array('success' => 1, 'cover_src' => '/images/uploads/covers/'.$__file_rand_name.'.jpg');

   } else {
    $json = array('error_text' => 'Неизвестная ошибка.');
   }
  }

  return json_encode($json);
 }
}

$profile = new profile;
