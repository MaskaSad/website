<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'support';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');
require($root.'/inc/classes/sessions.php');
include($root.'/inc/system/usession.php');
require($root.'/inc/classes/support.php');

$support_my_num = $support->my_num();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Помощь по сайту</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
<div id="header_load"></div>   
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
        <div class="tabs">
         <span class="tabs_left">
		  <a class="active" href="/support/faq" onclick="nav.go(this); return false;"><div class="tabdiv">FAQ</div></a>
          <a href="/support" onclick="nav.go(this); return false;"><div class="tabdiv">Мои вопросы</div></a>
          <? if($ugroup == 4 || $ugroup == 5) { ?><a href="/support/questions" onclick="nav.go(this); return false;"><div class="tabdiv">Все вопросы <? if($support_new) echo '(<b>'.$support_new.'</b>)'; ?></div></a><? } ?>
          <? if($ugroup == 4 || $ugroup == 5) { ?><a href="/support/rate" onclick="nav.go(this); return false;"><div class="tabdiv">Рейтинг агентов</div></a><? } ?>
         </span>
         <span class="tabs_right">
          <a class="other_a_tab" href="/support/new" onclick="nav.go(this); return false;">Новый вопрос</a>
         </span>
        </div>
        <div id="support_my_content">
          <div id="uTicketsFaq">
            <div href="/support/faq?act=faq&type=base" onclick="nav.go(this); return false;" style="margin-top: 1px; margin-left: 65px;" class="uTicketsFaqCat">
                <div class="uTicketsFaqCatLeft">
                    <img src="/images/uFaq.png">
                </div>
                <div class="uTicketsFaqCatRight">
                    <div  class="uTicketsFaqCatRightTitle">Базовый</div>
                    <div class="uTicketsFaqCatRightDesc">набор [часто задаваемые вопросы].</div>
                </div>
				  </div>

            <div href="/support/functional" onclick="nav.go(this); return false;" style="margin-top: 1px;" class="uTicketsFaqCat">
                <div class="uTicketsFaqCatLeft">
                    <img src="/images/uFaq.png">
                </div>

                <div class="uTicketsFaqCatRight">
                    <div class="uTicketsFaqCatRightTitle">Функциональный</div>
                    <div class="uTicketsFaqCatRightDesc">набор [часто задаваемые вопросы].</div>
                </div>
            </div>
         </div>
        </div>




        <div id="uTicketsFaqRow">
             <div class="uContent uLeft">
                <div class="uTitle">Как мне обменять баллы голоса?</div>
                <div class="uText">Что-бы обманять баллы на голоса у вас на балансе должно быть 350 баллов <div style="margin-top: 7px;"></div> Если у вас есть 350 баллов то перейдите в раздел <a href="/exchange" onclick="nav.go(this); return false;">обмен баллов на голоса</a> и там все будет понятно. </div>
            </div>
        </div>



       </div>
      </div>
     </div>
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
