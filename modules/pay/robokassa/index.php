<?php
$root = $_SERVER['DOCUMENT_ROOT'];

require($root.'/inc/classes/db.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
include($root.'/inc/system/profile.php');

$amount = $_GET['amount'];
$pen_id = $_GET['Desc'];
$act = $_GET['act'];
$unique_key = (($_GET['unique_key']) ? $_GET['unique_key'] : time() + rand(100, 1000) + $user_id);

$url = $robokassa['url'].'?MrchLogin='.$robokassa['login'].'&OutSum='.$amount.'&InvDesc='.$pen_id.'&InvId='.$unique_key.'&Desc='.$_GET['description'].'&shpuser_id='.$user_id.'&shpact='.$act.'&SignatureValue='.md5($robokassa['login'].':'.$amount.':'.$unique_key.':'.$robokassa['password_first'].':shpact='.$act.':shpuser_id='.$user_id);

header('Location: '.$url);
?>