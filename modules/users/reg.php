<?php
$root = $_SERVER['DOCUMENT_ROOT'];

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/logs.php');
require($root.'/inc/classes/sessions.php');
require($root.'/inc/classes/support.php');
require($root.'/inc/classes/users.php');

echo $user->reg(array(
 'first_name' => $_POST['first_name'],
 'last_name' => $_POST['last_name'],
 'login' => $_POST['login'],
 'password' => $_POST['password'],
 'email' => $_POST['email'],
 'reg_nickname' => $_POST['reg_nickname'],
 'users_day' => $_POST['users_day'],
 'users_month' => $_POST['users_month'],
 'users_year' => $_POST['users_year'],
 'vk_uid' => $_POST['vk_uid'],
 'invited_nickname' => $_POST['invited_nickname'],
 'captcha_code' => $_POST['captcha_code'],
 'captcha_key' => $_POST['captcha_key'],
 'ref' => $_POST['ref']
));
?>