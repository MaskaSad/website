<?php
$root = $_SERVER['DOCUMENT_ROOT'];

$page_name = 'donation';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
require($root.'/inc/system/profile.php');
require($root.'/inc/classes/donation.php');

if(!$user_logged) {
 // если пользователь неавторизован, перенаправляем на главную
 header('Location: /');
 exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Пожертвование компании</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
   <div id="header_load"></div>
   <div id="page">
     <? include($root.'/include/header.php') ?>
     <div id="content">
       <? include($root.'/include/left.php') ?>
       <div id="right_wrap">
         <div id="right_wrap_b">
           <div id="right">
             <div class="main nopad">
               <div class="info-header-block donation_img">
                 <h4>Пожертвование компании</h4>
                 <p>Данное пожертвование является добровольным, ваши деньги не уйдут ни кому в карман, они пойдут на развитие компании.</p>
                 <p>Минимальная сумма пожертвования — <b>30 руб</b></p>
                 <p>&#160;</p>
                 <div class="info-bottom" onclick="users._support_bottem();">
                  Если у вас возникли вопросы то обратитесь в поддержку »
                 </div>
               </div>
              <div class="donation-wrap">
                  <div class="yandex_money" style="margin: 0 auto;width: 450px;">
                  <iframe src="https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=Пожертвования&targets-hint=Помощь Russian Express&default-sum=&button-text=14&payment-type-choice=on&mobile-payment-type-choice=on&hint=&successURL=&quickpay=shop&account=41001825633776" width="450" height="213" frameborder="0" allowtransparency="true" scrolling="no"></iframe>
                  </div>
              </div>
             </div>
           </div>
         </div>
         <? include($root.'/include/footer.php') ?>
       </div>
     </div>
   </div>
   <? include($root.'/include/scripts.php') ?>
 </body>
</html>
