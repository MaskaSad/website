<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'edit_convoys';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/logs.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
require($root.'/inc/classes/sessions.php');
require($root.'/inc/classes/convoys.php');

$edit_form =  $convoys->edit_convoys();
$number = $user->info_number($user_id);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <title>Изменения конвоя</title>
    <? include($root.'/include/head.php'); ?>

</head>
<body>
<div id="header_load"></div>
<div id="page">
    <? include($root.'/include/header.php') ?>

    <div id="content">
        <? include($root.'/include/left.php') ?>

        <div id="right_wrap">
            <div id="right_wrap_b">
                <div id="right">
                    <div class="main nopad">
                        <div class="info-header-block convoys_img">
                            <h4>Мероприятия компании</h4>
                            <p>Здесь отображаются мероприятия добавленные администратором на ближайшие время.</p>
                            <p>Неадекватное поведение или оскорбление сотрудников компании карается увольнением с компании!</p>
                            <?php if($user_logged) { ?> <p><span class="convoys_number">Ваш порядковый номер в колонне <b><? echo $number; ?></b>.</span></p> <? } ?>
                            <div class="info-bottom" onclick="users._support_bottem();">
                                Если у вас возникли какие-то вопросы то напишите в поддержку »
                            </div>
                        </div>
                        <div class="tasks_tabs">
                            <a class="tab" href="/convoys/add?section=list" onclick="nav.go(this); return false;"><div class="tabdiv">&laquo; вернуться</div></a>
                        </div>

                        <?php echo $edit_form; ?>

                    </div>
                </div>
            </div>
            <? include($root.'/include/footer.php') ?>

        </div>
    </div>
</div>
<? include($root.'/include/scripts.php') ?>
</body>
</html>