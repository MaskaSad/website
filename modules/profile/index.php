<?php
$root = $_SERVER['DOCUMENT_ROOT'];

$page_name = 'profile';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
require($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');
require($root.'/inc/classes/profile.php');

if(!$user_logged) {
 // если пользователь неавторизован, перенаправляем на главную
 header('Location: /');
 exit;
}

$list_section = $_GET['section'];
$list_section_t = ($list_section == 'edit') ? 1 : 0;

$convoy_num = $user->users_convoy_num();
$urgent_num = $user->users_urgent_num();
$chat_num = $user->users_chat_num();

$info_users = $profile->users_info($_GET['uid']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
    <title><? echo $info_users['title']; ?></title>
    <? include($root.'/include/head.php') ?>
  </head>
  <body>
    <div id="header_load"></div>
    <div id="page">
      <? include($root.'/include/header.php') ?>
      <div id="content">
        <? include($root.'/include/left.php') ?>
        <div id="right_wrap">
          <div id="right_wrap_b">
            <div id="right">
              <div class="main nopad">
                <? echo $info_users['template']; ?>
              </div>
            </div>
          </div>
          <? include($root.'/include/footer.php') ?>
        </div>
      </div>
    </div>
    <? include($root.'/include/scripts.php') ?>
  </body>
</html>
