<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'urgent_r';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');
require($root.'/inc/classes/urgent.php');

$list_section = $_GET['section'];

if($ugroup != 4) {
 header('Location: /tasks');

 exit;
}

if($list_section == 'considered') {
  $section_title = 'Список рассмотренных отчётов';
} elseif($list_section == 'deleted') {
  $section_title = 'Список отклоненных отчётов';
} else {
  $section_title = 'Список отчётов';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Заявки срочные заказы</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
 <div id="header_load"></div>   
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
         <div class="tasks_tabs">
             <a class="tab active" href="/urgent/requests" onclick="nav.go(this); return false;"><div class="tabdiv">Все заявки</div></a>
             <a class="tab" href="/urgent/requests" onclick="nav.go(this); return false;"><div class="tabdiv">Расмотренные</div></a>
             <a class="tab" href="/urgent/requests" onclick="nav.go(this); return false;"><div class="tabdiv">Отклонненые</div></a>
         </div>
        <div class="list_tasks">
            <div id="head-info-block"><span><? echo $section_title; ?></span></div>
        <? echo $urgent->admin_requests(); ?>
        </div>
       </div>
      </div>
     </div>
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
