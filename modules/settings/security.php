<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'my_settings';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');
require($root.'/inc/classes/sessions.php');
include($root.'/inc/system/usession.php');

$vkid = $uvk_id ? '<a href="http://vk.com/id'.$uvk_id.'" target="_blank">vk.com/id'.$uvk_id.'</a>' : 'не прикреплена';
$vkbutton = $uvk_id ? '<div id="settings_steam_button" class="blue_button_wrap small_blue_button my_setting_account_change_account_no_active" onclick="accounts.change_vk(1);"><div class="blue_button">Отвязать страницу</div></div>' : '<div id="aLogin" data-ulogin="display=buttons;fields=first_name,last_name;redirect_uri=;callback=addvk"><div id="settings_steam_button" class="blue_button_wrap small_blue_button" data-uloginbutton = "vkontakte"><div class="blue_button">Привязать страницу</div></div></div>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Безопасность</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
 <div id="header_load"></div>   
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
         <div class="tasks_tabs">
             <a class="tab" href="/settings" onclick="nav.go(this); return false;"><div class="tabdiv">Общее</div></a>
             <a class="tab active" href="/settings/security" onclick="nav.go(this); return false;"><div class="tabdiv">Безопасность</div></a>
             <a class="tab" href="/settings/ref?menu=1" onclick="nav.go(this); return false;"><div class="tabdiv">Партнерская программа</div></a>
             <a class="tab" href="/settings/balance" onclick="nav.go(this); return false;"><div class="tabdiv">Баланс</div></a>
         </div>
        <div class="info-header-block security_img">
         <h4>Безопасность аккаунта</h4>
		      <p>В данным раздели вы сможете изменить <b>пароль</b> а так-же <b>E-mail</b></b></p>
		       <p>Не в коем случае не указывайте ваши данные от соц.сетей</p>
            <p>Не нужно прикреплять фейковые страницы ВКонтакте!</p>
            <div class="info-bottom" onclick="users._support_bottem();">
             Изменить E-mail вы сможете только написав в поддержку »
            </div>
          </div>
		 <div id="my_settings">
		 <div class="my_settings_content">
          <div id="my_settings_change_password">
           <div id="my_settings_change_password_name">Изменить пароль</div>
           <div id="my_settings_change_password_error"></div>
           <div id="my_settings_change_password_content">
            <div class="overflow_field">
             <div class="label">Старый пароль:</div>
             <div class="field"><input id="my_settings_old_password" type="password"></div>
            </div>
            <div class="overflow_field">
             <div class="label">Новый пароль:</div>
             <div class="field"><input id="my_settings_new_password" type="password"></div>
            </div>
            <div class="overflow_field">
             <div class="label">Повторите пароль:</div>
             <div class="field">
              <input id="my_settings_new_password2" type="password">
              <br />
              <div onclick="users.change_password();" id="settings_password_button" class="blue_button_wrap small_blue_button"><div class="blue_button">Изменить пароль</div></div>
             </div>
            </div>
           </div>
          </div>
		  <div id="my_settings_account">
           <div id="my_settings_account_name">Ваш персональный логин</div>
           <div id="my_settings_account_error"></div>
           <div id="my_settings_account_content">
            <div class="overflow_field">
             <div class="label">Текущий логин:</div>
             <div class="field field_settings_login"><? echo $user_login; ?></div>
            </div>
            <div class="overflow_field">
             <div class="label label_f">Новый логин:</div>
             <div class="field">
              <input id="my_settings_account_login_field" type="text">
              <br />
              <div onclick="users.change_login();" id="settings_login_button" class="blue_button_wrap small_blue_button"><div class="blue_button">Изменить логин</div></div>
             </div>
            </div>
           </div>
          </div>
		  <div id="my_settings_email">
           <div id="my_settings_email_name">Адрес Вашей электронной почты</div><? if(!$uemail_activated) { ?>
           <div id="my_settings_email_notif">
            На указанный адрес было отправлено письмо со ссылкой для подтверждения. Пожалуйста, проверьте Ваш почтовый ящик.
            <span id="reemail_a">
             <br />
             <a href="javascript://" onclick="users.reemail();">Отправить письмо повторно »</a>
            </span>
           </div><? } ?>
           <div id="my_settings_email_content">
            <div class="overflow_field">
             <div class="label">Текущий адрес:</div>
             <div class="field field_settings_login"><? echo $uemail; ?></div>
            </div>
           </div>
          </div>
		  <div id="my_settings_account">
           <div id="my_settings_account_name" style="margin-bottom: 10px;">Страница ВКонтакте</div>
           <div id="my_settings_account_new_vk_error"></div>
           <div id="my_settings_account_content">
            <div class="overflow_field">
             <div class="label">Текущая страница:</div>
             <div class="field field_settings_vk"><?php echo $vkid; ?></div>
            </div>
            <div class="overflow_field" id="new_nik">
             <div class="label label_f"> </div>
             <div class="field" style="margin-top: -8px;" id="button">
              <?php echo $vkbutton ?>
             </div>
            </div>
           </div>
          </div>
         </div>
		 </div>
       </div>
      </div>
     </div>
     <input type="hidden" value="<? echo $usession; ?>" id="ssid">
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
