<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'my_settings';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');
require($root.'/inc/classes/balance.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Баланс</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
 <div id="header_load"></div>   
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
         <div class="tasks_tabs">
             <a class="tab" href="/settings" onclick="nav.go(this); return false;"><div class="tabdiv">Общее</div></a>
             <a class="tab" href="/settings/security" onclick="nav.go(this); return false;"><div class="tabdiv">Безопасность</div></a>
             <a class="tab" href="/settings/ref?menu=1" onclick="nav.go(this); return false;"><div class="tabdiv">Партнерская программа</div></a>
             <a class="tab active" href="/settings/balance" onclick="nav.go(this); return false;"><div class="tabdiv">Баланс</div></a>
         </div>
        <div class="info-header-block balance_img">
         <h4>Ваш баланс</h4>
          <p>Здесь отображается состояние вашего счета.</p>
		      <p>Вы получаете <b>2 балла</b> за пройденный конвой если вы проехали половину конвоя то <b>1 балл.</b></p>
		      <p>Так-же если вы выполнили срочный заказ вы получаете <b>1 балл</b> на свой счёт.</p>
          <div class="info-bottom" onclick="users._support_bottem();">
           Если у вас возникли вопросы то обратитесь в поддержку »
          </div>
          </div>
		 <div class="users_balance">
		  <p>Ваш баланс: <b><? echo $upoints; ?> <? echo declOfNum(abs($upoints), array('балл.', 'балла.', 'баллов')); ?></b></p>
		 </div>
       </div>
      </div>
     </div>
     <input type="hidden" value="<? echo $usession; ?>" id="ssid">
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
