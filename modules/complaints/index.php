<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'my_complaints';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
include($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');
require($root.'/inc/classes/complaints.php');

$list_table_user_num = $complaints->list_table_user_num();

if($unew_complaints) {
 $db->query("UPDATE  `$dbName`.`users` SET  `complaints` = '0' WHERE  `users`.`uid` = '$user_id' LIMIT 1 ;");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Мои штрафы</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
 <div id="header_load"></div>   
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

     <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
        <div class="info-header-block fines_img">
         <h4>Штрафные баллы</h4>
          <p>Полностью обновленный раздел, теперь вы можете погасить штраф или же оплатить его.</p>
		       <p>Если вы не согласны со штрафом, вы можете подать на апелляцию (Нажать на крестик).</p>
            <p>Правила компании <a href="/blog?id=13" onclick="nav.go(this); return false;">ознакомится »</a></p>
            <div class="info-bottom" onclick="users._support_bottem();">
             Что-бы узнать причину штрафа напишите в поддержку »
            </div>
          </div>

         <div id="admin_complaints_content">
          <div id="head-info-block"><span>Вы получили <? echo $list_table_user_num.' '.declOfNum($list_table_user_num, array('штраф', 'штрафа', 'штрафов')); ?></span></div>
         <? if($list_table_user_num) { ?>
		<!--<div id="blacklist_bar">
          <div id="blacklist_bar_num">
           <? if(!$list_table_user_num) { ?>Ничего не найдено<? } else { ?>Вы получили <? echo $list_table_user_num.' '.declOfNum($list_table_user_num, array('штраф', 'штрафа', 'штрафов')); ?><? } ?>
          </div>
          <div id="blacklist_bar_page">
           <? echo pages(array('ents_count' => $list_table_user_num, 'ents_print' => 10, 'page' => $_GET['page'])); ?>
          </div>
         </div>	-->

		 <div class="complaints_list">
          <? echo $complaints->list_table_user(); ?>
		  </div>
         <? } else { ?>
         <div class="fines_comment_question">
          <div id="my_tasks_blacklist_none">Вы еще не получали штрафов.</div>
		</div>
         <? } ?>
        </div>
       </div>

      </div>
     </div>
<? include($root.'/include/footer.php') ?>

	 </div>
    </div>
   </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
