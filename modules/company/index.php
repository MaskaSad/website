<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'company';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
include($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Russian Express</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
<div id="header_load"></div>
  <div id="page">
    <? include($root.'/include/header.php') ?>
    <div id="content">
      <? include($root.'/include/left.php') ?>
      <div id="right_wrap">
        <div id="right_wrap_b">
          <div id="right">
            <div class="main nopad">
              <div class="info-company-wrap">
                <div class="cover" style="background-image: url(https://sun9-10.userapi.com/c841328/v841328330/79d8/w4X_wqkzkDE.jpg);"></div>
                <div class="fl_l avatar">
                  <a href="#">
                   <img src="https://pp.userapi.com/c841328/v841328330/79eb/JINKzR3cVa0.jpg" class="br60px" alt="Аватар">
                  </a>
                </div>
                <div class="block_title">Russian Express</div>
              </div>
            </div>
          </div>
        </div>
        <? include($root.'/include/footer.php') ?>
      </div>
    </div>
  </div>
  <? include($root.'/include/scripts.php') ?>
</body>
</html>
