<?php
$root = $_SERVER['DOCUMENT_ROOT'];

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
require($root.'/inc/classes/admin_balance.php');

if($ugroup != 4) {
  header('Location: /');
  exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Начисление баллов за конвой</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
   <div id="header_load"></div>
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
	  	<div class="balance_content" id="tasks_list">
		 <div class="panel_admin">
          <span class="count">Начисление баллов сотрудникам за пройденый конвой</span>
		   <div class="help_msg">Отметьте тех сотрудников которые проехали конвой, укажите кол-во баллов за конвой.</div>
          </div>
         <? echo $admin_balance->users_list(); ?>
          <div id="settings_balance_admin_users_add_f">
		   <div id="settings_balance_admin_users_add_field">
		   <input type="text" id="admin_balance_add_field_input" />
		   <input type="hidden" id="users_ids" />
		  </div>
		  <div id="settings_balance_admin_users_add_button_l">
		   <div id="admin_balance_users_add_button" class="blue_button_wrap small_blue_button" onclick="NEWUI.voutes()">
		    <div class="blue_button">Пополнить</div></div> <span id="admin_error_w"></span><div id="admin_error_result"></div>
			</div>
		  </div>
		 </div>
       </div>
      </div>
     </div>
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
