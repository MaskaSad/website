<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'admin.users';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/vk.api.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');

if($ugroup != 4) {
 header('Location: /');
 exit;
}

$users_list_num = $user->admin_users_list_num();
$sort = $_GET['sort'];
$section = $_GET['section'];
$search = $_GET['search'];

if($section == 'online' || $section == 'admin' || $section == 'instructor' || $section == 'agent' || $section == 'uban_type') {
 $section_result = $section;
} else {
 $section_result = '';
}

$newusers = $user->new_users_num();

if ($newusers > 0) {
  $num = '<span class="ft_count_users">'.$newusers.'</span>';
} else {
  $num = '';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Сотрудники</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
 <div id="header_load"></div>
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">

        <div class="tasks_tabs">
          <div class="ft_search">
          <input id="input_tasks_search" value="<? echo fxss($search); ?>" iplaceholder="Например: Николай Белый или никнейм сотрудника" type="text">
             <div id="task_add_url_loader"><div class="upload"></div></div>
             <div id="task_info_url"></div>
           </div>
            <a class="tab <? if(!$section_result) echo ' active'; ?>" href="/admin/modules/users/" onclick="nav.go(this); return false;"><div class="tabdiv">Все сотрудники</div></a>
            <a class="tab <? if($section_result == 'online') echo ' active'; ?>" href="/admin/modules/users/?section=online" onclick="nav.go(this); return false;"><div class="tabdiv">Сейчас на сайте</div></a>
            <span id="admin_actions_menu_d"></span>
            <div class="ft_users_search"></div>
        </div>
         
        <div class="ft_users_search_panel" style="display: none;">
          <input id="input_tasks_search" value="<? echo fxss($search); ?>" iplaceholder="Например: Николай Белый или никнейм сотрудника" type="text">
          <div id="task_add_url_loader"><div class="upload"></div></div>
          <div id="task_info_url"></div>
        </div>
        <? if(!$users_list_num) { ?>
        <div id="head-info-block"><span>Не найдено ни одного сотрудника.</span></div>
        <? } else { ?>
        <div class="admin_users">
          <div id="head-info-block"><span><? if(!$users_list_num) { ?> <? } else { ?><? echo declOfNum($users_list_num, array('Найден', 'Найдено', 'Найдено')); ?> <? echo $users_list_num; ?> <? echo declOfNum($users_list_num, array('сотрудник', 'сотрудника', 'сотрудников')); ?><? } ?></span><div class="fl_r"><? if($users_list_num) { ?><div id="admin_list_users_page"><? echo pages(array('ents_count' => $users_list_num, 'ents_print' => 10, 'page' => $_GET['page'])); ?></div><? } ?></div></div>
         <? echo $user->admin_users_list(); ?>
        </div>
        <? } ?>
       </div>
      </div>
     </div>
     <input type="hidden" id="captcha_key">
     <input type="hidden" id="captcha_code">
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
